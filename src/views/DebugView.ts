import Utils from '../utils/Utils';
import Register from '../cpu/Register';
import CPU from '../cpu/CPU';
import Memory from '../memory/Memory';
import Video from '../io/Video';

interface HTMLElements {
  // flag input elements
  regPC: HTMLInputElement,
  regSP: HTMLInputElement,
  regAC: HTMLInputElement,
  regX: HTMLInputElement,
  regY: HTMLInputElement,
  flagN: HTMLInputElement,
  flagV: HTMLInputElement,
  flag1: HTMLInputElement,
  flagB: HTMLInputElement,
  flagD: HTMLInputElement,
  flagI: HTMLInputElement,
  flagZ: HTMLInputElement,
  flagC: HTMLInputElement,
  // disassembler container elements
  dasmContent: HTMLPreElement,
  dasmPosition: HTMLInputElement,
  // memory dump container elements
  dumpContent: HTMLPreElement,
  dumpPosition: HTMLInputElement,
  // breakpoint position
  breakpoint: HTMLInputElement,
  // emulator action elements
  step: HTMLButtonElement,
  start: HTMLButtonElement,
  stop: HTMLButtonElement,
  // output screen
  screen: HTMLCanvasElement
}

class CPUView {

  private static MAX_LINES = 16; // maximum display lines (memory dump / disassembler)

  private mem: Memory;
  private reg: Register;
  private screen: Video;

  // Elements in HTML Output
  private rootElement: HTMLDivElement;
  private elements: HTMLElements;

  constructor(mem: Memory, cpu: CPU) {
    this.mem = mem;
    this.reg = cpu.getRegister();

    // create view page
    this.rootElement = document.getElementById('root') as HTMLDivElement;
    this.createView();
  }

  /**
   * registers a button handler with a given name
   *
   * @param type Eventtype (click, change, ...)
   * @param ObjectName (id from HTML Button)
   * @param Event Handler
   */
  public registerListener(type: string, name: string, handler: (e: Event) => void): void {
    this.elements[name].addEventListener(type, handler);
  }

  /**
   *  disables button
   */
  public disableButton(name: string) {
    this.elements[name].disabled = true;
  }

  /**
   *  enables button
   */
  public enableButton(name: string) {
    this.elements[name].disabled = false;
  }

  /**
   * registers keyboard input handler
   *
   * @param Event Handler
   */
  public registerKeyListener(type: string, handler: (e: KeyboardEvent) => void): void {
    document.body.addEventListener(type, handler);
  }

  /**
   * updates the dom elements with the values from the emulation state
   * (only updated once a second to reduce calculation time)
   */
  public update(): void {
    // print register values
    this.elements.regPC.value = '$' + Utils.decimalToHex(this.reg.pc, 4);
    this.elements.regSP.value = '$' + Utils.decimalToHex(this.reg.sp, 2);
    this.elements.regAC.value = '$' + Utils.decimalToHex(this.reg.ac, 2);
    this.elements.regX.value = '$' + Utils.decimalToHex(this.reg.x, 2);
    this.elements.regY.value = '$' + Utils.decimalToHex(this.reg.y, 2);

    // set checked flag for every status register element

    ['N', 'V', '1', 'B', 'D', 'I', 'Z', 'C'].forEach(element => {
      this.elements['flag' + element].checked = (this.reg.flags & Register['FLAG_' + element]) !== 0;
    });

    // dump memory contents beginning from current program counter
    let dumpBegin = Utils.intDiv(this.getMemoryDumpPositionValue(), 16 * CPUView.MAX_LINES) * 16 * CPUView.MAX_LINES; // neue Seite alle "maxLines"
    this.elements.dumpContent.innerText = this.mem.getMemoryDump(dumpBegin, CPUView.MAX_LINES);

    // disassemble beginning from current program counter
    let dasmBegin = this.getDisassemblerPositionValue();
    this.elements.dasmContent.innerText = this.mem.getDisassembly(dasmBegin, CPUView.MAX_LINES);
  }

  /**
   * updates the virtual screen
   */
  public updateScreen() {
    // redraw screen
    this.screen.update();
  }

  public getBreakpointValue() : string {
    return this.elements.breakpoint.value;
  }

  public showPreviousDisassemblerPage() {
    let dasmBegin = this.getDisassemblerPositionValue();
    dasmBegin -= CPUView.MAX_LINES;
    this.setDisassemblerPositionValue(dasmBegin);
    this.update();
  }

  public showNextDisassemblerPage() {
    let dasmBegin = this.getDisassemblerPositionValue();
    dasmBegin += CPUView.MAX_LINES;
    this.setDisassemblerPositionValue(dasmBegin);
    this.update();
  }

  public setDisassemblerPositionValue(begin: number) {
    this.elements.dasmPosition.value = '$' + Utils.decimalToHex(begin, 4);
  }

  public getDisassemblerPositionValue(): number {
    return parseInt(this.elements.dasmPosition.value.replace('$', ''), 16);
  }

  public showPreviousMemoryDumpPage() {
    let dumpBegin = Utils.intDiv(this.getMemoryDumpPositionValue(), 16 * CPUView.MAX_LINES) * 16 * CPUView.MAX_LINES;
    dumpBegin -= 16 * CPUView.MAX_LINES;
    this.setMemoryDumpPositionValue(dumpBegin);
    this.update();
  }

  public showNextMemoryDumpPage() {
    let dumpBegin = Utils.intDiv(this.getMemoryDumpPositionValue(), 16 * CPUView.MAX_LINES) * 16 * CPUView.MAX_LINES;
    dumpBegin += 16 * CPUView.MAX_LINES;
    this.setMemoryDumpPositionValue(dumpBegin);
    this.update();
  }

  public setMemoryDumpPositionValue(begin: number) {
    this.elements.dumpPosition.value = '$' + Utils.decimalToHex(begin, 4);
  }

  public getMemoryDumpPositionValue(): number {
    return parseInt(this.elements.dumpPosition.value.replace('$', ''), 16);
  }

  /**
   * creates the output page and sets all element variables to the
   * dom elements.
   */
  private createView(): void {
    let outputHTML =
      '<nav class="navbar navbar-default navbar-static-top" role="navigation">' +
      '  <div class="navbar-header">' +
      '    <span class="navbar-brand">6510 CPU</span>' +
      '  </div>' +
      '</nav>' +
      '<div class="row" style="max-width: 99%; margin:0 auto;">' +
      '  <div class="col-sm-6">' +
      '    <div class="panel panel-primary">' +
      '      <div class="panel-heading">Disassembler</div>' +
      '      <div class="panel-body">' +
      '        <pre id="dasmContent"></pre>' +
      '      <div class="col-sm-1">' +
      '        <button type="button" class="btn btn-primary btn-md" id="dasmBackward"><spand class="glyphicon glyphicon-chevron-left"></button>' +
      '      </div>' +
      '      <div class="col-sm-10">' +
      '        <div class="col-sm-9 col-sm-offset-1">' +
      '          <div class="input-group">' +
      '            <span class="input-group-addon">Adresse</span>' +
      '            <input type="text" class="form-control" id="dasmPosition">' +
      '          </div>' +
      '        </div>' +
      '        <div class="col-sm-1">' +
      '          <div class="btn-toolbar" role="toolbar">' +
      '            <button type="button" class="btn btn-primary btn-md" id="dasmPositionChange"><span class="glyphicon glyphicon-search"></button>' +
      '          </div>' +
      '        </div>' +
      '      </div>' +
      '      <div class="col-sm-1">' +
      '        <button type="button" class="btn btn-primary btn-md" id="dasmForward"><spand class="glyphicon glyphicon-chevron-right"></button>' +
      '      </div>' +
      '    </div>' +
      '  </div>' +
      '  <div class="panel panel-primary">' +
      '    <div class="panel-heading">Memory Dump</div>' +
      '      <div class="panel-body">' +
      '        <pre id="dumpContent"></pre>' +
      '        <div class="col-sm-1">' +
      '          <button type="button" class="btn btn-primary btn-md" id="dumpBackward"><spand class="glyphicon glyphicon-chevron-left"></button>' +
      '        </div>' +
      '        <div class="col-sm-10">' +
      '          <div class="col-sm-9 col-sm-offset-1">' +
      '            <div class="input-group">' +
      '              <span class="input-group-addon">Adresse</span>' +
      '              <input type="text" class="form-control" id="dumpPosition">' +
      '            </div>' +
      '          </div>' +
      '          <div class="col-sm-1">' +
      '            <div class="btn-toolbar" role="toolbar">' +
      '              <button type="button" class="btn btn-primary btn-md" id="dumpPositionChange"><span class="glyphicon glyphicon-search"></button>' +
      '            </div>' +
      '          </div>' +
      '        </div>' +
      '        <div class="col-sm-1">' +
      '          <button type="button" class="btn btn-primary btn-md" id="dumpForward"><spand class="glyphicon glyphicon-chevron-right"></button>' +
      '        </div>' +
      '      </div>' +
      '    </div>' +
      '  </div>' +
      '  <div class="col-sm-6">' +
      '    <div class="col-sm-12">' +
      '      <div class="panel panel-primary">' +
      '        <div class="panel-heading">Screen</div>' +
      '        <div class="panel-body">' +
      '          <canvas id="screen" width="448" height="284" style="border:1px solid #000000; width: 448px; height: 284px;"></canvas>' +
      '        </div>' +
      '      </div>' +
      '      <div class="panel panel-primary">' +
      '        <div class="panel-heading">Register</div>' +
      '        <div class="panel-body">' +
      '          <div class="col-sm-6">' +
      '            <div class="input-group">' +
      '              <span class="input-group-addon">PC</span>' +
      '              <input type="text" class="form-control" style="background-color: #fff;" id="regPC" readonly>' +
      '            </div>' +
      '            <br>' +
      '            <div class="input-group">' +
      '              <span class="input-group-addon">SP</span>' +
      '              <input type="text" class="form-control" style="background-color: #fff;" id="regSP" readonly>' +
      '            </div>' +
      '            <br>' +
      '            <div class="input-group">' +
      '              <span class="input-group-addon">AC</span>' +
      '              <input type="text" class="form-control" style="background-color: #fff;" id="regAC" readonly>' +
      '            </div>' +
      '            <br>' +
      '          </div>' +
      '          <div class="col-sm-6">' +
      '            <div class="input-group">' +
      '              <span class="input-group-addon">X</span>' +
      '              <input type="text" class="form-control" style="background-color: #fff;" id="regX" readonly>' +
      '            </div>' +
      '            <br>' +
      '            <div class="input-group">' +
      '              <span class="input-group-addon">Y</span>' +
      '              <input type="text" class="form-control" style="background-color: #fff;" id="regY" readonly>' +
      '            </div>' +
      '            <br>' +
      '          </div>' +
      '        </div>' +
      '      </div>' +
      '    </div>' +
      '    <div class="col-sm-12">' +
      '      <div class="panel panel-primary">' +
      '        <div class="panel-heading">Statusregister</div>' +
      '        <div class="panel-body">' +
      '          <div class="col-sm-12">' +
      '            <div class="input-group-addon checkbox" style="background-color: #fff;">' +
      '              <label style="min-height: initial;"><input type="checkbox" id="flagN"> N </label>' +
      '            </div>' +
      '            <div class="input-group-addon checkbox" style="background-color: #fff;">' +
      '              <label style="min-height: initial;"><input type="checkbox" id="flagV"> V </label>' +
      '            </div>' +
      '            <div class="input-group-addon checkbox" style="background-color: #fff;">' +
      '              <label style="min-height: initial;"><input type="checkbox" id="flag1"> 1 </label>' +
      '            </div>' +
      '            <div class="input-group-addon checkbox" style="background-color: #fff;">' +
      '              <label style="min-height: initial;"><input type="checkbox" id="flagB"> B </label>' +
      '            </div>' +
      '            <div class="input-group-addon checkbox" style="background-color: #fff;">' +
      '              <label style="min-height: initial;"><input type="checkbox" id="flagD"> D </label>' +
      '            </div>' +
      '            <div class="input-group-addon checkbox" style="background-color: #fff;">' +
      '              <label style="min-height: initial;"><input type="checkbox" id="flagI"> I </label>' +
      '            </div>' +
      '            <div class="input-group-addon checkbox" style="background-color: #fff;">' +
      '              <label style="min-height: initial;"><input type="checkbox" id="flagZ"> Z </label>' +
      '            </div>' +
      '            <div class="input-group-addon checkbox" style="background-color: #fff;">' +
      '              <label style="min-height: initial;"><input type="checkbox" id="flagC"> C </label>' +
      '            </div>' +
      '          </div>' +
      '        </div>' +
      '      </div>' +
      '    </div>' +
      '    <div class="col-sm-12">' +
      '      <div class="panel panel-primary">' +
      '        <div class="panel-heading">Aktionen</div>' +
      '        <div class="panel-body">' +
      '          <div class="col-sm-12">' +
      '            <div class="btn-toolbar">' +
      '              <button type="button" class="btn btn-primary btn-md" id="step">Step</button>' +
      '              <button type="button" class="btn btn-primary btn-md" id="start">Start</button>' +
      '              <button type="button" class="btn btn-primary btn-md" id="stop">Stop</button>' +
      '            </div>' +
      '            <br>' +
      '            <div class="input-group">' +
      '              <span class="input-group-addon">Breakpoint</span>' +
      '              <input type="text" class="form-control" id="breakpoint">' +
      '            </div>' +
      '          </div>' +
      '        </div>' +
      '      </div>' +
      '    </div>' +
      '  </div>' +
      '</div>';

    this.rootElement.innerHTML = outputHTML;

    // get output elements
    this.elements = {
      regPC: document.getElementById("regPC") as HTMLInputElement,
      regSP: document.getElementById("regSP") as HTMLInputElement,
      regAC: document.getElementById("regAC") as HTMLInputElement,
      regX: document.getElementById("regX") as HTMLInputElement,
      regY: document.getElementById("regY") as HTMLInputElement,
      flagN: document.getElementById("flagN") as HTMLInputElement,
      flagV: document.getElementById("flagV") as HTMLInputElement,
      flag1: document.getElementById("flag1") as HTMLInputElement,
      flagB: document.getElementById("flagB") as HTMLInputElement,
      flagD: document.getElementById("flagD") as HTMLInputElement,
      flagI: document.getElementById("flagI") as HTMLInputElement,
      flagZ: document.getElementById("flagZ") as HTMLInputElement,
      flagC: document.getElementById("flagC") as HTMLInputElement,
      dasmContent: document.getElementById("dasmContent") as HTMLPreElement,
      dasmPosition: document.getElementById("dasmPosition") as HTMLInputElement,
      dumpContent: document.getElementById("dumpContent") as HTMLPreElement,
      dumpPosition: document.getElementById("dumpPosition") as HTMLInputElement,
      breakpoint: document.getElementById("breakpoint") as HTMLInputElement,
      step: document.getElementById("step") as HTMLButtonElement,
      start: document.getElementById("start") as HTMLButtonElement,
      stop: document.getElementById("stop") as HTMLButtonElement,
      screen: document.getElementById("screen") as HTMLCanvasElement
    }

    // start values for positions
    this.elements.breakpoint.value = '$' + Utils.decimalToHex(0, 4);

    // register button handler that only change the state of the view
    document.getElementById("dasmPositionChange").addEventListener('click', () => this.update());
    document.getElementById("dasmPosition").addEventListener('change', () => this.update());
    document.getElementById("dasmBackward").addEventListener('click', () => this.showPreviousDisassemblerPage());
    document.getElementById("dasmForward").addEventListener('click', () => this.showNextDisassemblerPage());

    document.getElementById("dumpPositionChange").addEventListener('click', () => this.update());
    document.getElementById("dumpPosition").addEventListener('change', () => this.update());
    document.getElementById("dumpBackward").addEventListener('click', () => this.showPreviousMemoryDumpPage());
    document.getElementById("dumpForward").addEventListener('click', () => this.showNextMemoryDumpPage());

    // create screen
    this.screen = new Video(this.mem, this.elements.screen);
  }
}
export default CPUView;