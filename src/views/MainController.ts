import MainView from './MainView';
import Memory from '../memory/Memory';
import CPU from '../cpu/CPU';
import Keyboard from '../io/Keyboard';

const basicRom = require('media/basic.901226-01.bin');
const characterRom = require('media/character.901225-01.bin');
const kernalRom = require('media/kernal.901227-03.bin');

class MainController {
    private memory: Memory;
    private cpu: CPU;
    private keyboard : Keyboard;

    private view: MainView;
    private running: boolean;
    private fps: number;
    private startTime: number;

    constructor(memory: Memory, cpu: CPU, keyboard : Keyboard) {
      this.memory = memory;
      this.cpu = cpu;
      this.keyboard = keyboard;
      this.memory.registerKeyboard(this.keyboard);

      this.running = false;
    }

    /**
     * registers a view and adds the listeners
     *
     * @param view
     */
    public registerView(view: MainView) {
      this.view = view;
      this.view.registerKeyListener('keydown', (e) => this.keyboard.onKeyDown(e));
      this.view.registerKeyListener('keyup', (e) => this.keyboard.onKeyUp(e));
    }

    /**
     * loads initial program into memory
     *
     * @param view
     */
    public initialize(data: any) {
      // load operating system roms
      this.memory.loadBASIC(data.get(basicRom));
      this.memory.loadKERNAL(data.get(kernalRom));
      this.memory.loadCHARACTER(data.get(characterRom));
      this.cpu.reset();

      // update view
      this.view.updateScreen();

      // autostart emulation
      this.startEmulation();
    }

    /**
     * starts the emulation loop
     */
    public startEmulation() {
      // console.log('controller: start');
      this.running = true;

      this.fps = 0; // frame counter
      this.startTime = performance.now(); // starttime of first frame
      requestAnimationFrame(this.emulationLoop.bind(this));
    }

    /**
     * main emulation loop
     */
    private emulationLoop(timestamp: number) {
      // run main loop
      let targetCycleCount = this.cpu.getCycleCount() + 20000;
      this.cpu.setInterruptOccured(); // interrupt every frame

      while (this.running && this.cpu.getCycleCount() < targetCycleCount) {
        this.cpu.step();

        let blankingPeriodLow = targetCycleCount - 100;
        if ((this.cpu.getCycleCount() >= blankingPeriodLow) && (this.cpu.getCycleCount() <= targetCycleCount)) {
          this.memory.writeByte(0xd012, 0);
        } else {
          this.memory.writeByte(0xd012, 1);
        }
      }

      this.view.updateScreen();
      this.fps++; // frame finished

      if ((performance.now() - this.startTime) > 1000) {  // > 1000ms
        this.startTime = performance.now();
        this.fps = this.fps;
        // console.log(' fps: ' + this.fps);
        this.fps = 0;
      }

      // schedule next frame
      if (this.running) {
        requestAnimationFrame(this.emulationLoop.bind(this));
      }
    }
  }
  export default MainController;