import DebugView from './DebugView';
import Memory from '../memory/Memory';
import CPU from '../cpu/CPU';
import Keyboard from '../io/Keyboard';

const basicRom = require('media/basic.901226-01.bin');
const characterRom = require('media/character.901225-01.bin');
const kernalRom = require('media/kernal.901227-03.bin');

class DebugController {
    private memory: Memory;
    private cpu: CPU;
    private keyboard: Keyboard;

    private view: DebugView;
    private running: boolean;
    private breakpointActive: boolean;
    private breakpoint: number;
    private fps: number;
    private startTime: number;

    constructor(memory: Memory, cpu: CPU, keyboard: Keyboard) {
        this.memory = memory;
        this.cpu = cpu;
        this.keyboard = keyboard;
        this.memory.registerKeyboard(this.keyboard);

        this.running = false;
        this.breakpointActive = false;
        this.breakpoint = 0x0000;
    }

    /**
     * registers a view and adds the listeners
     *
     * @param view
     */
    public registerView(view: DebugView) {
        this.view = view;
        this.view.registerKeyListener('keydown', (e) => this.keyboard.onKeyDown(e));
        this.view.registerKeyListener('keyup', (e) => this.keyboard.onKeyUp(e));
        this.view.registerListener('click', 'step', () => this.step());
        this.view.registerListener('click', 'start', () => this.startEmulation());
        this.view.registerListener('click', 'stop', () => this.stopEmulation());
        this.view.registerListener('change', 'breakpoint', () => this.changeBreakpoint());
        this.view.disableButton('start');
        this.view.disableButton('stop');
        this.view.disableButton('step');
    }

    /**
     * loads initial program into memory
     *
     * @param view
     */
    public initialize(data: any) {
        // load operating system roms
        this.memory.loadBASIC(data.get(basicRom));
        this.memory.loadKERNAL(data.get(kernalRom));
        this.memory.loadCHARACTER(data.get(characterRom));
        this.cpu.reset();

        // everything initialized => enable buttons
        this.view.enableButton('step');
        this.view.enableButton('start');
        this.view.disableButton('stop');

        // update view
        this.view.setMemoryDumpPositionValue(this.cpu.getRegister().pc);
        this.view.setDisassemblerPositionValue(this.cpu.getRegister().pc);
        this.view.update();
        this.view.updateScreen();

        // autostart emulation
        this.startEmulation();
    }

    /**
     * Executes one cpu cycle and updates the output view
     */
    public step() {
        // console.log('controller: step');
        this.cpu.step();

        // update
        this.view.setMemoryDumpPositionValue(this.cpu.getRegister().pc);
        this.view.setDisassemblerPositionValue(this.cpu.getRegister().pc);
        this.view.update();
        this.view.updateScreen();
    }

    /**
     * starts the emulation loop
     */
    public startEmulation() {
        // console.log('controller: start');
        this.view.disableButton('start');
        this.view.enableButton('stop');
        this.running = true;

        this.fps = 0; // frame counter
        this.startTime = performance.now(); // starttime of first frame
        requestAnimationFrame(this.emulationLoop.bind(this));
    }

    /**
     * stops the emulation loop
     */
    public stopEmulation() {
        // console.log('controller: stop');
        this.view.enableButton('start');
        this.view.disableButton('stop');
        this.running = false;
    }

    public changeBreakpoint(): void {
        let inputValue = this.view.getBreakpointValue();
        if (inputValue != '') {
            let breakpoint = parseInt(inputValue.replace('$', ''), 16);
            console.log('controller: activateBreakpoint at ' + inputValue + ' (dec: ' + breakpoint + ')');
            this.breakpoint = breakpoint;
            this.breakpointActive = true;
        } else {
            console.log('controller: disableBreakpoint');
            this.breakpointActive = false;
            this.breakpoint = 0x0000;
        }
    }

    /**
     * main emulation loop
     */
    private emulationLoop(timestamp: number) {
        // run main loop
        let targetCycleCount = this.cpu.getCycleCount() + 20000;
        this.cpu.setInterruptOccured(); // interrupt every frame

        while (this.running && this.cpu.getCycleCount() < targetCycleCount) {
            this.cpu.step();

            let blankingPeriodLow = targetCycleCount - 100;
            if ((this.cpu.getCycleCount() >= blankingPeriodLow) && (this.cpu.getCycleCount() <= targetCycleCount)) {
                this.memory.writeByte(0xd012, 0);
            } else {
                this.memory.writeByte(0xd012, 1);
            }

            // if breakpoint reached, then stop loop
            if (this.breakpointActive && this.breakpoint == this.cpu.getRegister().pc) {
                this.stopEmulation();
                return;
            }
        }

        this.view.updateScreen();
        this.fps++; // frame finished

        if ((performance.now() - this.startTime) > 1000) {  // > 1000ms
            this.startTime = performance.now();
            this.fps = this.fps;
            // console.log(' fps: ' + this.fps);
            this.fps = 0;
            this.view.setMemoryDumpPositionValue(this.cpu.getRegister().pc);
            this.view.setDisassemblerPositionValue(this.cpu.getRegister().pc);
            this.view.update(); // update debug output once a second
        }

        // schedule next frame
        if (this.running) {
            requestAnimationFrame(this.emulationLoop.bind(this));
        }
    }
}
export default DebugController;