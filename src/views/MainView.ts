import Video from '../io/Video';
import Memory from '../memory/Memory';

interface HTMLElements {
    screen: HTMLCanvasElement
}

class MainView {
    private memory: Memory;
    private screen: Video;

    // Elements in HTML Output
    private rootElement: HTMLDivElement;
    private elements: HTMLElements;

    constructor(memory: Memory) {
        this.memory = memory;
        // create view page
        this.rootElement = document.getElementById('root') as HTMLDivElement;
        this.createView();
    }

    /**
     * registers keyboard input handler
     *
     * @param Event Handler
     */
    public registerKeyListener(type: string, handler: (e: KeyboardEvent) => void): void {
        document.body.addEventListener(type, handler);
    }

    /**
     * updates the virtual screen
     */
    public updateScreen() {
        // redraw screen
        this.screen.update();
    }

    /**
     * creates the output page and sets all element variables to the
     * dom elements.
     */
    private createView(): void {
        let outputHTML =
            '<nav class="navbar navbar-default navbar-static-top" role="navigation">' +
            '  <div class="navbar-header">' +
            '    <span class="navbar-brand">C64 Emulator</span>' +
            '  </div>' +
            '</nav>' +
            '<div style="max-width: 75%; margin:0 auto;">' +
            '  <canvas id="screen" width="448" height="284" style="width: 100%;"></canvas>' +
            '</div>';

        this.rootElement.innerHTML = outputHTML;

        // get output elements
        this.elements = {
            screen: document.getElementById("screen") as HTMLCanvasElement
        }

        // create screen
        this.screen = new Video(this.memory, this.elements.screen);
    }
}
export default MainView;