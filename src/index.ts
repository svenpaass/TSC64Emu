declare function require(arg: string): any;
import 'bootstrap/dist/css/bootstrap.css';

const basicRom = require('media/basic.901226-01.bin');
const characterRom = require('media/character.901225-01.bin');
const kernalRom = require('media/kernal.901227-03.bin');

import AssetLoader from './utils/AssetLoader';
import Memory from './memory/Memory';
import CPU from './cpu/CPU';
import Keyboard from './io/Keyboard';

import MainController from './views/MainController';
import MainView from './views/MainView';

/******************************************************************************
 * application entry point
 ******************************************************************************/

const main = () => {

  // model
  const keyboard = new Keyboard();
  const memory = new Memory(64 * 1024); // 64kb
  const cpu = new CPU(memory);

  // view
  const view = new MainView(memory);

  // controller
  const controller = new MainController(memory, cpu, keyboard);
  controller.registerView(view);

  new AssetLoader(
    [
      characterRom,
      basicRom,
      kernalRom
    ], (data) => { controller.initialize(data); })
    ;
}
main();
