import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('67: CPU - 6502 BRK Preserves Decimal Flag', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('BRK', function () {
        it('test_brk_preserves_decimal_flag_when_it_is_set', function () {
            cpu.getRegister().flags = Register.FLAG_D;

            // $C000 BRK
            mem.writeByte(0xC000, 0x00);
            cpu.getRegister().pc = 0xC000;
            cpu.step();

            assert.equal(cpu.getRegister().flags & Register.FLAG_D, Register.FLAG_D); // Status
        })

        it('test_brk_preserves_decimal_flag_when_it_is_clear', function () {
            cpu.getRegister().flags = 0x00;

            // $C000 BRK
            mem.writeByte(0xC000, 0x00);
            cpu.getRegister().pc = 0xC000;
            cpu.step();

            assert.equal(cpu.getRegister().flags & Register.FLAG_D, 0); // Status
        })
    })
})
