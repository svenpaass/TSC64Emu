import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('66: CPU - 6502 Page Wrap Bug', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('ADC Indirect, Indexed (X)', function () {

        it('test_adc_ind_indexed_has_page_wrap_bug', function () {
            cpu.getRegister().flags = 0x00;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0xFF;

            // $0000 ADC ($0080,X)
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x80);
            // $007F Vector to $BBBB (read if page wrapped)
            mem.writeByte(0x007F, 0xBB);
            mem.writeByte(0x0080, 0xBB);
            // $017F Vector to $ABCD (read if no page wrap)
            mem.writeByte(0x017F, 0xCD);
            mem.writeByte(0x0180, 0xAB);
            // Data
            mem.writeByte(0xABCD, 0x01);
            mem.writeByte(0xBBBB, 0x02);

            cpu.step();
            assert.equal(cpu.getRegister().ac, 0x03);
        })
    })

    describe('ADC Indexed, Indirect (Y)', function () {

        it('test_adc_indexed_ind_y_has_page_wrap_bug', function () {
            cpu.getRegister().pc = 0x1000;
            cpu.getRegister().flags = 0x00;
            cpu.getRegister().ac = 0x42;
            cpu.getRegister().y = 0x02;

            // $1000 ADC ($FF),Y
            mem.writeByte(0x1000, 0x71);
            mem.writeByte(0x1001, 0xFF);
            // Vector
            mem.writeByte(0x00FF, 0x10); // low byte
            mem.writeByte(0x0100, 0x20); // high byte if no page wrap
            mem.writeByte(0x0000, 0x00); // high byte if page wrapped
            // Data
            mem.writeByte(0x2012, 0x14); // read if no page wrap
            mem.writeByte(0x0012, 0x42); // read if page wrapped

            cpu.step();
            assert.equal(cpu.getRegister().ac, 0x84);
        })
    })

    describe('AND Indexed, Indirect (Y)', function () {
        it('test_and_indexed_ind_y_has_page_wrap_bug', function () {
            cpu.getRegister().pc = 0x1000;
            cpu.getRegister().ac = 0x42;
            cpu.getRegister().y = 0x02;

            // $1000 AND ($FF),Y
            mem.writeByte(0x0000, 0x31);
            mem.writeByte(0x0001, 0xFF);
            // Vector
            mem.writeByte(0x00FF, 0x10); // low byte
            mem.writeByte(0x0100, 0x20); // high byte if no page wrap
            mem.writeByte(0x0000, 0x00); // high byte if page wrapped
            // Data
            mem.writeByte(0x2012, 0x00); // read if no page wrap
            mem.writeByte(0x0012, 0xFF); // read if page wrapped

            cpu.step();
            assert.equal(cpu.getRegister().ac, 0x42);
        })
    })

    describe('CMP Indirect, Indexed (X)', function () {

        it('test_cmp_ind_x_has_page_wrap_bug', function () {
            cpu.getRegister().flags = 0x00;
            cpu.getRegister().ac = 0x42;
            cpu.getRegister().x = 0xFF;

            // $0000 CMP ($80,X)
            mem.writeByte(0x0000, 0xC1);
            mem.writeByte(0x0001, 0x80);
            // $007f Vector to $BBBB (read if page wrapped)
            mem.writeByte(0x007F, 0xBB);
            mem.writeByte(0x0080, 0xBB);
            // $017f Vector to $ABCD (read if no page wrap)
            mem.writeByte(0x017F, 0xCD);
            mem.writeByte(0x0180, 0xAB);
            // Data
            mem.writeByte(0xABCD, 0x00);
            mem.writeByte(0xBBBB, 0x42);

            cpu.step();
            assert.equal(cpu.getRegister().ac, 0x42);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })
    })

    describe('CMP Indexed, Indirect (Y)', function () {

        it('test_cmp_indexed_ind_y_has_page_wrap_bug', function () {
            cpu.getRegister().pc = 0x1000;
            cpu.getRegister().flags = 0x00;
            cpu.getRegister().ac = 0x42;
            cpu.getRegister().y = 0x02;

            // $1000 CMP ($FF),Y
            mem.writeByte(0x1000, 0xD1);
            mem.writeByte(0x1001, 0xFF);
            // Vector
            mem.writeByte(0x00FF, 0x10); // low byte
            mem.writeByte(0x0100, 0x20); // high byte if no page wrap
            mem.writeByte(0x0000, 0x00); // high byte if page wrapped
            // Data
            mem.writeByte(0x2012, 0x14); // read if no page wrap
            mem.writeByte(0x0012, 0x42); // read if page wrapped

            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })
    })

    describe('EOR Indirect, Indexed (X)', function () {

        it('test_eor_ind_x_has_page_wrap_bug', function () {
            cpu.getRegister().flags = 0x00;
            cpu.getRegister().ac = 0xAA;
            cpu.getRegister().x = 0xFF;

            // $0000 EOR ($80,X)
            mem.writeByte(0x0000, 0x41);
            mem.writeByte(0x0001, 0x80);
            // $007f Vector to $BBBB (read if page wrapped)
            mem.writeByte(0x007F, 0xBB);
            mem.writeByte(0x0080, 0xBB);
            // $017f Vector to $ABCD (read if no page wrap)
            mem.writeByte(0x017F, 0xCD);
            mem.writeByte(0x0180, 0xAB);
            // Data
            mem.writeByte(0xABCD, 0x00);
            mem.writeByte(0xBBBB, 0xFF);

            cpu.step();
            assert.equal(cpu.getRegister().ac, 0x55);
        })
    })

    describe('EOR Indexed, Indirect (Y)', function () {

        it('test_eor_indexed_ind_y_has_page_wrap_bug', function () {
            cpu.getRegister().pc = 0x1000;
            cpu.getRegister().ac = 0xAA;
            cpu.getRegister().y = 0x02;

            // $1000 EOR ($FF),Y
            mem.writeByte(0x1000, 0x51);
            mem.writeByte(0x1001, 0xFF);
            // Vector
            mem.writeByte(0x00FF, 0x10); // low byte
            mem.writeByte(0x0100, 0x20); // high byte if no page wrap
            mem.writeByte(0x0000, 0x00); // high byte if page wrapped
            // Data
            mem.writeByte(0x2012, 0x00); // read if no page wrap
            mem.writeByte(0x0012, 0xFF); // read if page wrapped

            cpu.step();
            assert.equal(cpu.getRegister().ac, 0x55);
        })
    })

    describe('LDA Indirect, Indexed (X)', function () {

        it('test_lda_ind_indexed_x_has_page_wrap_bug', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0xFF;

            // $0000 LDA ($80,X)
            mem.writeByte(0x0000, 0xA1);
            mem.writeByte(0x0001, 0x80);
            // $007f Vector to $BBBB (read if page wrapped)
            mem.writeByte(0x007F, 0xBB);
            mem.writeByte(0x0080, 0xBB);
            // $017f Vector to $ABCD (read if no page wrap)
            mem.writeByte(0x017F, 0xCD);
            mem.writeByte(0x0180, 0xAB);
            // Data
            mem.writeByte(0xABCD, 0x42);
            mem.writeByte(0xBBBB, 0xEF);

            cpu.step();
            assert.equal(cpu.getRegister().ac, 0xEF);
        })
    })

    describe('LDA Indexed, Indirect (Y)', function () {

        it('test_lda_indexed_ind_y_has_page_wrap_bug', function () {
            cpu.getRegister().pc = 0x1000;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x02;

            // $1000 LDA ($FF),Y
            mem.writeByte(0x1000, 0xB1);
            mem.writeByte(0x1001, 0xFF);
            // Vector
            mem.writeByte(0x00FF, 0x10); // low byte
            mem.writeByte(0x0100, 0x20); // high byte if no page wrap
            mem.writeByte(0x0000, 0x00); // high byte if page wrapped
            // Data
            mem.writeByte(0x2012, 0x14); // read if no page wrap
            mem.writeByte(0x0012, 0x42); // read if page wrapped

            cpu.step();
            assert.equal(cpu.getRegister().ac, 0x42);
        })
    })


    describe('LDA Zero Page, X-Indexed', function () {
        it('test_lda_zp_x_indexed_page_wraps', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0xFF;
            // $0000 LDA $80,X
            mem.writeByte(0x0000, 0xB5);
            mem.writeByte(0x0001, 0x80);
            mem.writeByte(0x007F, 0x42);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x42);
        })
    })

    describe('JMP Indirect', function () {

        it('test_jmp_jumps_to_address_with_page_wrap_bug', function () {
            mem.writeByte(0x00ff, 0x00);

            // $0000 JMP ($00)
            mem.writeByte(0x0000, 0x6C);
            mem.writeByte(0x0001, 0xFF);
            mem.writeByte(0x0002, 0x00);

            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x6C00);
            // assert.equal(cpu.processorCycles, 5)
        })
    })

    describe('ORA Indexed, Indirect (Y)', function () {

        it('test_ora_indexed_ind_y_has_page_wrap_bug', function () {
            cpu.getRegister().pc = 0x1000;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x02;

            // $1000 ORA ($FF),Y
            mem.writeByte(0x1000, 0x11);
            mem.writeByte(0x1001, 0xFF);
            // Vector
            mem.writeByte(0x00FF, 0x10); // low byte
            mem.writeByte(0x0100, 0x20); // high byte if no page wrap
            mem.writeByte(0x0000, 0x00); // high byte if page wrapped
            // Data
            mem.writeByte(0x2012, 0x00); // read if no page wrap
            mem.writeByte(0x0012, 0x42); // read if page wrapped

            cpu.step();
            assert.equal(cpu.getRegister().ac, 0x42);
        })
    })

    describe('SBC Indexed, Indirect (Y)', function () {

        it('test_sbc_indexed_ind_y_has_page_wrap_bug', function () {
            cpu.getRegister().pc = 0x1000;
            cpu.getRegister().flags = Register.FLAG_C;
            cpu.getRegister().ac = 0x42;
            cpu.getRegister().y = 0x02;

            // $1000 SBC ($FF),Y
            mem.writeByte(0x1000, 0xF1);
            mem.writeByte(0x1001, 0xFF);
            // Vector
            mem.writeByte(0x00FF, 0x10); // low byte
            mem.writeByte(0x0100, 0x20); // high byte if no page wrap
            mem.writeByte(0x0000, 0x00); // high byte if page wrapped
            // Data
            mem.writeByte(0x2012, 0x02); // read if no page wrap
            mem.writeByte(0x0012, 0x03); // read if page wrapped

            cpu.step();
            assert.equal(cpu.getRegister().ac, 0x3F);
        })
    })
})

