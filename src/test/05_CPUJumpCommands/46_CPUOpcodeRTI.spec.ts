import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('46: CPU - RTI (return from interrupt) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('RTI', function () {

        it('test_rti_restores_status_and_pc_and_updates_sp', function () {
            // $0000 RTI
            mem.writeByte(0x000, 0x40);

            mem.writeByte(0x01FD, 0xFC); // Status, PCL, PCH
            mem.writeByte(0x01FE, 0x03);
            mem.writeByte(0x01FF, 0xC0);
            cpu.getRegister().sp = 0xFC;

            cpu.step();
            assert.equal(cpu.getRegister().pc, 0xC003);
            assert.equal(cpu.getRegister().flags, 0xFC);
            assert.equal(cpu.getRegister().sp, 0xFF);
        })

        it('test_rti_forces_break_and_unused_flags_high', function () {
            // $0000 RTI
            mem.writeByte(0x000, 0x40);

            mem.writeByte(0x01FD, 0x00); // Status, PCL, PCH
            mem.writeByte(0x01FE, 0x03);
            mem.writeByte(0x01FF, 0xC0);
            cpu.getRegister().sp = 0xFC;

            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_B, Register.FLAG_B);
            assert.equal(cpu.getRegister().flags & Register.FLAG_1, Register.FLAG_1);
        })
    })
})
