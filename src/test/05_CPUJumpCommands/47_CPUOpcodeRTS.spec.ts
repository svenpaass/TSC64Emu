import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('47: CPU - RTS (return from subroutine) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('RTS', function () {

        it('test_rts_restores_pc_and_increments_then_updates_sp', function () {
            // $0000 RTS
            mem.writeByte(0x000, 0x60);

            mem.writeByte(0x01FE, 0x03); // PCL, PCH
            mem.writeByte(0x01FF, 0xC0);
            cpu.getRegister().pc = 0x0000;
            cpu.getRegister().sp = 0xFD;

            cpu.step();
            assert.equal(cpu.getRegister().pc, 0xC004);
            assert.equal(cpu.getRegister().sp, 0xFF);
        })

        it('test_rts_wraps_around_top_of_memory', function () {
            // $1000 RTS
            mem.writeByte(0x1000, 0x60);

            mem.writeByte(0x01FE, 0xFF); // PCL, PCH
            mem.writeByte(0x01FF, 0xFF);
            cpu.getRegister().pc = 0x1000;
            cpu.getRegister().sp = 0xFD;

            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0000);
            assert.equal(cpu.getRegister().sp, 0xFF);
        })
    })
})
