import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('44: CPU - JMP (jump) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('JMP Absolute', function () {

        it('test_jmp_abs_jumps_to_absolute_address', function () {
            // $0000 JMP $ABCD
            mem.writeByte(0x0000, 0x4C);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0xABCD);
        })
    })

    describe('JMP Indirect', function () {

        it('test_jmp_ind_jumps_to_indirect_address', function () {
            // $0000 JMP ($ABCD)
            mem.writeByte(0x0000, 0x6C);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0x02);
            mem.writeByte(0x0200, 0xCD);
            mem.writeByte(0x0201, 0xAB);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0xABCD);
        })
    })
})
