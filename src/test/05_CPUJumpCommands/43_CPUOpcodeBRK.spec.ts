import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('43: CPU - BRK (break / interrupt) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('BRK', function () {

        it('test_brk_pushes_pc_plus_2_and_status_then_sets_pc_to_irq_vector', function () {
            cpu.getRegister().flags = Register.FLAG_1;
            mem.writeByte(0xFFFE, 0xCD);
            mem.writeByte(0xFFFF, 0xAB);

            // $C000 BRK
            mem.writeByte(0xC000, 0x00);
            cpu.getRegister().pc = 0xC000;
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0xABCD);
            assert.equal(mem.readByte(0x1ff), 0xC0); // PCH
            assert.equal(mem.readByte(0x1fe), 0x02); // PCL
            assert.equal(mem.readByte(0x1fd), Register.FLAG_B | Register.FLAG_1); // Status
            assert.equal(cpu.getRegister().sp, 0xFC);

            assert.equal(cpu.getRegister().flags, Register.FLAG_B | Register.FLAG_1 | Register.FLAG_I);
        })
    })
})
