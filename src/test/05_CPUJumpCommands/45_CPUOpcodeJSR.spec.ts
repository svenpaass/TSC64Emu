import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('45: CPU - JSR (jump subroutine) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('JSR', function () {

        it('test_jsr_pushes_pc_plus_2_and_sets_pc', function () {
            // $0000 JSR $FFD2
            mem.writeByte(0xC000, 0x20);
            mem.writeByte(0xC001, 0xD2);
            mem.writeByte(0xC002, 0xFF);
            cpu.getRegister().pc = 0xC000;
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0xFFD2);
            assert.equal(cpu.getRegister().sp, 0xFD);
            assert.equal(mem.readByte(0x01FF), 0xC0); // PCH
            assert.equal(mem.readByte(0x01FE), 0x02); // PCL+2
        })

        it('test_jsr_pushes_return_point_minus_1_border_case', function () {
            // $ABFD JSR $AB45
            mem.writeByte(0xABFD, 0x20);
            mem.writeByte(0xABFE, 0x45);
            mem.writeByte(0xABFF, 0xAB);
            cpu.getRegister().pc = 0xABFD;
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0xAB45);
            assert.equal(cpu.getRegister().sp, 0xFD);
            assert.equal(mem.readByte(0x01FF), 0xAB); // 0xAC00 - 1
            assert.equal(mem.readByte(0x01FE), 0xFF);
        })
    })
})