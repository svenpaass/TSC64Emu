import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('51: CPU - LSR (logical shift right) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('LSR Accumulator', function () {

        it('test_lsr_accumulator_rotates_in_zero_not_carry', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            // $0000 LSR A
            mem.writeByte(0x0000, 0x4A);
            cpu.getRegister().ac = 0x00;
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_lsr_accumulator_sets_carry_and_zero_flags_after_rotation', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            // $0000 LSR A
            mem.writeByte(0x0000, 0x4A);
            cpu.getRegister().ac = 0x01;
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_lsr_accumulator_rotates_bits_right', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            // $0000 LSR A
            mem.writeByte(0x0000, 0x4A);
            cpu.getRegister().ac = 0x04;
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })
    })

    describe('LSR Absolute', function () {

        it('test_lsr_absolute_rotates_in_zero_not_carry', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            // $0000 LSR $ABCD
            mem.writeByte(0x0000, 0x4E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_lsr_absolute_sets_carry_and_zero_flags_after_rotation', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            // $0000 LSR $ABCD
            mem.writeByte(0x0000, 0x4E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x01);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_lsr_absolute_rotates_bits_right', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            // $0000 LSR $ABCD
            mem.writeByte(0x0000, 0x4E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x04);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })
    })

    describe('LSR Zero Page', function () {

        it('test_lsr_zp_rotates_in_zero_not_carry', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            // $0000 LSR $0010
            mem.writeByte(0x0000, 0x46);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_lsr_zp_sets_carry_and_zero_flags_after_rotation', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            // $0000 LSR $0010
            mem.writeByte(0x0000, 0x46);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x01);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_lsr_zp_rotates_bits_right', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            // $0000 LSR $0010
            mem.writeByte(0x0000, 0x46);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x04);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })
    })

    describe('LSR Absolute, X-Indexed', function () {

        it('test_lsr_abs_x_indexed_rotates_in_zero_not_carry', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            cpu.getRegister().x = 0x03;
            // $0000 LSR $ABCD,X
            mem.writeByte(0x0000, 0x5E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_lsr_abs_x_indexed_sets_c_and_z_flags_after_rotation', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().x = 0x03;
            // $0000 LSR $ABCD,X
            mem.writeByte(0x0000, 0x5E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x01);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_lsr_abs_x_indexed_rotates_bits_right', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            cpu.getRegister().x = 0x03;
            // $0000 LSR $ABCD,X
            mem.writeByte(0x0000, 0x5E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x04);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })
    })

    describe('LSR Zero Page, X-Indexed', function () {

        it('test_lsr_zp_x_indexed_rotates_in_zero_not_carry', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            cpu.getRegister().x = 0x03;
            // $0000 LSR $0010,X
            mem.writeByte(0x0000, 0x56);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_lsr_zp_x_indexed_sets_carry_and_zero_flags_after_rotation', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().x = 0x03;
            // $0000 LSR $0010,X
            mem.writeByte(0x0000, 0x56);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x01);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_lsr_zp_x_indexed_rotates_bits_right', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            cpu.getRegister().x = 0x03;
            // $0000 LSR $0010,X
            mem.writeByte(0x0000, 0x56);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x04);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })
    })
})
