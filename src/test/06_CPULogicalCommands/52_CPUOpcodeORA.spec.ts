import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('52: CPU - ORA (or with accumulator) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('ORA Absolute', function () {

        it('test_ora_absolute_zeroes_or_zeros_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().ac = 0x00;

            // $0000 ORA $ABCD
            mem.writeByte(0x0000, 0x0D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_ora_absolute_turns_bits_on_sets_n_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_N;
            cpu.getRegister().ac = 0x03;

            // $0000 ORA $ABCD
            mem.writeByte(0x0000, 0x0D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x82);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x83);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ORA Zero Page', function () {

        it('test_ora_zp_zeroes_or_zeros_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().ac = 0x00;

            // $0000 ORA $0010
            mem.writeByte(0x0000, 0x05);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_ora_zp_turns_bits_on_sets_n_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_N;
            cpu.getRegister().ac = 0x03;

            // $0000 ORA $0010
            mem.writeByte(0x0000, 0x05);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x82);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x83);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ORA Immediate', function () {

        it('test_ora_immediate_zeroes_or_zeros_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().ac = 0x00;

            // $0000 ORA #$00
            mem.writeByte(0x0000, 0x09);
            mem.writeByte(0x0001, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_ora_immediate_turns_bits_on_sets_n_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_N;
            cpu.getRegister().ac = 0x03;

            // $0000 ORA #$82
            mem.writeByte(0x0000, 0x09);
            mem.writeByte(0x0001, 0x82);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x83);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ORA Absolute, X', function () {

        it('test_ora_abs_x_indexed_zeroes_or_zeros_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 ORA $ABCD,X
            mem.writeByte(0x0000, 0x1D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_ora_abs_x_indexed_turns_bits_on_sets_n_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_N;
            cpu.getRegister().ac = 0x03;
            cpu.getRegister().x = 0x03;

            // $0000 ORA $ABCD,X
            mem.writeByte(0x0000, 0x1D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x82);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x83);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ORA Absolute, Y', function () {

        it('test_ora_abs_y_indexed_zeroes_or_zeros_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;

            // $0000 ORA $ABCD,Y
            mem.writeByte(0x0000, 0x19);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_ora_abs_y_indexed_turns_bits_on_sets_n_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_N;
            cpu.getRegister().ac = 0x03;
            cpu.getRegister().y = 0x03;

            // $0000 ORA $ABCD,Y
            mem.writeByte(0x0000, 0x19);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x82);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x83);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ORA Indirect, Indexed (X)', function () {

        it('test_ora_ind_indexed_x_zeroes_or_zeros_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 ORA ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x01);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_ora_ind_indexed_x_turns_bits_on_sets_n_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_N;
            cpu.getRegister().ac = 0x03;
            cpu.getRegister().x = 0x03;

            // $0000 ORA ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x01);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);

            mem.writeByte(0xABCD, 0x82);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x83);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ORA Indexed, Indirect (Y)', function () {

        it('test_ora_indexed_ind_y_zeroes_or_zeros_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 ORA ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x11);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_ora_indexed_ind_y_turns_bits_on_sets_n_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_N;
            cpu.getRegister().ac = 0x03;
            cpu.getRegister().x = 0x03;

            // $0000 ORA ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x11);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x82);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x83);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ORA Zero Page, X', function () {

        it('test_ora_zp_x_indexed_zeroes_or_zeros_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 ORA $0010,X
            mem.writeByte(0x0000, 0x15);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_ora_zp_x_indexed_turns_bits_on_sets_n_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().ac = 0x03;
            cpu.getRegister().x = 0x03;

            // $0000 ORA $0010,X
            mem.writeByte(0x0000, 0x15);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x82);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x83);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })
})
