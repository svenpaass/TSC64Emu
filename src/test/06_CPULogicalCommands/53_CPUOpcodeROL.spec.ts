import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('53: CPU - ROL (rotate left) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('ROL Accumulator', function () {

        it('test_rol_accumulator_zero_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL A
            mem.writeByte(0x0000, 0x2A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_accumulator_80_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().ac = 0x80;
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().flags &= ~Register.FLAG_Z;

            // $0000 ROL A
            mem.writeByte(0x0000, 0x2A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_accumulator_zero_and_carry_one_clears_z_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROL A
            mem.writeByte(0x0000, 0x2A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_accumulator_sets_n_flag', function () {
            cpu.getRegister().ac = 0x40;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROL A
            mem.writeByte(0x0000, 0x2A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_rol_accumulator_shifts_out_zero', function () {
            cpu.getRegister().ac = 0x7F;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL A
            mem.writeByte(0x0000, 0x2A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_rol_accumulator_shifts_out_one', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL A
            mem.writeByte(0x0000, 0x2A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ROL Absolute', function () {

        it('test_rol_absolute_zero_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL $ABCD
            mem.writeByte(0x0000, 0x2E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_absolute_80_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().flags &= ~Register.FLAG_Z;

            // $0000 ROL $ABCD
            mem.writeByte(0x0000, 0x2E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x80);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_absolute_zero_and_carry_one_clears_z_flag', function () {
            cpu.getRegister().ac= 0x00;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROL $ABCD
            mem.writeByte(0x0000, 0x2E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_absolute_sets_n_flag', function () {
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROL $ABCD
            mem.writeByte(0x0000, 0x2E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x40);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_rol_absolute_shifts_out_zero', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL $ABCD
            mem.writeByte(0x0000, 0x2E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x7F);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_rol_absolute_shifts_out_one', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL $ABCD
            mem.writeByte(0x0000, 0x2E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ROL Zero Page', function () {

        it('test_rol_zp_zero_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL $0010
            mem.writeByte(0x0000, 0x26);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_zp_80_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().flags &= ~Register.FLAG_Z;

            // $0000 ROL $0010
            mem.writeByte(0x0000, 0x26);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x80);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_zp_zero_and_carry_one_clears_z_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROL $0010
            mem.writeByte(0x0000, 0x26);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_zp_sets_n_flag', function () {
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROL $0010
            mem.writeByte(0x0000, 0x26);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x40);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_rol_zp_shifts_out_zero', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL $0010
            mem.writeByte(0x0000, 0x26);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x7F);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_rol_zp_shifts_out_one', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL $0010
            mem.writeByte(0x0000, 0x26);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ROL Absolute, X-Indexed', function () {

        it('test_rol_abs_x_indexed_zero_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().x = 0x03;

            // $0000 ROL $ABCD,X
            mem.writeByte(0x0000, 0x3E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_abs_x_indexed_80_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().x = 0x03;

            // $0000 ROL $ABCD,X
            mem.writeByte(0x0000, 0x3E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x80);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_abs_x_indexed_zero_and_carry_one_clears_z_flag', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROL $ABCD,X
            mem.writeByte(0x0000, 0x3E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_abs_x_indexed_sets_n_flag', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROL $ABCD,X
            mem.writeByte(0x0000, 0x3E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x40);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_rol_abs_x_indexed_shifts_out_zero', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL $ABCD,X
            mem.writeByte(0x0000, 0x3E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x7F);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_rol_abs_x_indexed_shifts_out_one', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL $ABCD,X
            mem.writeByte(0x0000, 0x3E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ROL Zero Page, X-Indexed', function () {

        it('test_rol_zp_x_indexed_zero_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().x = 0x03;

            // $0000 ROL $0010,X
            mem.writeByte(0x0000, 0x36);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_zp_x_indexed_80_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().x = 0x03;

            // $0000 ROL $0010,X
            mem.writeByte(0x0000, 0x36);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x80);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_zp_x_indexed_zero_and_carry_one_clears_z_flag', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROL $0010,X
            mem.writeByte(0x0000, 0x36);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_rol_zp_x_indexed_sets_n_flag', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROL $0010,X
            mem.writeByte(0x0000, 0x36);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x40);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_rol_zp_x_indexed_shifts_out_zero', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL $0010,X
            mem.writeByte(0x0000, 0x36);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x7F);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_rol_zp_x_indexed_shifts_out_one', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROL $0010,X
            mem.writeByte(0x0000, 0x36);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })
})
