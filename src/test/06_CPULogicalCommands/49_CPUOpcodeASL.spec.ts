import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('49: CPU - ASL (arithmetic shift left) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('ASL Accumulator', function () {

        it('test_asl_accumulator_sets_z_flag', function () {
            cpu.getRegister().ac = 0x00;

            // $0000 ASL A
            mem.writeByte(0x0000, 0x0A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_asl_accumulator_sets_n_flag', function () {
            cpu.getRegister().ac = 0x40;

            // $0000 ASL A
            mem.writeByte(0x0000, 0x0A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_asl_accumulator_shifts_out_zero', function () {
            cpu.getRegister().ac = 0x7F;

            // $0000 ASL A
            mem.writeByte(0x0000, 0x0A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_asl_accumulator_shifts_out_one', function () {
            cpu.getRegister().ac = 0xFF;

            // $0000 ASL A
            mem.writeByte(0x0000, 0x0A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })

        it('test_asl_accumulator_80_sets_z_flag', function () {
            cpu.getRegister().ac = 0x80;
            cpu.getRegister().flags &= ~(Register.FLAG_Z);

            // $0000 ASL A
            mem.writeByte(0x0000, 0x0A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })
    })

    describe('ASL Absolute', function () {

        it('test_asl_absolute_sets_z_flag', function () {
            // $0000 ASL $ABCD
            mem.writeByte(0x0000, 0x0E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_asl_absolute_sets_n_flag', function () {
            // $0000 ASL $ABCD
            mem.writeByte(0x0000, 0x0E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x40);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_asl_absolute_shifts_out_zero', function () {
            cpu.getRegister().ac = 0xAA;

            // $0000 ASL $ABCD
            mem.writeByte(0x0000, 0x0E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x7F);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(mem.readByte(0xABCD), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_asl_absolute_shifts_out_one', function () {
            cpu.getRegister().ac = 0xAA;

            // $0000 ASL $ABCD
            mem.writeByte(0x0000, 0x0E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(mem.readByte(0xABCD), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ASL Zero Page', function () {

        it('test_asl_zp_sets_z_flag', function () {
            // $0000 ASL $0010
            mem.writeByte(0x0000, 0x06);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_asl_zp_sets_n_flag', function () {
            // $0000 ASL $0010
            mem.writeByte(0x0000, 0x06);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x40);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_asl_zp_shifts_out_zero', function () {
            cpu.getRegister().ac = 0xAA;

            // $0000 ASL $0010
            mem.writeByte(0x0000, 0x06);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x7F);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(mem.readByte(0x0010), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_asl_zp_shifts_out_one', function () {
            cpu.getRegister().ac = 0xAA;

            // $0000 ASL $0010
            mem.writeByte(0x0000, 0x06);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(mem.readByte(0x0010), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ASL Absolute, X-Indexed', function () {

        it('test_asl_abs_x_indexed_sets_z_flag', function () {
            cpu.getRegister().x = 0x03;

            // $0000 ASL $ABCD,X
            mem.writeByte(0x0000, 0x1E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_asl_abs_x_indexed_sets_n_flag', function () {
            cpu.getRegister().x = 0x03;

            // $0000 ASL $ABCD,X
            mem.writeByte(0x0000, 0x1E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x40);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_asl_abs_x_indexed_shifts_out_zero', function () {
            cpu.getRegister().ac = 0xAA;
            cpu.getRegister().x = 0x03;

            // $0000 ASL $ABCD,X
            mem.writeByte(0x0000, 0x1E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x7F);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_asl_abs_x_indexed_shifts_out_one', function () {
            cpu.getRegister().ac = 0xAA;
            cpu.getRegister().x = 0x03;

            // $0000 ASL $ABCD,X
            mem.writeByte(0x0000, 0x1E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ASL Zero Page, X-Indexed', function () {

        it('test_asl_zp_x_indexed_sets_z_flag', function () {
            cpu.getRegister().x = 0x03;

            // $0000 ASL $0010,X
            mem.writeByte(0x0000, 0x16);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_asl_zp_x_indexed_sets_n_flag', function () {
            cpu.getRegister().x = 0x03;

            // $0000 ASL $0010,X
            mem.writeByte(0x0000, 0x16);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x40);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_asl_zp_x_indexed_shifts_out_zero', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().ac = 0xAA;

            // $0000 ASL $0010,X
            mem.writeByte(0x0000, 0x16);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x7F);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_asl_zp_x_indexed_shifts_out_one', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().ac = 0xAA;

            // $0000 ASL $0010,X
            mem.writeByte(0x0000, 0x16);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0xFE);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })
})
