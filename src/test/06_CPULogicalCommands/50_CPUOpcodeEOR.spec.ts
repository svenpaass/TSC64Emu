import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('50: CPU - EOR (exclusive or (with accumulator)) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('EOR Absolute', function () {

        it('test_eor_absolute_flips_bits_over_setting_z_flag', function () {
            cpu.getRegister().ac = 0xFF;

            // $0000 EOR $ABCD
            mem.writeByte(0x0000, 0x4D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(mem.readByte(0xABCD), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_eor_absolute_flips_bits_over_setting_n_flag', function () {
            cpu.getRegister().ac = 0x00;

            // $0000 EOR $ABCD
            mem.writeByte(0x0000, 0x4D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(mem.readByte(0xABCD), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('EOR Zero Page', function () {

        it('test_eor_zp_flips_bits_over_setting_z_flag', function () {
            cpu.getRegister().ac = 0xFF;

            // $0000 EOR $0010
            mem.writeByte(0x0000, 0x45);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(mem.readByte(0x0010), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_eor_zp_flips_bits_over_setting_n_flag', function () {
            cpu.getRegister().ac = 0x00;

            // $0000 EOR $0010
            mem.writeByte(0x0000, 0x45);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(mem.readByte(0x0010), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('EOR Immediate', function () {

        it('test_eor_immediate_flips_bits_over_setting_z_flag', function () {
            cpu.getRegister().ac = 0xFF;

            // $0000 EOR #$FF
            mem.writeByte(0x0000, 0x49);
            mem.writeByte(0x0001, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_eor_immediate_flips_bits_over_setting_n_flag', function () {
            cpu.getRegister().ac = 0x00;

            // $0000 EOR #$FF
            mem.writeByte(0x0000, 0x49);
            mem.writeByte(0x0001, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('EOR Absolute, X-Indexed', function () {

        it('test_eor_abs_x_indexed_flips_bits_over_setting_z_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;

            // $0000 EOR $ABCD,X
            mem.writeByte(0x0000, 0x5D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_eor_abs_x_indexed_flips_bits_over_setting_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 EOR $ABCD,X
            mem.writeByte(0x0000, 0x5D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('EOR Absolute, Y-Indexed', function () {

        it('test_eor_abs_y_indexed_flips_bits_over_setting_z_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().y = 0x03;

            // $0000 EOR $ABCD,Y
            mem.writeByte(0x0000, 0x59);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().y), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_eor_abs_y_indexed_flips_bits_over_setting_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;

            // $0000 EOR $ABCD,Y
            mem.writeByte(0x0000, 0x59);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().y), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('EOR Indirect, Indexed (X)', function () {

        it('test_eor_ind_indexed_x_flips_bits_over_setting_z_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;

            // $0000 EOR ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x41);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);

            mem.writeByte(0xABCD, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(mem.readByte(0xABCD), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_eor_ind_indexed_x_flips_bits_over_setting_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 EOR ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x41);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);

            mem.writeByte(0xABCD, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(mem.readByte(0xABCD), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('EOR Indexed, Indirect (Y)', function () {

        it('test_eor_indexed_ind_y_flips_bits_over_setting_z_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;

            // $0000 EOR ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x51);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().y), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_eor_indexed_ind_y_flips_bits_over_setting_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 EOR ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x51);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().y), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('EOR Zero Page, X-Indexed', function () {

        it('test_eor_zp_x_indexed_flips_bits_over_setting_z_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;

            // $0000 EOR $0010,X
            mem.writeByte(0x0000, 0x55);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_eor_zp_x_indexed_flips_bits_over_setting_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 EOR $0010,X
            mem.writeByte(0x0000, 0x55);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFF);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })
})
