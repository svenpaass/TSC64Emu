import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('48: CPU - AND (and (with accumulator)) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('AND (Absolute)', function () {

        it('test_and_absolute_all_zeros_setting_zero_flag', function () {
            cpu.getRegister().ac = 0xFF;

            // $0000 AND $ABCD
            mem.writeByte(0x0000, 0x2D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_and_absolute_zeros_and_ones_setting_negative_flag', function () {
            cpu.getRegister().ac = 0xFF;

            // $0000 AND $ABCD
            mem.writeByte(0x0000, 0x2D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0xAA);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('AND (Zero Page)', function () {

        it('test_and_zp_all_zeros_setting_zero_flag', function () {
            cpu.getRegister().ac = 0xFF;

            // $0000 AND $0010
            mem.writeByte(0x0000, 0x25);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_and_zp_zeros_and_ones_setting_negative_flag', function () {
            cpu.getRegister().ac = 0xFF;

            // $0000 AND $0010
            mem.writeByte(0x0000, 0x25);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0xAA);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('AND (Immediate)', function () {

        it('test_and_immediate_all_zeros_setting_zero_flag', function () {
            cpu.getRegister().ac = 0xFF;

            // $0000 AND #$00
            mem.writeByte(0x0000, 0x29);
            mem.writeByte(0x0001, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_and_immediate_zeros_and_ones_setting_negative_flag', function () {
            cpu.getRegister().ac = 0xFF;

            // $0000 AND #$AA
            mem.writeByte(0x0000, 0x29);
            mem.writeByte(0x0001, 0xAA);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('AND (Absolute, X-Indexed)', function () {

        it('test_and_abs_x_all_zeros_setting_zero_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;

            // $0000 AND $ABCD,X
            mem.writeByte(0x0000, 0x3D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_and_abs_x_zeros_and_ones_setting_negative_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;

            // $0000 AND $ABCD,X
            mem.writeByte(0x0000, 0x3D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0xAA);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('AND (Absolute, Y-Indexed)', function () {

        it('test_and_abs_y_all_zeros_setting_zero_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().y = 0x03;

            // $0000 AND $ABCD,Y
            mem.writeByte(0x0000, 0x39);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_and_abs_y_zeros_and_ones_setting_negative_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().y = 0x03;

            // $0000 AND $ABCD,Y
            mem.writeByte(0x0000, 0x39);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0xAA);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('AND Indirect, Indexed (X)', function () {

        it('test_and_ind_indexed_x_all_zeros_setting_zero_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;

            // $0000 AND ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x21);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_and_ind_indexed_x_zeros_and_ones_setting_negative_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;

            // $0000 AND ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x21);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);

            mem.writeByte(0xABCD, 0xAA);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('AND Indexed, Indirect (Y)', function () {

        it('test_and_indexed_ind_y_all_zeros_setting_zero_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().y = 0x03;

            // $0000 AND ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x31);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_and_indexed_ind_y_zeros_and_ones_setting_negative_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().y = 0x03;

            // $0000 AND ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x31);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0xAA);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('AND Zero Page, X-Indexed', function () {

        it('test_and_zp_x_all_zeros_setting_zero_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;

            // $0000 AND $0010,X
            mem.writeByte(0x0000, 0x35);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_and_zp_x_all_zeros_and_ones_setting_negative_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;

            // $0000 AND $0010,X
            mem.writeByte(0x0000, 0x35);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0xAA);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xAA);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })
})

