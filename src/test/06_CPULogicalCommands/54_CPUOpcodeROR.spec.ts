import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('54: CPU - ROR (rotate right) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('ROR Accumulator', function () {

        it('test_ror_accumulator_zero_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROR A
            mem.writeByte(0x0000, 0x6A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_ror_accumulator_zero_and_carry_one_rotates_in_sets_n_flags', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR A
            mem.writeByte(0x0000, 0x6A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })

        it('test_ror_accumulator_shifts_out_zero', function () {
            cpu.getRegister().ac = 0x02;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR A
            mem.writeByte(0x0000, 0x6A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_ror_accumulator_shifts_out_one', function () {
            cpu.getRegister().ac = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR A
            mem.writeByte(0x0000, 0x6A);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ROR Absolute', function () {

        it('test_ror_absolute_zero_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROR $ABCD
            mem.writeByte(0x0000, 0x6E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_ror_absolute_zero_and_carry_one_rotates_in_sets_n_flags', function () {
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $ABCD
            mem.writeByte(0x0000, 0x6E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })

        it('test_ror_absolute_shifts_out_zero', function () {
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $ABCD
            mem.writeByte(0x0000, 0x6E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x02);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_ror_absolute_shifts_out_one', function () {
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $ABCD
            mem.writeByte(0x0000, 0x6E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x03);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ROR Zero Page', function () {

        it('test_ror_zp_zero_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROR $0010
            mem.writeByte(0x0000, 0x66);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_ror_zp_zero_and_carry_one_rotates_in_sets_n_flags', function () {
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $0010
            mem.writeByte(0x0000, 0x66);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })

        it('test_ror_zp_zero_absolute_shifts_out_zero', function () {
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $0010
            mem.writeByte(0x0000, 0x66);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x02);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_ror_zp_shifts_out_one', function () {
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $0010
            mem.writeByte(0x0000, 0x66);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x03);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ROR Absolute, X-Indexed', function () {

        it('test_ror_abs_x_indexed_zero_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROR $ABCD,X
            mem.writeByte(0x0000, 0x7E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_ror_abs_x_indexed_z_and_c_1_rotates_in_sets_n_flags', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $ABCD,X
            mem.writeByte(0x0000, 0x7E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })

        it('test_ror_abs_x_indexed_shifts_out_zero', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $ABCD,X
            mem.writeByte(0x0000, 0x7E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x02);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_ror_abs_x_indexed_shifts_out_one', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $ABCD,X
            mem.writeByte(0x0000, 0x7E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x03);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ROR Zero Page, X-Indexed', function () {

        it('test_ror_zp_x_indexed_zero_and_carry_zero_sets_z_flag', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 ROR $0010,X
            mem.writeByte(0x0000, 0x76);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_ror_zp_x_indexed_zero_and_carry_one_rotates_in_sets_n_flags', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $0010,X
            mem.writeByte(0x0000, 0x76);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })

        it('test_ror_zp_x_indexed_zero_absolute_shifts_out_zero', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $0010,X
            mem.writeByte(0x0000, 0x76);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x02);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_ror_zp_x_indexed_shifts_out_one', function () {
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ROR $0010,X
            mem.writeByte(0x0000, 0x76);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x03);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x81);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })
})
