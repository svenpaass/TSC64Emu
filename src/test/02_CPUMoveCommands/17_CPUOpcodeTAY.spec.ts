import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('17: CPU - TAY (transfer accumulator to Y) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('TAY', function () {

        it('test_tay_transfers_accumulator_into_y', function () {
            cpu.getRegister().ac = 0xAB;
            cpu.getRegister().y = 0x00;
            // $0000 TAY
            mem.writeByte(0x0000, 0xA8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0xAB);
            assert.equal(cpu.getRegister().y, 0xAB);
        });

        it('test_tay_sets_negative_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_N);
            cpu.getRegister().ac = 0x80;
            cpu.getRegister().y = 0x00;
            // $0000 TAY
            mem.writeByte(0x0000, 0xA8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().y, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });

        it('test_tay_sets_zero_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_Z);
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0xFF;
            // $0000 TAY
            mem.writeByte(0x0000, 0xA8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        });
    });
});
