import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('21: CPU - TYA (transfer Y to accumulator) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('TYA', function () {

        it('test_tya_transfers_y_into_a', function () {
            cpu.getRegister().y = 0xAB;
            cpu.getRegister().ac = 0x00;
            // $0000 TYA
            mem.writeByte(0x0000, 0x98);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0xAB);
            assert.equal(cpu.getRegister().y, 0xAB);
        });

        it('test_tya_sets_negative_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_N);
            cpu.getRegister().y = 0x80;
            cpu.getRegister().ac = 0x00;
            // $0000 TYA
            mem.writeByte(0x0000, 0x98);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().y, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });

        it('test_tya_sets_zero_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_Z);
            cpu.getRegister().y = 0x00;
            cpu.getRegister().ac = 0xFF;
            // $0000 TYA
            mem.writeByte(0x0000, 0x98);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        });
    });
});
