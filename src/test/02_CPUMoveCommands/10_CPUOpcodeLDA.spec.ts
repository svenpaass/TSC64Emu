import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('10: CPU - LDA (load accumulator) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('LDA Absolute', function () {

        it('test_lda_absolute_loads_a_sets_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            // $0000 LDA $ABCD
            mem.writeByte(0x0000, 0xAD);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_lda_absolute_loads_a_sets_z_flag', function () {
            cpu.getRegister().ac = 0xff;
            // $0000 LDA $ABCD
            mem.writeByte(0x0000, 0xAD);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

    });

    describe('LDA Zero Page', function () {

        it('test_lda_zp_loads_a_sets_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            // $0000 LDA $0010
            mem.writeByte(0x0000, 0xA5);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_lda_zp_loads_a_sets_z_flag', function () {
            cpu.getRegister().ac = 0xFF;
            // $0000 LDA $0010
            mem.writeByte(0x0000, 0xA5);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDA Immediate', function () {

        it('test_lda_immediate_loads_a_sets_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            // $0000 LDA #$80
            mem.writeByte(0x0000, 0xA9);
            mem.writeByte(0x0001, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_lda_immediate_loads_a_sets_z_flag', function () {
            cpu.getRegister().ac = 0xFF;
            // $0000 LDA #$00
            mem.writeByte(0x0000, 0xA9);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });


    describe('LDA Absolute, X-Indexed', function () {

        it('test_lda_abs_x_indexed_loads_a_sets_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            // $0000 LDA $ABCD,X
            mem.writeByte(0x0000, 0xBD);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });


        it('test_lda_abs_x_indexed_loads_a_sets_z_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;
            // $0000 LDA $ABCD,X
            mem.writeByte(0x0000, 0xBD);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_lda_abs_x_indexed_does_not_page_wrap', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x0FF;
            // $0000 LDA $0080,X
            mem.writeByte(0x0000, 0xBD);
            mem.writeByte(0x0001, 0x80);
            mem.writeByte(0x0002, 0x00);

            mem.writeByte(0x0080 + cpu.getRegister().x, 0x42);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x42);
        });
    });

    describe('LDA Absolute, Y-Indexed', function () {

        it('test_lda_abs_y_indexed_loads_a_sets_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            // $0000 LDA $ABCD,Y
            mem.writeByte(0x0000, 0xB9);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_lda_abs_y_indexed_loads_a_sets_z_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;
            // $0000 LDA $ABCD,Y
            mem.writeByte(0x0000, 0xB9);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_lda_abs_y_indexed_does_not_page_wrap', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x0FF;
            // $0000 LDA $0080,Y
            mem.writeByte(0x0000, 0xB9);
            mem.writeByte(0x0001, 0x80);
            mem.writeByte(0x0002, 0x00);

            mem.writeByte(0x0080 + cpu.getRegister().y, 0x42);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x42);
        });
    });

    describe('LDA Indirect, Indexed (X)', function () {

        it('test_lda_ind_indexed_x_loads_a_sets_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            // $0000 LDA ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0xA1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);

            mem.writeByte(0xABCD, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_lda_ind_indexed_x_loads_a_sets_z_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            // $0000 LDA ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0xA1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDA Indexed, Indirect (Y)', function () {

        it('test_lda_indexed_ind_y_loads_a_sets_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;
            // $0000 LDA ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0xB1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_lda_indexed_ind_y_loads_a_sets_z_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;
            // $0000 LDA ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0xA1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDA Zero Page, X-Indexed', function () {

        it('test_lda_zp_x_indexed_loads_a_sets_n_flag', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            // $0000 LDA $10,X
            mem.writeByte(0x0000, 0xB5);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_lda_zp_x_indexed_loads_a_sets_z_flag', function () {
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;
            // $0000 LDA $10,X
            mem.writeByte(0x0000, 0xB5);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

});

