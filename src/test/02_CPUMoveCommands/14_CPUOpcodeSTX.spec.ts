import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('14: CPU - STX (store X) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('STX Absolute', function () {

        it('test_stx_absolute_stores_x_leaves_x_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().x = 0xFF;
            // $0000 STX $ABCD
            mem.writeByte(0x0000, 0x8E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0xFF);
            assert.equal(cpu.getRegister().x, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_stx_absolute_stores_x_leaves_x_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().x = 0x00;
            // $0000 STX $ABCD
            mem.writeByte(0x0000, 0x8E);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });

    describe('STX Zero Page', function () {

        it('test_stx_zp_stores_x_leaves_x_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().x = 0xFF;
            // $0000 STX $0010
            mem.writeByte(0x0000, 0x86);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0xFF);
            assert.equal(cpu.getRegister().x, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_stx_zp_stores_x_leaves_x_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().x = 0x00;
            // $0000 STX $0010
            mem.writeByte(0x0000, 0x86);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });

    describe('STX Zero Page, Y-Indexed', function () {

        it('test_stx_zp_y_indexed_stores_x_leaves_x_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().x = 0xFF;
            cpu.getRegister().y = 0x03;
            // $0000 STX $0010,Y
            mem.writeByte(0x0000, 0x96);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().y), 0xFF);
            assert.equal(cpu.getRegister().x, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_stx_zp_y_indexed_stores_x_leaves_x_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().x = 0x00;
            cpu.getRegister().y = 0x03;
            // $0000 STA $0010,Y
            mem.writeByte(0x0000, 0x96);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().y, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().y), 0x00);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });
});
