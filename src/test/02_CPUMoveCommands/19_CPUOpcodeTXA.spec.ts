import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('19: CPU - TXA (transfer X to accumulator) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('TXA', function () {

        it('test_txa_transfers_x_into_a', function () {
            cpu.getRegister().x = 0xAB;
            cpu.getRegister().ac = 0x00;
            // $0000 TXA
            mem.writeByte(0x0000, 0x8A);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0xAB);
            assert.equal(cpu.getRegister().x, 0xAB);
        });

        it('test_txa_sets_negative_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_N);
            cpu.getRegister().x = 0x80;
            cpu.getRegister().ac = 0x00;
            // $0000 TXA
            mem.writeByte(0x0000, 0x8A);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().x, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });

        it('test_txa_sets_zero_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_Z);
            cpu.getRegister().x = 0x00;
            cpu.getRegister().ac = 0xFF;
            // $0000 TXA
            mem.writeByte(0x0000, 0x8A);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        });
    });
});
