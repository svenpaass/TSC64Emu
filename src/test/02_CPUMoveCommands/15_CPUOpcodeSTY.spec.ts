import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('15: CPU - STY (store Y) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('STY Absolute', function () {

        it('test_sty_absolute_stores_y_leaves_y_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().y = 0xFF;
            // $0000 STY $ABCD
            mem.writeByte(0x0000, 0x8C);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0xFF);
            assert.equal(cpu.getRegister().y, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_sty_absolute_stores_y_leaves_y_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().y = 0x00;
            // $0000 STY $ABCD
            mem.writeByte(0x0000, 0x8C);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });

    describe('STY Zero Page', function () {

        it('test_sty_zp_stores_y_leaves_y_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().y = 0xFF;
            // $0000 STY $0010
            mem.writeByte(0x0000, 0x84);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0xFF);
            assert.equal(cpu.getRegister().y, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_sty_zp_stores_y_leaves_y_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().y = 0x00;
            // $0000 STY $0010
            mem.writeByte(0x0000, 0x84);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });

    describe('STY Zero Page, X-Indexed', function () {

        it('test_sty_zp_x_indexed_stores_y_leaves_y_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().y = 0xFF;
            cpu.getRegister().x = 0x03;
            // $0000 STY $0010,Y
            mem.writeByte(0x0000, 0x94);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0xFF);
            assert.equal(cpu.getRegister().y, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_sty_zp_x_indexed_stores_y_leaves_y_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().y = 0x00;
            cpu.getRegister().x = 0x03;
            // $0000 STA $0010,X
            mem.writeByte(0x0000, 0x94);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });
});
