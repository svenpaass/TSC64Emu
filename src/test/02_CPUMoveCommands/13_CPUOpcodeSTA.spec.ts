import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('13: CPU - STA (store accumulator) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('STA Absolute', function () {

        it('test_sta_absolute_stores_a_leaves_a_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0xFF;
            // $0000 STA $ABCD
            mem.writeByte(0x0000, 0x8D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0xFF);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_sta_absolute_stores_a_leaves_a_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0x00;
            // $0000 STA $ABCD
            mem.writeByte(0x0000, 0x8D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });

    describe('STA Zero Page', function () {

        it('test_sta_zp_stores_a_leaves_a_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0xFF;
            // $0000 STA $0010
            mem.writeByte(0x0000, 0x85);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0xFF);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_sta_zp_stores_a_leaves_a_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0x00;
            // $0000 STA $0010
            mem.writeByte(0x0000, 0x85);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });

    describe('STA Absolute, X-Indexed', function () {

        it('test_sta_abs_x_indexed_stores_a_leaves_a_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;
            // $0000 STA $ABCD,X
            mem.writeByte(0x0000, 0x9D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0xFF);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_sta_abs_x_indexed_stores_a_leaves_a_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            // $0000 STA $ABCD,X
            mem.writeByte(0x0000, 0x9D);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });

    describe('STA Absolute, Y-Indexed', function () {

        it('test_sta_abs_y_indexed_stores_a_leaves_a_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().y = 0x03;
            // $0000 STA $ABCD,Y
            mem.writeByte(0x0000, 0x99);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().y), 0xFF);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_sta_abs_y_indexed_stores_a_leaves_a_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;
            // $0000 STA $ABCD,Y
            mem.writeByte(0x0000, 0x99);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().y), 0x00);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });

    describe('STA Indirect, Indexed (X)', function () {

        it('test_sta_ind_indexed_x_stores_a_leaves_a_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;
            // $0000 STA ($0010,X)
            // $0013 Vector to $FEED
            mem.writeByte(0x0000, 0x81);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xED);
            mem.writeByte(0x0014, 0xFE);

            mem.writeByte(0xFEED, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0xFEED), 0xFF);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_sta_ind_indexed_x_stores_a_leaves_a_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            // $0000 STA ($0010,X)
            // $0013 Vector to $FEED
            mem.writeByte(0x0000, 0x81);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xED);
            mem.writeByte(0x0014, 0xFE);

            mem.writeByte(0xFEED, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0xFEED), 0x00);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });

    describe('STA Indexed, Indirect (Y)', function () {

        it('test_sta_indexed_ind_y_stores_a_leaves_a_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().y = 0x03;
            // $0000 STA ($0010),Y
            // $0010 Vector to $FEED
            mem.writeByte(0x0000, 0x91);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xED);
            mem.writeByte(0x0011, 0xFE);

            mem.writeByte(0xFEED + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0xFEED + cpu.getRegister().y), 0xFF);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_sta_indexed_ind_y_stores_a_leaves_a_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;
            // $0000 STA ($0010),Y
            // $0010 Vector to $FEED
            mem.writeByte(0x0000, 0x91);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xED);
            mem.writeByte(0x0011, 0xFE);

            mem.writeByte(0xFEED + cpu.getRegister().y, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0xFEED + cpu.getRegister().y), 0x00);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });

    describe('STA Zero Page, X-Indexed', function () {

        it('test_sta_zp_x_indexed_stores_a_leaves_a_and_n_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_N);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0xFF;
            cpu.getRegister().x = 0x03;
            // $0000 STA $0010,X
            mem.writeByte(0x0000, 0x95);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0xFF);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags, flags);
        });

        it('test_sta_zp_x_indexed_stores_a_leaves_a_and_z_flag_unchanged', function () {
            let flags = 0xFF & ~(Register.FLAG_Z);
            cpu.getRegister().flags = flags;
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            // $0000 STA $0010,X
            mem.writeByte(0x0000, 0x95);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags, flags);
        });
    });
});
