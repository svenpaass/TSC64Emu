import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('12: CPU - LDY (load Y) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('LDY Absolute', function () {

        it('test_ldy_absolute_loads_y_sets_n_flag', function () {
            cpu.getRegister().y = 0x00;
            // $0000 LDY $ABCD
            mem.writeByte(0x0000, 0xAC);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().y, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_ldy_absolute_loads_y_sets_z_flag', function () {
            cpu.getRegister().y = 0xff;
            // $0000 LDY $ABCD
            mem.writeByte(0x0000, 0xAC);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDY Zero Page', function () {

        it('test_ldy_zp_loads_y_sets_n_flag', function () {
            cpu.getRegister().y = 0x00;
            // $0000 LDY $0010
            mem.writeByte(0x0000, 0xA4);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().y, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_ldy_zp_loads_y_sets_z_flag', function () {
            cpu.getRegister().y = 0xFF;
            // $0000 LDY $0010
            mem.writeByte(0x0000, 0xA4);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDY Immediate', function () {

        it('test_ldy_immediate_loads_y_sets_n_flag', function () {
            cpu.getRegister().x = 0x00;
            // $0000 LDY #$80
            mem.writeByte(0x0000, 0xA0);
            mem.writeByte(0x0001, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().y, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_ldy_immediate_loads_y_sets_z_flag', function () {
            cpu.getRegister().y = 0xFF;
            // $0000 LDY #$00
            mem.writeByte(0x0000, 0xA0);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDY Absolute, X-Indexed', function () {

        it('test_ldy_abs_x_indexed_loads_x_sets_n_flag', function () {
            cpu.getRegister().y = 0x00;
            cpu.getRegister().x = 0x03;
            // $0000 LDY $ABCD,X
            mem.writeByte(0x0000, 0xBC);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().y, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_ldy_abs_x_indexed_loads_x_sets_z_flag', function () {
            cpu.getRegister().y = 0xFF;
            cpu.getRegister().x = 0x03;
            // $0000 LDY $ABCD,X
            mem.writeByte(0x0000, 0xBC);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDY Zero Page, X-Indexed', function () {

        it('test_ldy_zp_x_indexed_loads_x_sets_n_flag', function () {
            cpu.getRegister().x = 0x00;
            cpu.getRegister().y = 0x03;
            // $0000 LDY $0010,X
            mem.writeByte(0x0000, 0xB4);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().y, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_ldy_zp_x_indexed_loads_x_sets_z_flag', function () {
            cpu.getRegister().y = 0xFF;
            cpu.getRegister().x = 0x03;
            // $0000 LDY $0010,X
            mem.writeByte(0x0000, 0xB4);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });
});
