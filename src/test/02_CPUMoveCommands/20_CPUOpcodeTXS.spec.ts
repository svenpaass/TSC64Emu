import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('20: CPU - TXS (transfer X to stack pointer) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('TXS', function () {

        it('test_txs_transfers_x_into_stack_pointer', function () {
            cpu.getRegister().x = 0xAB;
            // $0000 TXS
            mem.writeByte(0x0000, 0x9A);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().sp, 0xAB);
            assert.equal(cpu.getRegister().x, 0xAB);
        });

        it('test_txs_does_not_set_negative_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_N);
            cpu.getRegister().x = 0x80;
            // $0000 TXS
            mem.writeByte(0x0000, 0x9A);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().sp, 0x80);
            assert.equal(cpu.getRegister().x, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_txs_does_not_set_zero_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_Z);
            cpu.getRegister().x = 0x00;
            // $0000 TXS
            mem.writeByte(0x0000, 0x9A);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().sp, 0x00);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });
    });
});
