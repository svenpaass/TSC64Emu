import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('18: CPU - TSX (transfer stack pointer to X) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('TSX', function () {

        it('test_tsx_transfers_stack_pointer_into_x', function () {
            cpu.getRegister().sp = 0xAB;
            cpu.getRegister().x = 0x00;
            // $0000 TSX
            mem.writeByte(0x0000, 0xBA);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().sp, 0xAB);
            assert.equal(cpu.getRegister().x, 0xAB);
        });

        it('test_tsx_sets_negative_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_N);
            cpu.getRegister().sp = 0x80;
            cpu.getRegister().x = 0x00;
            // $0000 TSX
            mem.writeByte(0x0000, 0xBA);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().sp, 0x80);
            assert.equal(cpu.getRegister().x, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });

        it('test_tsx_sets_zero_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_Z);
            cpu.getRegister().sp = 0x00;
            cpu.getRegister().y = 0xFF;
            // $0000 TSX
            mem.writeByte(0x0000, 0xBA);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().sp, 0x00);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        });
    });
});
