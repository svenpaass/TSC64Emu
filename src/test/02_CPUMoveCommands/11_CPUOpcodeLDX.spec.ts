import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('11: CPU - LDX (load X) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('LDX Absolute', function () {
        it('test_ldx_absolute_loads_x_sets_n_flag', function () {
            cpu.getRegister().x = 0x00;
            // $0000 LDX $ABCD
            mem.writeByte(0x0000, 0xAE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().x, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_ldx_absolute_loads_x_sets_z_flag', function () {
            cpu.getRegister().x = 0xff;
            // $0000 LDX $ABCD
            mem.writeByte(0x0000, 0xAE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDX Zero Page', function () {
        it('test_ldx_zp_loads_x_sets_n_flag', function () {
            cpu.getRegister().x = 0x00;
            // $0000 LDX $0010
            mem.writeByte(0x0000, 0xA6);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().x, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_ldx_zp_loads_x_sets_z_flag', function () {
            cpu.getRegister().x = 0xFF;
            // $0000 LDX $0010
            mem.writeByte(0x0000, 0xA6);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDX Immediate', function () {

        it('test_ldx_immediate_loads_x_sets_n_flag', function () {
            cpu.getRegister().x = 0x00;
            // $0000 LDX #$80
            mem.writeByte(0x0000, 0xA2);
            mem.writeByte(0x0001, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().x, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_ldx_immediate_loads_x_sets_z_flag', function () {
            cpu.getRegister().x = 0xFF;
            // $0000 LDX #$00
            mem.writeByte(0x0000, 0xA2);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDX Absolute, Y-Indexed', function () {

        it('test_ldx_abs_y_indexed_loads_x_sets_n_flag', function () {
            cpu.getRegister().x = 0x00;
            cpu.getRegister().y = 0x03;
            // $0000 LDX $ABCD,Y
            mem.writeByte(0x0000, 0xBE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().x, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_ldx_abs_y_indexed_loads_x_sets_z_flag', function () {
            cpu.getRegister().x = 0xFF;
            cpu.getRegister().y = 0x03;
            // $0000 LDX $ABCD,Y
            mem.writeByte(0x0000, 0xBE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });
    });

    describe('LDX Zero Page, Y-Indexed', function () {

        it('test_ldx_zp_y_indexed_loads_x_sets_n_flag', function () {
            cpu.getRegister().x = 0x00;
            cpu.getRegister().y = 0x03;
            // $0000 LDY $0010,X
            mem.writeByte(0x0000, 0xB6);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().y, 0x80);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().x, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        });

        it('test_ldx_zp_y_indexed_loads_x_sets_z_flag', function () {
            cpu.getRegister().x = 0xFF;
            cpu.getRegister().y = 0x03;
            // $0000 LDA $0010,Y
            mem.writeByte(0x0000, 0xB6);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

    });

});
