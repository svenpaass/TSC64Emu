import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('16: CPU - TAX (transfer accumulator to X) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('TAX', function () {

        it('test_tax_transfers_accumulator_into_x', function () {
            cpu.getRegister().ac = 0xAB;
            cpu.getRegister().x = 0x00;
            // $0000 TAX
            mem.writeByte(0x0000, 0xAA);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0xAB);
            assert.equal(cpu.getRegister().x, 0xAB);
        });

        it('test_tax_sets_negative_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_N);
            cpu.getRegister().ac = 0x80;
            cpu.getRegister().x = 0x00;
            // $0000 TAX
            mem.writeByte(0x0000, 0xAA);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().x, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });

        it('test_tax_sets_zero_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_Z);
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0xFF;
            // $0000 TAX
            mem.writeByte(0x0000, 0xAA);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        });
    });
});
