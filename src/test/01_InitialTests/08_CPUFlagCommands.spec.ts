import Register from '../../cpu/Register';
import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('08: CPU Flag Command tests', function () {

  beforeEach(() => {
    mem = new Memory(64 * 1024);
    cpu = new CPU(mem);
  });

  /* BPL  Branch on Result Plus
   * branch on N = 0                  - - - - - -
   */
  describe('BPL', function () {
    it('BPL - N-Flag Set', function () {
      cpu.getRegister().setFlag(Register.FLAG_N);
      mem.writeByte(0x0000, 0x10);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0002);
    });

    it('BPL - N-Flag not Set', function () {
      cpu.getRegister().clearFlag(Register.FLAG_N);
      mem.writeByte(0x0000, 0x10);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0022);
    });
  });

  /* BMI  Branch on Result Minus
   * branch on N = 1                  - - - - - -
   */
  describe('BMI', function () {
    it('BMI - N-Flag Set', function () {
      cpu.getRegister().setFlag(Register.FLAG_N);
      mem.writeByte(0x0000, 0x30);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0022);
    });

    it('BMI - N-Flag not Set', function () {
      cpu.getRegister().clearFlag(Register.FLAG_N);
      mem.writeByte(0x0000, 0x30);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0002);
    });
  });

  /* BVC  Branch on Overflow Clear
   * branch on V = 0                  - - - - - -
   */
  describe('BVC', function () {
    it('BVC - V-Flag Set', function () {
      cpu.getRegister().setFlag(Register.FLAG_V);
      mem.writeByte(0x0000, 0x50);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0002);
    });

    it('BVC - V-Flag not Set', function () {
      cpu.getRegister().clearFlag(Register.FLAG_V);
      mem.writeByte(0x0000, 0x50);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0022);
    });
  });

  /* BVS  Branch on Overflow Set
   * branch on V = 1                  - - - - - -
   */
  describe('BVS', function () {
    it('BVS - V-Flag Set', function () {
      cpu.getRegister().setFlag(Register.FLAG_V);
      mem.writeByte(0x0000, 0x70);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0022);
    });

    it('BVS - V-Flag not Set', function () {
      cpu.getRegister().clearFlag(Register.FLAG_V);
      mem.writeByte(0x0000, 0x70);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0002);
    });
  });

  /* BCC  Branch on Carry Clear
   * branch on C = 0                  - - - - - -
   */
  describe('BCC', function () {
    it('BVC - C-Flag Set', function () {
      cpu.getRegister().setFlag(Register.FLAG_C);
      mem.writeByte(0x0000, 0x90);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0002);
    });

    it('BVC - C-Flag not Set', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      mem.writeByte(0x0000, 0x90);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0022);
    });
  });

  /* BCS  Branch on Carry Set
   * branch on C = 1                  - - - - - -
   */
  describe('BCS', function () {
    it('BCS - C-Flag Set', function () {
      cpu.getRegister().setFlag(Register.FLAG_C);
      mem.writeByte(0x0000, 0xb0);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0022);
    });

    it('BCS - C-Flag not Set', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      mem.writeByte(0x0000, 0xb0);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0002);
    });
  });

  /* BNE  Branch on Result not Zero
   * branch on Z = 0                  - - - - - -
   */
  describe('BNE', function () {
    it('BNE - Z-Flag Set', function () {
      cpu.getRegister().setFlag(Register.FLAG_Z);
      mem.writeByte(0x0000, 0xd0);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0002);
    });

    it('BNE - Z-Flag not Set', function () {
      cpu.getRegister().clearFlag(Register.FLAG_Z);
      mem.writeByte(0x0000, 0xd0);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0022);
    });
  });

  /* BEQ  Branch on Result Zero
   * branch on Z = 1                  - - - - - -
   */
  describe('BEQ', function () {
    it('BEQ - Z-Flag Set', function () {
      cpu.getRegister().setFlag(Register.FLAG_Z);
      mem.writeByte(0x0000, 0xf0);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0022);
    });

    it('BEQ - Z-Flag not Set', function () {
      cpu.getRegister().clearFlag(Register.FLAG_Z);
      mem.writeByte(0x0000, 0xf0);
      mem.writeByte(0x0001, 0x20);

      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0002);
    });
  });

  /* BIT  Test Bits in Memory with Accumulator
   * bits 7 and 6 of operand are transfered to bit 7 and 6 of SR (N,V);
   * the zeroflag is set to the result of operand AND accumulator.
   * A AND M, M7 -> N, M6 -> V        N Z - - - V
   */
  describe('BIT', function () {
    it('BIT - Zero Page Addressing', function () {
      mem.writeByte(0x00d0, 0x54);
      cpu.getRegister().ac = 0x04;

      mem.writeByte(0x0000, 0x24);
      mem.writeByte(0x0001, 0xd0);
      cpu.step();

      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
    });

    it('BIT - Absolute Addressing', function () {
      mem.writeByte(0x15d0, 0x54);
      cpu.getRegister().ac = 0x04;

      mem.writeByte(0x0000, 0x2c);
      mem.writeByte(0x0001, 0xd0);
      mem.writeByte(0x0002, 0x15);
      cpu.step();

      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
    });
  });

  /* CLC  Clear Carry Flag
   * 0 -> C                           - - C - - -
   */
  describe('CLC', function () {
    it('CLC', function () {
      cpu.getRegister().setFlag(Register.FLAG_C);
      mem.writeByte(0x0000, 0x18);
      cpu.step();

      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
    });
  });

  /* SEC  Set Carry Flag
   * 1 -> C                           - - C - - -
   */
  describe('SEC', function () {
    it('SEC', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      mem.writeByte(0x0000, 0x38);
      cpu.step();

      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });

  });

  /* CLD  Clear Decimal Mode
   * 0 -> D                           - - - - D -
   */
  describe('CLD', function () {
    it('CLD', function () {
      cpu.getRegister().setFlag(Register.FLAG_D);
      mem.writeByte(0x0000, 0xd8);
      cpu.step();

      assert.equal(cpu.getRegister().flags & Register.FLAG_D, 0);
    });
  });

  /* SED  Set Decimal Flag
   * 1 -> D                           - - - - D -
   */
  describe('SED', function () {
    it('SED', function () {
      cpu.getRegister().clearFlag(Register.FLAG_D);
      mem.writeByte(0x0000, 0xf8);
      cpu.step();

      assert.equal(cpu.getRegister().flags & Register.FLAG_D, Register.FLAG_D);
    });
  });

  /* CLI  Clear Interrupt Disable Bit
   * 0 -> I                           - - - I - -
   */
  describe('CLI', function () {
    it('CLI', function () {
      cpu.getRegister().setFlag(Register.FLAG_I);
      mem.writeByte(0x0000, 0x58);
      cpu.step();

      assert.equal(cpu.getRegister().flags & Register.FLAG_I, 0);
    });
  });

  /* SEI  Set Interrupt Disable Status
   * 1 -> I                           - - - I - -
   */
  describe('SEI', function () {
    it('SEI', function () {
      cpu.getRegister().clearFlag(Register.FLAG_D);
      mem.writeByte(0x0000, 0x78);
      cpu.step();

      assert.equal(cpu.getRegister().flags & Register.FLAG_I, Register.FLAG_I);
    });
  });

  /* CLV  Clear Overflow Flag
   * 0 -> V                           - - - - - V
   */
  describe('CLV', function () {
    it('CLV', function () {
      cpu.getRegister().setFlag(Register.FLAG_V);
      mem.writeByte(0x0000, 0xb8);
      cpu.step();

      assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
    });
  });

  /* NOP  No Operation
   * ---                              - - - - - -
   */
  describe('NOP', function () {
    it('NOP', function () {
      mem.writeByte(0x0000, 0xea);
      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x0001);
    });
  });

});