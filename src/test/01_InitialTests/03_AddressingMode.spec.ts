import Memory from '../../memory/Memory';
import Register from '../../cpu/Register';
import AddressingMode from '../../cpu/AddressingMode';

import {assert} from 'chai';

let mem : Memory;
let reg : Register;

describe('03: Addressing Mode tests', function() {

  beforeEach(() => {
      mem = new Memory(64 * 1024);
      reg = new Register();
  });

  /* accumulator - OPC A
   * operand is AC
   */
  describe('accumulator - OPC A', function() {
    // calculate effective address (operator is ac => undefined)
    it('should return undefined when calculating accumulator addressing', function() {
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ACCUMULATOR, mem, reg, undefined, undefined);
      assert.equal(effectiveAddress, undefined);
    });
  });

  /* implied - OPC
   * operand implied
   */
  describe('implied - OPC', function() {
    // calculate effective address (no operrands => undefined)
    it('should return undefined when calculating implied addressing', function() {
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.IMPLIED, mem, reg, undefined, undefined);
      assert.equal(effectiveAddress, undefined);
    });
  });

  /* immediate - OPC #$BB
   * operand is byte (BB)
   */
  describe('immediate - OPC #$BB', function() {
    // calculate effective address (operand is next byte => undefined)
    it('should return undefined when calculating immediate addressing', function() {
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.IMMEDIATE, mem, reg, undefined, undefined);
      assert.equal(effectiveAddress, undefined);
    });
  });

  /* relative - OPC $BB
   * branch target is PC + offset (BB), bit 7 signifies negative offset
   */
  describe('relative - OPC $BB', function() {
    // calculate effective address (get next two bytes and read memory at that address)
    it('value 0x33', function() {
      reg.pc = 0x0100;
      const rel = 0x33;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.RELATIVE, mem, reg, rel, undefined);
      assert.equal(effectiveAddress, reg.pc + 0x33);
    });

    it('value -0x33 (negative offset)', function() {
      reg.pc = 0x0100;
      const rel = (0x33 ^ 0xFF) + 1; // -0x33
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.RELATIVE, mem, reg, rel, undefined);
      assert.equal(effectiveAddress, reg.pc - 0x33);
    });
  });

  /* absolute - OPC $HHLL
   * operand is address $HHLL
   */
  describe('absolute - OPC $HHLL', function() {
    // calculate effective address (get next two bytes)
    it('should return 16bit address 0x2211 when calculating absolute addressing', function() {
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ABSOLUTE, mem, reg, 0x11, 0x22);
      assert.equal(effectiveAddress, 0x2211);
    });
  });

  /* absolute, X-indexed - OPC $HHLL,X
   * operand is address incremented by X with carry
   */
  describe('absolute, X-indexed - OPC $HHLL,X', function() {
    // calculate effective address (get next two bytes and add x)
    it('test x=0x33 and input address=0x2211', function() {
      reg.x = 0x33;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ABSOLUTE_INDEXED_BY_X, mem, reg, 0x11, 0x22);
      assert.equal(effectiveAddress, 0x2244);
    });

    it('test x=0xee and input address=0x2222', function() {
      reg.x = 0xee;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ABSOLUTE_INDEXED_BY_X, mem, reg, 0x22, 0x22);
      assert.equal(effectiveAddress, 0x2310);
    });

    it('test x=0xee and input address=0xffee (overflow)', function() {
      reg.x = 0xee;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ABSOLUTE_INDEXED_BY_X, mem, reg, 0xee, 0xff);
      assert.equal(effectiveAddress, 0x00dc);
    });
  });

  /* absolute, Y-indexed - OPC $HHLL,Y
   * operand is address incremented by Y with carry
   */
  describe('absolute, Y-indexed - OPC $HHLL,Y', function() {
    // calculate effective address (get next two bytes and add y)
    it('test y=0x33 and input address=0x2211', function() {
      reg.y = 0x33;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ABSOLUTE_INDEXED_BY_Y, mem, reg, 0x11, 0x22);
      assert.equal(effectiveAddress, 0x2244);
    });

    it('test y=0xee and input address=0x2222', function() {
      reg.y = 0xee;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ABSOLUTE_INDEXED_BY_Y, mem, reg, 0x22, 0x22);
      assert.equal(effectiveAddress, 0x2310);
    });

    it('test y=0xee and input address=0xffee (overflow)', function() {
      reg.y = 0xee;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ABSOLUTE_INDEXED_BY_Y, mem, reg, 0xee, 0xff);
      assert.equal(effectiveAddress, 0x00dc);
    });
  });

  /* indirect - OPC ($HHLL)
   * operand is effective address; effective address is value of address
   */
  describe('indirect - OPC ($HHLL)', function() {
    // calculate effective address (get next two bytes and read memory at that address)
    it('put value 0xffee at memory location 0x1020', function() {
      // write value at location 0x1020
      mem.writeByte(0x1020, 0xee);
      mem.writeByte(0x1021, 0xff);
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ABSOLUTE_INDIRECT, mem, reg, 0x20, 0x10);
      assert.equal(effectiveAddress, 0xffee);
    });
  });


  /* zeropage - OPC $LL
   * operand is of address; address hibyte = zero ($00xx)
   */
  describe('zeropage - OPC $LL', function() {
    it('value 0x33', function() {
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ZERO_PAGE, mem, reg, 0x33, undefined);
      assert.equal(effectiveAddress, 0x0033);
    });
  });

  /* zeropage, X-indexed - OPC $LL,X
   * operand is address incremented by X; address hibyte = zero ($00xx); no page transition
   */
  describe('zeropage, X-indexed - OPC $LL,X', function() {
    it('value 0x33, x=0x33', function() {
      reg.x = 0x33;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ZERO_PAGE_INDEXED_BY_X, mem, reg, 0x33, undefined);
      assert.equal(effectiveAddress, 0x0066);
    });

    it('value 0xee, x=0x33', function() {
      reg.x = 0xee;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ZERO_PAGE_INDEXED_BY_X, mem, reg, 0x33, undefined);
      assert.equal(effectiveAddress, 0x0021);
    });
  });

  /* zeropage, Y-indexed - OPC $LL,Y
   * operand is address incremented by Y; address hibyte = zero ($00xx); no page transition
   */
  describe('zeropage, Y-indexed - OPC $LL,Y', function() {
    it('value 0x33, x=0x33', function() {
      reg.y = 0x33;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ZERO_PAGE_INDEXED_BY_Y, mem, reg, 0x33, undefined);
      assert.equal(effectiveAddress, 0x0066);
    });

    it('value 0xee, x=0x33', function() {
      reg.y = 0xee;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ZERO_PAGE_INDEXED_BY_Y, mem, reg, 0x33, undefined);
      assert.equal(effectiveAddress, 0x0021);
    });
  });

  /* indirect, Y-indexed - OPC ($LL),Y
   * operand is effective address incremented by Y with carry; effective address is word at zeropage address
   */
  describe('indirect, Y-indexed - OPC ($LL),Y', function() {
    it('value 0x20 at memory location 0x00f5 (x=0x10, arg1=0xe5)', function() {
      mem.writeByte(0x00e5, 0x20);
      mem.writeByte(0x00e6, 0x10);
      reg.y = 0x10;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED, mem, reg, 0xe5, undefined);
      assert.equal(effectiveAddress, 0x1030); // 0x00e5=$20 + y=$10
    });

    // calculate effective address (get next two bytes and read memory at that address)
    it('value 0x20 at memory location 0x0010 (y=0xf0, arg1=0x20)', function() {
      mem.writeByte(0x0020, 0x20);
      mem.writeByte(0x0021, 0x10);

      reg.y = 0xf0;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED, mem, reg, 0x20, undefined);
      assert.equal(effectiveAddress, 0x1110);
    });
  });

  /* X-indexed, indirect - OPC ($BB,X)
   * operand is effective zeropage address; effective address is byte (BB) incremented by X without carry
   */
  describe('X-indexed, indirect - OPC ($BB,X)', function() {
    // calculate effective address (get next two bytes and read memory at that address)
    it('value 0x20 at memory location 0x00f5 (x=0x10, arg1=0xe5)', function() {
      mem.writeByte(0x00f5, 0x20);

      reg.x = 0x10;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT, mem, reg, 0xe5, undefined);
      assert.equal(effectiveAddress, 0x0020);
    });

    // calculate effective address (get next two bytes and read memory at that address)
    it('value 0x20 at memory location 0x0010 (x=0xf0, arg1=0x20)', function() {
      mem.writeByte(0x0010, 0x20);

      reg.x = 0xf0;
      let effectiveAddress = AddressingMode.calculateEffectiveAdd(0x00, AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT, mem, reg, 0x20, undefined);
      assert.equal(effectiveAddress, 0x0020);
    });
  });

});
