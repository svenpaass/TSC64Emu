import Memory from '../../memory/Memory';

import {assert} from 'chai';
let mem = new Memory(64 * 1024);

describe('01: Memory tests', function() {
  it('should write and read a byte', function() {
    mem.writeByte(0x0000, 0x5f);
    assert.equal(mem.readByte(0), 0x5f);
  });

  it('should read a word in big-endian', function() {
    mem.writeByte(0x0000, 0x01);
    mem.writeByte(0x0001, 0x02);
    assert.equal(mem.readWord(0), 0x0102);
  });

  it('should write a word in big-endian', function () {
    mem.writeWord(0x0100, 0x0304);
    assert.equal(mem.readByte(0x0100), 0x03);
    assert.equal(mem.readByte(0x0101), 0x04);
  });

  it('should read a word in little-endian', function() {
    mem = new Memory(64 * 1024, Memory.LITTLE_ENDIAN);
    mem.writeByte(0x0000, 0x02);
    mem.writeByte(0x0001, 0x01);
    assert.equal(mem.readWord(0), 0x0102);
  });

  it('should write a word in little-endian', function () {
    mem = new Memory(64 * 1024, Memory.LITTLE_ENDIAN);
    mem.writeWord(0x0100, 0x0304);
    assert.equal(mem.readByte(0x0100), 0x04);
    assert.equal(mem.readByte(0x0101), 0x03);
  });


});