import Register from '../../cpu/Register';
import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('05: CPU Arithmetic Command tests', function () {

  beforeEach(() => {
    mem = new Memory(64 * 1024);
    cpu = new CPU(mem);
  });

  /* ADC  Add Memory to Accumulator with Carry
   * A + M + C -> A, C                N Z C - - V
   */
  describe('ADC', function () {
    it('ADC #$10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x20;
      mem.writeByte(0x0000, 0x69);
      mem.writeByte(0x0001, 0x05);
      cpu.step();

      assert.equal(cpu.getRegister().ac, 0x25);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('ADC $10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x20;
      mem.writeByte(0x0010, 0x05);
      mem.writeByte(0x0000, 0x65);
      mem.writeByte(0x0001, 0x10);
      cpu.step();

      assert.equal(cpu.getRegister().ac, 0x25);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('ADC #$10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x01;
      mem.writeByte(0x0000, 0x69);
      mem.writeByte(0x0001, 0x7f);
      cpu.step();

      assert.equal(cpu.getRegister().ac, 0x80);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('ADC #$10', function () {
      cpu.getRegister().setFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x01;
      mem.writeByte(0x0000, 0x69);
      mem.writeByte(0x0001, 0x7e);
      cpu.step();

      assert.equal(cpu.getRegister().ac, 0x80);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
      // functional test => carry has to be clear
      //assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
    });
  });

  /* SBC  Subtract Memory from Accumulator with Borrow
   * A - M - C -> A                   N Z C - - V
   */
  describe('SBC', function () {
    it('SBC #$10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x20;
      mem.writeByte(0x0000, 0xe9);
      mem.writeByte(0x0001, 0x05);
      cpu.step();

      assert.equal(cpu.getRegister().ac, 0x1a);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
      assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
    });

    it('SBC #$10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x10;
      mem.writeByte(0x0010, 0x10);
      mem.writeByte(0x0000, 0xe5);
      mem.writeByte(0x0001, 0x10);
      cpu.step();

      assert.equal(cpu.getRegister().ac, 0xff);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
    });

    it('SBC #$10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x02;
      mem.writeByte(0x0000, 0xe9);
      mem.writeByte(0x0001, 0x01);
      cpu.step();

      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
      assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
    });

    it('SBC #$10', function () {
      cpu.getRegister().setFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x01;
      mem.writeByte(0x0000, 0xe9);
      mem.writeByte(0x0001, 0x01);
      cpu.step();

      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
      assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
    });
  });

  /* CMP  Compare Memory with Accumulator
   * A - M                            N Z C - - -
   */
  describe('CMP', function () {
    it('CMP #$20', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x20;
      mem.writeByte(0x0000, 0xc9);
      mem.writeByte(0x0001, 0x20);
      cpu.step();

      assert.equal(cpu.getRegister().ac, 0x20);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });

    it('CMP $10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x10;
      mem.writeByte(0x0000, 0xc5);
      mem.writeByte(0x0001, 0x10);
      mem.writeByte(0x0010, 0x40);
      cpu.step();

      assert.equal(cpu.getRegister().ac, 0x10);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
    });
  });

  /* CPX  Compare Memory and Index X
   * X - M                            N Z C - - -
   */
  describe('CPX', function () {
    it('CPX #$10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().x = 0x20;
      mem.writeByte(0x0000, 0xe0);
      mem.writeByte(0x0001, 0x20);
      cpu.step();

      assert.equal(cpu.getRegister().x, 0x20);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });

    it('CPX $10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().x = 0x10;
      mem.writeByte(0x0000, 0xe4);
      mem.writeByte(0x0001, 0x10);
      mem.writeByte(0x0010, 0x40);
      cpu.step();

      assert.equal(cpu.getRegister().x, 0x10);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
    });
  });

  /* CPY  Compare Memory and Index Y
   * Y - M                            N Z C - - -
   */
  describe('CPY', function () {
    it('CPY #$10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().y = 0x20;
      mem.writeByte(0x0000, 0xc0);
      mem.writeByte(0x0001, 0x20);
      cpu.step();

      assert.equal(cpu.getRegister().y, 0x20);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });

    it('CPY $10', function () {
      cpu.getRegister().clearFlag(Register.FLAG_C);
      cpu.getRegister().y = 0x10;
      mem.writeByte(0x0000, 0xc4);
      mem.writeByte(0x0001, 0x10);
      mem.writeByte(0x0010, 0x40);
      cpu.step();

      assert.equal(cpu.getRegister().y, 0x10);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
    });
  });

  /* DEC  Decrement Memory by One
   * M - 1 -> M                       N Z - - - -
   */
  describe('DEC', function () {
    it('DEC $10 - value 0x05', function () {
      mem.writeByte(0x0010, 0x05);
      mem.writeByte(0x0000, 0xc6);
      mem.writeByte(0x0001, 0x10);
      cpu.step();

      assert.equal(mem.readByte(0x0010), 0x04);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('DEC $10 - value 0x01', function () {
      mem.writeByte(0x0010, 0x01);
      mem.writeByte(0x0000, 0xc6);
      mem.writeByte(0x0001, 0x10);
      cpu.step();

      assert.equal(mem.readByte(0x0010), 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });

    it('DEC $10 - value 0x00', function () {
      mem.writeByte(0x0010, 0x00);
      mem.writeByte(0x0000, 0xc6);
      mem.writeByte(0x0001, 0x10);
      cpu.step();

      assert.equal(mem.readByte(0x0010), 0xff);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });
  });

  /* DEX  Decrement Index X by One
   * X - 1 -> X                       N Z - - - -
   */
  describe('DEX', function () {
    it('DEX - value 0x05', function () {
      cpu.getRegister().x = 0x05;
      mem.writeByte(0x0000, 0xca);
      cpu.step();

      assert.equal(cpu.getRegister().x, 0x04);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('DEX - value 0x01', function () {
      cpu.getRegister().x = 0x01;
      mem.writeByte(0x0000, 0xca);
      cpu.step();

      assert.equal(cpu.getRegister().x, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });

    it('DEX - value 0x00', function () {
      cpu.getRegister().x = 0x00;
      mem.writeByte(0x0000, 0xca);
      cpu.step();

      assert.equal(cpu.getRegister().x, 0xff);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });
  });

  /* DEY  Decrement Index Y by One
   * Y - 1 -> Y                       N Z - - - -
   */
  describe('DEY', function () {
    it('DEY - value 0x05', function () {
      cpu.getRegister().y = 0x05;
      mem.writeByte(0x0000, 0x88);
      cpu.step();

      assert.equal(cpu.getRegister().y, 0x04);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('DEY - value 0x01', function () {
      cpu.getRegister().y = 0x01;
      mem.writeByte(0x0000, 0x88);
      cpu.step();

      assert.equal(cpu.getRegister().y, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });

    it('DEY - value 0x00', function () {
      cpu.getRegister().y = 0x00;
      mem.writeByte(0x0000, 0x88);
      cpu.step();

      assert.equal(cpu.getRegister().y, 0xff);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });
  });

  /* INC  Increment Memory by One
   * M + 1 -> M                       N Z - - - -
   */
  describe('INC', function () {
    it('INC $10 - value 0x05', function () {
      mem.writeByte(0x0010, 0x05);
      mem.writeByte(0x0000, 0xe6);
      mem.writeByte(0x0001, 0x10);
      cpu.step();

      assert.equal(mem.readByte(0x0010), 0x06);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('INC $10 - value 0xff', function () {
      mem.writeByte(0x0010, 0xff);
      mem.writeByte(0x0000, 0xe6);
      mem.writeByte(0x0001, 0x10);
      cpu.step();

      assert.equal(mem.readByte(0x0010), 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });

    it('INC $10 - value 0x7f', function () {
      mem.writeByte(0x0010, 0x7f);
      mem.writeByte(0x0000, 0xe6);
      mem.writeByte(0x0001, 0x10);
      cpu.step();

      assert.equal(mem.readByte(0x0010), 0x80);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });
  });

  /* INX  Increment Index X by One
   * X + 1 -> X                       N Z - - - -
   */
  describe('INX', function () {
    it('INX - value 0x05', function () {
      cpu.getRegister().x = 0x05;
      mem.writeByte(0x0000, 0xe8);
      cpu.step();

      assert.equal(cpu.getRegister().x, 0x06);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('INX - value 0xff', function () {
      cpu.getRegister().x = 0xff;
      mem.writeByte(0x0000, 0xe8);
      cpu.step();

      assert.equal(cpu.getRegister().x, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });

    it('INX - value 0x7f', function () {
      cpu.getRegister().x = 0x7f;
      mem.writeByte(0x0000, 0xe8);
      cpu.step();

      assert.equal(cpu.getRegister().x, 0x80);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });
  });

  /* INY  Increment Index Y by One
   * Y + 1 -> Y                       N Z - - - -
   */
  describe('INY', function () {
    it('INY - value 0x05', function () {
      cpu.getRegister().y = 0x05;
      mem.writeByte(0x0000, 0xc8);
      cpu.step();

      assert.equal(cpu.getRegister().y, 0x06);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('INY - value 0xff', function () {
      cpu.getRegister().y = 0xff;
      mem.writeByte(0x0000, 0xc8);
      cpu.step();

      assert.equal(cpu.getRegister().y, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });

    it('INY - value 0x7f', function () {
      cpu.getRegister().y = 0x7f;
      mem.writeByte(0x0000, 0xc8);
      cpu.step();

      assert.equal(cpu.getRegister().y, 0x80);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });
  });

});