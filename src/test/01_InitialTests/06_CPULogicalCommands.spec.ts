import Register from '../../cpu/Register';
import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import {assert} from 'chai';
let mem: Memory;
let cpu: CPU;

describe('06: CPU Logical Command tests', function () {

  beforeEach(() => {
    mem = new Memory(64 * 1024);
    cpu = new CPU(mem);
  });

  /* ORA  OR Memory with Accumulator
   * A OR M -> A                      N Z - - - -
   */
  describe('ORA', function () {
    it('ORA #$15', function () {
      cpu.getRegister().ac = 0x00;
      mem.writeByte(0x0000, 0x09);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x15);
    });

    it('ORA #$15 - NZ-Flags', function () {
      cpu.getRegister().ac = 0x00;
      mem.writeByte(0x0000, 0x09);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x15);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('ORA #$80 - NZ-Flags', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x09);
      mem.writeByte(0x0001, 0x80);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xaf);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('ORA #$00 - flags', function () {
      cpu.getRegister().ac = 0x00;
      mem.writeByte(0x0000, 0x09);
      mem.writeByte(0x0001, 0x00);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  /* AND  AND Memory with Accumulator
   * A AND M -> A                     N Z - - - -
   */
  describe('AND', function () {
    it('AND #$15', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x29);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x05);
    });

    it('AND #$15 - NZ-Flags', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x29);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x05);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('AND #$80 - NZ-Flags', function () {
      cpu.getRegister().ac = 0xae;
      mem.writeByte(0x0000, 0x29);
      mem.writeByte(0x0001, 0x80);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x80);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('AND #$00 - flags', function () {
      cpu.getRegister().ac = 0xfe;
      mem.writeByte(0x0000, 0x29);
      mem.writeByte(0x0001, 0x00);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  /* EOR  Exclusive-OR Memory with Accumulator
   * A EOR M -> A                     N Z - - - -
   */
  describe('EOR', function () {
    it('EOR #$15', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x49);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x3a);
    });

    it('EOR #$15 - NZ-Flags', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x49);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x3a);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('EOR #$80 - NZ-Flags', function () {
      cpu.getRegister().ac = 0x75;
      mem.writeByte(0x0000, 0x49);
      mem.writeByte(0x0001, 0x80);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xf5);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('EOR #$FE - NZ-Flags', function () {
      cpu.getRegister().ac = 0xfe;
      mem.writeByte(0x0000, 0x49);
      mem.writeByte(0x0001, 0xfe);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  /* ASL  Shift Left One Bit (Memory or Accumulator)
   * C <- [76543210] <- 0             N Z C - - -
   */
  describe('ASL', function () {
    it('ASL', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x0a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x5e);
    });

    it('ASL - NZ-Flags', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x0a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x5e);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('ASL - NZ-Flags', function () {
      cpu.getRegister().ac = 0x75;
      mem.writeByte(0x0000, 0x0a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xea);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('ASL - NZ-Flags', function () {
      cpu.getRegister().ac = 0x80;
      mem.writeByte(0x0000, 0x0a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });
  });

  /* ROL  Rotate One Bit Left (Memory or Accumulator)
   * C <- [76543210] <- C             N Z C - - -
   */
  describe('ROL', function () {
    it('ROL', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x2a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x5e);
    });

    it('ROL - NZ-Flags', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x2a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x5e);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
    });

    it('ROL - NZ-Flags', function () {
      cpu.getRegister().setFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x75;
      mem.writeByte(0x0000, 0x2a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xeb);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
    });

    it('ROL - NZ-Flags', function () {
      cpu.getRegister().ac = 0xeb;
      mem.writeByte(0x0000, 0x2a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xd6);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });

    it('ROL - NZ-Flags', function () {
      cpu.getRegister().ac = 0x80;
      mem.writeByte(0x0000, 0x2a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });
  });

  /* LSR  Shift One Bit Right (Memory or Accumulator)
   * 0 -> [76543210] -> C             - Z C - - -
   */
  describe('LSR', function () {
    it('LSR', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x4a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x17);
    });

    it('LSR - NZ-Flags', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x4a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x17);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });

    it('LSR - NZ-Flags', function () {
      cpu.getRegister().ac = 0x01;
      mem.writeByte(0x0000, 0x4a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });
  });

  /* ROR  Rotate One Bit Right (Memory or Accumulator)
   * C -> [76543210] -> C             N Z C - - -
   */
  describe('ROR', function () {
    it('ROR', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x6a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x17);
    });

    it('ROR - NZ-Flags', function () {
      cpu.getRegister().ac = 0x2f;
      mem.writeByte(0x0000, 0x6a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x17);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });

    it('ROR - NZ-Flags', function () {
      cpu.getRegister().setFlag(Register.FLAG_C);
      cpu.getRegister().ac = 0x74;
      mem.writeByte(0x0000, 0x6a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xba);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
    });

    it('ROR - NZ-Flags', function () {
      cpu.getRegister().ac = 0xeb;
      mem.writeByte(0x0000, 0x6a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x75);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });

    it('ROR - NZ-Flags', function () {
      cpu.getRegister().ac = 0x01;
      mem.writeByte(0x0000, 0x6a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    });
  });

});