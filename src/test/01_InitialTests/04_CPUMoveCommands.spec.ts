import Register from '../../cpu/Register';
import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('04: CPU Move Command tests', function () {

  beforeEach(() => {
    mem = new Memory(64 * 1024);
    cpu = new CPU(mem);
  });

  /* LDA  Load Accumulator with Memory
   * M -> A                           N Z - - - -
   */
  describe('LDA', function () {
    it('LDA #$15', function () {
      mem.writeByte(0x0000, 0xa9);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x15);
    });

    it('LDA #$15 - NZ-Flags', function () {
      mem.writeByte(0x0000, 0xa9);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x15);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('LDA #$af - NZ-Flags', function () {
      mem.writeByte(0x0000, 0xa9);
      mem.writeByte(0x0001, 0xaf);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xaf);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('LDA #$00 - flags', function () {
      mem.writeByte(0x0000, 0xa9);
      mem.writeByte(0x0001, 0x00);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  /* STA  Store Accumulator in Memory
   * A -> M                           - - - - - -
   */
  describe('STA', function () {
    it('STA $ef', function () {
      cpu.getRegister().ac = 0x55;
      mem.writeByte(0x0000, 0x85);
      mem.writeByte(0x0001, 0xef);
      cpu.step();
      assert.equal(mem.readByte(0x00ef), 0x55);
    });

    it('STA $23ef', function () {
      cpu.getRegister().ac = 0x55;
      mem.writeByte(0x0000, 0x8d);
      mem.writeByte(0x0001, 0xef);
      mem.writeByte(0x0002, 0x23);
      cpu.step();
      assert.equal(mem.readByte(0x23ef), 0x55);
    });
  });

  /* LDX  Load Index X with Memory
   * M -> X                           N Z - - - -
   */
  describe('LDX', function () {
    it('LDX #$15', function () {
      cpu.getRegister().x = 0x00;
      mem.writeByte(0x0000, 0xa2);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().x, 0x15);
    });

    it('LDX #$15 - NZ-Flags', function () {
      cpu.getRegister().x = 0x00;
      mem.writeByte(0x0000, 0xa2);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().x, 0x15);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('LDX #$af - NZ-Flags', function () {
      cpu.getRegister().x = 0x00;
      mem.writeByte(0x0000, 0xa2);
      mem.writeByte(0x0001, 0xaf);
      cpu.step();
      assert.equal(cpu.getRegister().x, 0xaf);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('LDX #$00 - NZ-Flags', function () {
      cpu.getRegister().x = 0x55;
      mem.writeByte(0x0000, 0xa2);
      mem.writeByte(0x0001, 0x00);
      cpu.step();
      assert.equal(cpu.getRegister().x, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  /* STX  Store Index X in Memory
   * X -> M                           - - - - - -
   */
  describe('STX', function () {
    it('STX $ef', function () {
      cpu.getRegister().x = 0x55;
      mem.writeByte(0x0000, 0x86);
      mem.writeByte(0x0001, 0xef);
      cpu.step();
      assert.equal(mem.readByte(0x00ef), 0x55);
    });

    it('STX $ef,y', function () {
      cpu.getRegister().y = 0x01;
      cpu.getRegister().x = 0x45;
      mem.writeByte(0x0000, 0x96);
      mem.writeByte(0x0001, 0xef);
      cpu.step();
      assert.equal(mem.readByte(0x00f0), 0x45);
    });

    it('STX $12ef', function () {
      cpu.getRegister().x = 0x55;
      mem.writeByte(0x0000, 0x8e);
      mem.writeByte(0x0001, 0xef);
      mem.writeByte(0x0002, 0x12);
      cpu.step();
      assert.equal(mem.readByte(0x12ef), 0x55);
    });

  });

  /* LDY  Load Index Y with Memory
   * M -> Y                           N Z - - - -
   */
  describe('LDY', function () {
    it('LDY #$15', function () {
      cpu.getRegister().y = 0x00;
      mem.writeByte(0x0000, 0xa0);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().y, 0x15);
    });

    it('LDY #$15 - NZ-Flags', function () {
      cpu.getRegister().y = 0x00;
      mem.writeByte(0x0000, 0xa0);
      mem.writeByte(0x0001, 0x15);
      cpu.step();
      assert.equal(cpu.getRegister().y, 0x15);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('LDY #$af - NZ-Flags', function () {
      cpu.getRegister().y = 0x00;
      mem.writeByte(0x0000, 0xa0);
      mem.writeByte(0x0001, 0xaf);
      cpu.step();
      assert.equal(cpu.getRegister().y, 0xaf);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('LDY #$00 - NZ-Flags', function () {
      cpu.getRegister().y = 0x55;
      mem.writeByte(0x0000, 0xa0);
      mem.writeByte(0x0001, 0x00);
      cpu.step();
      assert.equal(cpu.getRegister().y, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  /* STY  Sore Index Y in Memory
   * Y -> M                           - - - - - -
   */
  describe('STY', function () {
    it('STY $ef', function () {
      cpu.getRegister().y = 0x55;
      mem.writeByte(0x0000, 0x84);
      mem.writeByte(0x0001, 0xef);
      cpu.step();
      assert.equal(mem.readByte(0x00ef), 0x55);
    });

    it('STY $ef,x', function () {
      cpu.getRegister().x = 0x01;
      cpu.getRegister().y = 0x25;
      mem.writeByte(0x0000, 0x94);
      mem.writeByte(0x0001, 0xef);
      cpu.step();
      assert.equal(mem.readByte(0x00f0), 0x25);
    });

    it('STY $73ef', function () {
      cpu.getRegister().y = 0x55;
      mem.writeByte(0x0000, 0x8c);
      mem.writeByte(0x0001, 0xef);
      mem.writeByte(0x0002, 0x73);
      cpu.step();
      assert.equal(mem.readByte(0x73ef), 0x55);
    });

  });

  /* TAX  Transfer Accumulator to Index X
   * A -> X                           N Z - - - -
   */
  describe('TAX', function () {
    it('TAX #$15', function () {
      cpu.getRegister().ac = 0x15;
      cpu.getRegister().x = 0x00;
      mem.writeByte(0x0000, 0xaa);
      cpu.step();
      assert.equal(cpu.getRegister().x, 0x15);
    });

    it('TAX #$15 - NZ-Flags', function () {
      cpu.getRegister().ac = 0x15;
      cpu.getRegister().x = 0x00;
      mem.writeByte(0x0000, 0xaa);
      cpu.step();
      assert.equal(cpu.getRegister().x, 0x15);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('TAX #$af - NZ-Flags', function () {
      cpu.getRegister().ac = 0xaf;
      cpu.getRegister().x = 0x00;
      mem.writeByte(0x0000, 0xaa);
      cpu.step();
      assert.equal(cpu.getRegister().x, 0xaf);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('TAX #$00 - NZ-Flags', function () {
      cpu.getRegister().ac = 0x00;
      cpu.getRegister().x = 0x55;
      mem.writeByte(0x0000, 0xaa);
      cpu.step();
      assert.equal(cpu.getRegister().x, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  /* TXA  Transfer Index X to Accumulator
   * X -> A                           N Z - - - -
   */
  describe('TXA', function () {
    it('TXA #$15', function () {
      cpu.getRegister().x = 0x15;
      cpu.getRegister().ac = 0x00;
      mem.writeByte(0x0000, 0x8a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x15);
    });

    it('TXA #$15 - NZ-Flags', function () {
      cpu.getRegister().x = 0x15;
      cpu.getRegister().ac = 0x00;
      mem.writeByte(0x0000, 0x8a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x15);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('TXA #$af - NZ-Flags', function () {
      cpu.getRegister().x = 0xaf;
      cpu.getRegister().ac = 0x00;
      mem.writeByte(0x0000, 0x8a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xaf);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('TXA #$00 - NZ-Flags', function () {
      cpu.getRegister().x = 0x00;
      cpu.getRegister().ac = 0x55;
      mem.writeByte(0x0000, 0x8a);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  /* TAY  Transfer Accumulator to Index Y
   * A -> Y                           N Z - - - -
   */
  describe('TAY', function () {
    it('TAY #$15', function () {
      cpu.getRegister().ac = 0x15;
      cpu.getRegister().y = 0x00;
      mem.writeByte(0x0000, 0xa8);
      cpu.step();
      assert.equal(cpu.getRegister().y, 0x15);
    });

    it('TAY #$15 - NZ-Flags', function () {
      cpu.getRegister().ac = 0x15;
      cpu.getRegister().y = 0x00;
      mem.writeByte(0x0000, 0xa8);
      cpu.step();
      assert.equal(cpu.getRegister().y, 0x15);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('TAY #$af - NZ-Flags', function () {
      cpu.getRegister().ac = 0xaf;
      cpu.getRegister().y = 0x00;
      mem.writeByte(0x0000, 0xa8);
      cpu.step();
      assert.equal(cpu.getRegister().y, 0xaf);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('TAY #$00 - NZ-Flags', function () {
      cpu.getRegister().ac = 0x00;
      cpu.getRegister().y = 0x55;
      mem.writeByte(0x0000, 0xa8);
      cpu.step();
      assert.equal(cpu.getRegister().y, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  /* TYA  Transfer Index Y to Accumulator
   * Y -> A                           N Z - - - -
   */
  describe('TYA', function () {
    it('TYA #$15', function () {
      cpu.getRegister().y = 0x15;
      cpu.getRegister().ac = 0x00;
      mem.writeByte(0x0000, 0x98);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x15);
    });

    it('TYA #$15 - NZ-Flags', function () {
      cpu.getRegister().y = 0x15;
      cpu.getRegister().ac = 0x00;
      mem.writeByte(0x0000, 0x98);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x15);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('TYA #$af - NZ-Flags', function () {
      cpu.getRegister().y = 0xaf;
      cpu.getRegister().ac = 0x00;
      mem.writeByte(0x0000, 0x98);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xaf);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('TYA #$00 - NZ-Flags', function () {
      cpu.getRegister().y = 0x00;
      cpu.getRegister().ac = 0x55;
      mem.writeByte(0x0000, 0x98);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  /* TSX  Transfer Stack Pointer to Index X (doesn't set N,Z)
   * SP -> X                         N Z - - - -
   */
  describe('TSX', function () {
    it('TSX #$15', function () {
      cpu.getRegister().sp = 0x15;
      cpu.getRegister().x = 0x00;
      mem.writeByte(0x0000, 0xba);
      cpu.step();
      assert.equal(cpu.getRegister().x, 0x15);
    });
  });

  /* TXS  Transfer Index X to Stack Register
   * X -> SP                          N Z - - - -
   */
  describe('TXS', function () {
    it('TXS #$15', function () {
      cpu.getRegister().x = 0x15;
      cpu.getRegister().sp = 0x00;
      mem.writeByte(0x0000, 0x9a);
      cpu.step();
      assert.equal(cpu.getRegister().sp, 0x15);
    });
  });


  /* PLA  Pull Accumulator from Stack
   * pull A                           N Z - - - -
   */
  describe('PLA', function () {
    it('PLA - NZ-Flags $15', function () {
      cpu.getRegister().ac = 0xff;
      cpu.getRegister().sp = 0xff;
      mem.writeByte(0x0100, 0x15);

      mem.writeByte(0x0000, 0x68);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x15);
      assert.equal(cpu.getRegister().sp, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('PLA - NZ-Flags $af', function () {
      cpu.getRegister().ac = 0xff;
      cpu.getRegister().sp = 0xff;
      mem.writeByte(0x0100, 0xaf);

      mem.writeByte(0x0000, 0x68);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xaf);
      assert.equal(cpu.getRegister().sp, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('PLA - NZ-Flags $00', function () {
      cpu.getRegister().ac = 0xff;
      cpu.getRegister().sp = 0xff;
      mem.writeByte(0x0100, 0x00);

      mem.writeByte(0x0000, 0x68);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().sp, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });

  });

  /* PHA  Push Accumulator on Stack
   * push A                           - - - - - -
   */
  describe('PHA', function () {
    it('PHA - AC=$15', function () {
      cpu.getRegister().sp = 0x00;
      cpu.getRegister().ac = 0x15;
      mem.writeByte(0x0000, 0x48);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x15);
      assert.equal(mem.readByte(0x100), 0x15);
      assert.equal(cpu.getRegister().sp, 0xff);
    });
  });

  /* PLP  Pull Processor Status from Stack
   * pull SR                          from stack
   */
  describe('PLP', function () {
    it('PLP - $ff', function () {
      cpu.getRegister().sp = 0xff;
      cpu.getRegister().flags = 0x00;
      mem.writeByte(0x0100, 0xff);

      mem.writeByte(0x0000, 0x28);
      cpu.step();
      assert.equal(cpu.getRegister().flags, 0xff);
      assert.equal(cpu.getRegister().sp, 0x00);
    });

    it('PLP - $00', function () {
      cpu.getRegister().sp = 0xff;
      cpu.getRegister().flags = 0xff;
      mem.writeByte(0x0100, 0x00);

      mem.writeByte(0x0000, 0x28);
      cpu.step();
      assert.equal(cpu.getRegister().flags, 0x00);
      assert.equal(cpu.getRegister().sp, 0x00);
    });
  });

  /* PHP  Push Processor Status on Stack
   * push SR                          - - - - - -
   */
  describe('PHP', function () {
    it('PHP - SR=$ff', function () {
      cpu.getRegister().sp = 0xff;
      cpu.getRegister().flags = 0xff;
      mem.writeByte(0x0000, 0x08);
      cpu.step();
      assert.equal(cpu.getRegister().flags, 0xff);
      assert.equal(mem.readByte(0x1ff), 0xff);
      assert.equal(cpu.getRegister().sp, 0xfe);
    });

  });

});