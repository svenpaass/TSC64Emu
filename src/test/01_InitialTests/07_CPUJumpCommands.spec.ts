import Register from '../../cpu/Register';
import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('07: CPU Jump Command tests', function () {

  beforeEach(() => {
    mem = new Memory(64 * 1024);
    cpu = new CPU(mem);
  });

  /* BRK  Force Break
   * interrupt, push PC+2, push SR    - - - I - -
   */
  describe('BRK', function () {
    it('BRK', function () {
      mem.writeByte(0xfffe, 0x55);
      mem.writeByte(0xffff, 0x44);
      cpu.getRegister().sp = 0xff;
      cpu.getRegister().flags = 0x84;
      cpu.getRegister().pc = 0x1020;

      mem.writeByte(0x1020, 0x00);
      cpu.step();

      assert.equal(cpu.getRegister().flags & Register.FLAG_B, Register.FLAG_B);
      assert.equal(cpu.getRegister().flags & Register.FLAG_1, Register.FLAG_1);
      assert.equal(mem.readByte(0x01ff), 0x10);
      assert.equal(mem.readByte(0x01fe), 0x22);
      assert.equal(mem.readByte(0x01fd), 0xB4);
      assert.equal(cpu.getRegister().sp, 0xfc);
      assert.equal(cpu.getRegister().pc, 0x4455);
    });
  });

  /* RTI  Return from Interrupt
   * pull SR, pull PC                 from stack
   */
  describe('RTI', function () {
    it('RTI', function () {
      mem.writeByte(0x01ff, 0x10);
      mem.writeByte(0x01fe, 0x22);
      mem.writeByte(0x01fd, 0x94);
      cpu.getRegister().sp = 0xfc;
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().pc = 0x4455;

      mem.writeByte(0x4455, 0x40);
      cpu.step();

      assert.equal(cpu.getRegister().flags, 0xB4);
      assert.equal(cpu.getRegister().pc, 0x1022);
      assert.equal(cpu.getRegister().sp, 0xff);
    });
  });

  /* JSR  Jump to New Location Saving Return Address
   * push (PC+2),                     - - - - - -
   * (PC+1) -> PCL, (PC+2) -> PCH
   */
  describe('JSR', function () {
    it('JSR #$1234', function () {
      cpu.getRegister().pc = 0x1122;
      mem.writeByte(0x1122, 0x20);
      mem.writeByte(0x1123, 0x34);
      mem.writeByte(0x1124, 0x12);  // return address 0x1125 => Stack: 0x1124
      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x1234);
      assert.equal(cpu.getRegister().sp, 0xfd);
      assert.equal(mem.readByte(0x01fe), 0x24);
      assert.equal(mem.readByte(0x01ff), 0x11);
    });
  });

  /* RTS  Return from Subroutine
   * pull PC, PC+1 -> PC              - - - - - -
   */
  describe('RTS', function () {
    it('RTS', function () {
      cpu.getRegister().sp = 0xfe;
      mem.writeByte(0x0100, 0x11); // Return Address: 0x1126 - 1
      mem.writeByte(0x01ff, 0x25);

      cpu.getRegister().pc = 0x1122;
      mem.writeByte(0x1122, 0x60);
      cpu.step();

      assert.equal(cpu.getRegister().pc, 0x1126); // new PC: 0x1126
      assert.equal(cpu.getRegister().sp, 0x00);
    });
  });

  /* JMP  Jump to New Location
   * (PC+1) -> PCL, (PC+2) -> PCH     - - - - - -
   */
  describe('JMP', function () {
    it('JMP #$1234', function () {
      mem.writeByte(0x0000, 0x4c);
      mem.writeByte(0x0001, 0x34);
      mem.writeByte(0x0002, 0x12);
      cpu.step();
      assert.equal(cpu.getRegister().pc, 0x1234);
    });

    it('JMP ($1234)', function () {
      mem.writeByte(0x0000, 0x6c);
      mem.writeByte(0x0001, 0x34);
      mem.writeByte(0x0002, 0x12);

      mem.writeByte(0x1234, 0xff);
      mem.writeByte(0x1235, 0xee);

      cpu.step();
      assert.equal(cpu.getRegister().pc, 0xeeff);
    });
  });

});