import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('09: CPU Interrupt Vector tests', function () {

  beforeEach(() => {
    mem = new Memory(64 * 1024);
    cpu = new CPU(mem);
  });

  /* CPU Reset
   * load PC from $FFFC/$FFFD
   */
  describe('CPU Reset - PC value', function () {
    it('CPU Reset', function () {
      mem.writeByte(0xfffc, 0x22);
      mem.writeByte(0xfffd, 0x11);

      cpu.reset();

      assert.equal(cpu.getRegister().pc, 0x1122);
    });

  });
});