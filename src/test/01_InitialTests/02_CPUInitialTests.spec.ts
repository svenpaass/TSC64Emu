import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('02: CPU Initial Instruction tests', function () {
  beforeEach(() => {
    mem = new Memory(64 * 1024);
    cpu = new CPU(mem);
  });

  describe('negative flag', function () {

    it('test_negative_flag_value', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0x80);
    })

    it('test_negative_flag_set', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
    })

    it('test_negative_flag_clear', function () {
      cpu.getRegister().flags = 0xFF;
      cpu.getRegister().clearFlag(Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
    })
  })

  describe('overflow flag', function () {

    it('test_overflow_flag_value', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_V);
      assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0x40);
    })

    it('test_overflow_flag_set', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_V);
      assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
    })

    it('test_overflow_flag_clear', function () {
      cpu.getRegister().flags = 0xFF;
      cpu.getRegister().clearFlag(Register.FLAG_V);
      assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
    })
  })

  describe('unused flag', function () {

    it('test_unused_flag_value', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_1);
      assert.equal(cpu.getRegister().flags & Register.FLAG_1, 0x20);
    })

    it('test_unused_flag_set', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_1);
      assert.equal(cpu.getRegister().flags & Register.FLAG_1, Register.FLAG_1);
    })

    it('test_unused_flag_clear', function () {
      cpu.getRegister().flags = 0xFF;
      cpu.getRegister().clearFlag(Register.FLAG_1);
      assert.equal(cpu.getRegister().flags & Register.FLAG_1, 0);
    })
  })

  describe('break flag', function () {

    it('test_break_flag_value', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_B);
      assert.equal(cpu.getRegister().flags & Register.FLAG_B, 0x10);
    })

    it('test_break_flag_set', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_B);
      assert.equal(cpu.getRegister().flags & Register.FLAG_B, Register.FLAG_B);
    })

    it('test_break_flag_clear', function () {
      cpu.getRegister().flags = 0xFF;
      cpu.getRegister().clearFlag(Register.FLAG_B);
      assert.equal(cpu.getRegister().flags & Register.FLAG_B, 0);
    })
  })

  describe('decimal flag', function () {

    it('test_decimal_flag_value', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_D);
      assert.equal(cpu.getRegister().flags & Register.FLAG_D, 0x08);
    })

    it('test_decimal_flag_set', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_D);
      assert.equal(cpu.getRegister().flags & Register.FLAG_D, Register.FLAG_D);
    })

    it('test_decimal_flag_clear', function () {
      cpu.getRegister().flags = 0xFF;
      cpu.getRegister().clearFlag(Register.FLAG_D);
      assert.equal(cpu.getRegister().flags & Register.FLAG_D, 0);
    })
  })

  describe('interrupt disable flag', function () {

    it('test_interrupt disable_flag_value', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_I);
      assert.equal(cpu.getRegister().flags & Register.FLAG_I, 0x04);
    })

    it('test_interrupt disable_flag_set', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_I);
      assert.equal(cpu.getRegister().flags & Register.FLAG_I, Register.FLAG_I);
    })

    it('test_interrupt disable_flag_clear', function () {
      cpu.getRegister().flags = 0xFF;
      cpu.getRegister().clearFlag(Register.FLAG_I);
      assert.equal(cpu.getRegister().flags & Register.FLAG_I, 0);
    })
  })

  describe('zero flag', function () {

    it('test_zero_flag_value', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0x02);
    })

    it('test_zero_flag_set', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    })

    it('test_zero_flag_clear', function () {
      cpu.getRegister().flags = 0xFF;
      cpu.getRegister().clearFlag(Register.FLAG_Z);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    })
  })

  describe('carry flag', function () {

    it('test_carry_flag_value', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_C);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0x01);
    })

    it('test_carry_flag_set', function () {
      cpu.getRegister().flags = 0x00;
      cpu.getRegister().setFlag(Register.FLAG_C);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
    })

    it('test_carry_flag_clear', function () {
      cpu.getRegister().flags = 0xFF;
      cpu.getRegister().clearFlag(Register.FLAG_C);
      assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
    })
  })

  describe('LDA', function () {

    it('LDA #$20', function () {
      mem.writeByte(0x0000, 0xa9);
      mem.writeByte(0x0001, 0x20);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x20);
    });

    it('LDA #$20 - flags', function () {
      mem.writeByte(0x0000, 0xa9);
      mem.writeByte(0x0001, 0x20);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x20);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('LDA #$ff - flags', function () {
      mem.writeByte(0x0000, 0xa9);
      mem.writeByte(0x0001, 0xff);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0xff);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
    });

    it('LDA #$00 - flags', function () {
      mem.writeByte(0x0000, 0xa9);
      mem.writeByte(0x0001, 0x00);
      cpu.step();
      assert.equal(cpu.getRegister().ac, 0x00);
      assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
      assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
    });
  });

  describe('STA', function () {

    it('STA $08', function () {
      cpu.getRegister().ac = 0x55;
      mem.writeByte(0x0000, 0x85);
      mem.writeByte(0x0001, 0x08);
      cpu.step();
      assert.equal(mem.readByte(0x0008), 0x55);
    });

  });
});

