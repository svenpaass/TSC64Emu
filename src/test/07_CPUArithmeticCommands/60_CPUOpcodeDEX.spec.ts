import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('60: CPU - DEX (decrement X) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('DEX', function () {

        it('test_dex_decrements_x', function () {
            cpu.getRegister().x = 0x10;

            // $0000 DEX
            mem.writeByte(0x0000, 0xCA);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().x, 0x0F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_dex_below_00_rolls_over_and_sets_negative_flag', function () {
            cpu.getRegister().x = 0x00;

            // $0000 DEX
            mem.writeByte(0x0000, 0xCA);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().x, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_dex_sets_zero_flag_when_decrementing_to_zero', function () {
            cpu.getRegister().x = 0x01;

            // $0000 DEX
            mem.writeByte(0x0000, 0xCA);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })
    })
})