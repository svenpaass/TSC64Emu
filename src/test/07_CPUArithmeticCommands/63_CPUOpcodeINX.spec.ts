import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('63: CPU - INX (increment X) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('INX', function () {

        it('test_inx_increments_x', function () {
            cpu.getRegister().x = 0x09;

            // $0000 INX
            mem.writeByte(0x0000, 0xE8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().x, 0x0A);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_inx_above_FF_rolls_over_and_sets_zero_flag', function () {
            cpu.getRegister().x = 0xFF;

            // $0000 INX
            mem.writeByte(0x0000, 0xE8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().x, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_inx_sets_negative_flag_when_incrementing_above_7F', function () {
            cpu.getRegister().x = 0x7F;

            // $0000 INX
            mem.writeByte(0x0000, 0xE8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().x, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })
    })
})