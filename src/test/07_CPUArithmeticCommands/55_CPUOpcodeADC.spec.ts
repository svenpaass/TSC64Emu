import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('55: CPU - ADC (add with carry) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('ADC Absolute', function () {

        it('test_adc_bcd_off_absolute_carry_clear_in_accumulator_zeroes', function () {
            cpu.getRegister().ac = 0x00;

            // $0000 ADC $C000
            mem.writeByte(0x0000, 0x6D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);

            mem.writeByte(0xC000, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_adc_bcd_off_absolute_carry_set_in_accumulator_zero', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ADC $C000
            mem.writeByte(0x0000, 0x6D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            // functional test => carry has to be clear
            //assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_adc_bcd_off_absolute_carry_clear_in_no_carry_clear_out', function () {
            cpu.getRegister().ac = 0x01;

            // $0000 ADC $C000
            mem.writeByte(0x0000, 0x6D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000, 0xFE);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_absolute_carry_clear_in_carry_set_out', function () {
            cpu.getRegister().ac = 0x02;

            // $0000 ADC $C000
            mem.writeByte(0x0000, 0x6D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_absolute_overflow_clr_no_carry_01_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;

            // $0000 ADC $C000
            mem.writeByte(0x0000, 0x6D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_absolute_overflow_clr_no_carry_01_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;

            // $0000 ADC $C000
            mem.writeByte(0x0000, 0x6D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_absolute_overflow_set_no_carry_7f_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x7f;

            // $0000 ADC $C000
            mem.writeByte(0x0000, 0x6D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_absolute_overflow_set_no_carry_80_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x80;

            // $0000 ADC $C000
            mem.writeByte(0x0000, 0x6D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x7F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_absolute_overflow_set_on_40_plus_40', function () {
            cpu.getRegister().flags &= ~Register.FLAG_V;
            cpu.getRegister().ac = 0x40;

            // $0000 ADC $C000
            mem.writeByte(0x0000, 0x6D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000, 0x40);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ADC Zero Page', function () {

        it('test_adc_bcd_off_zp_carry_clear_in_accumulator_zeroes', function () {
            cpu.getRegister().ac = 0x00;

            // $0000 ADC $00B0
            mem.writeByte(0x0000, 0x65);
            mem.writeByte(0x0001, 0xB0);
            mem.writeByte(0x00B0, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_adc_bcd_off_zp_carry_set_in_accumulator_zero', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ADC $00B0
            mem.writeByte(0x0000, 0x65);
            mem.writeByte(0x0001, 0xB0);
            mem.writeByte(0x00B0, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            // functional test => carry has to be clear
            //assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_adc_bcd_off_zp_carry_clear_in_no_carry_clear_out', function () {
            cpu.getRegister().ac = 0x01;

            // $0000 ADC $00B0
            mem.writeByte(0x0000, 0x65);
            mem.writeByte(0x0001, 0xB0);
            mem.writeByte(0x00B0, 0xFE);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_zp_carry_clear_in_carry_set_out', function () {
            cpu.getRegister().ac = 0x02;

            // $0000 ADC $00B0
            mem.writeByte(0x0000, 0x65);
            mem.writeByte(0x0001, 0xB0);
            mem.writeByte(0x00B0, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_zp_overflow_clr_no_carry_01_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;

            // $0000 ADC $00B0
            mem.writeByte(0x0000, 0x65);
            mem.writeByte(0x0001, 0xB0);
            mem.writeByte(0x00B0, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_zp_overflow_clr_no_carry_01_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;

            // $0000 ADC $00B0
            mem.writeByte(0x0000, 0x65);
            mem.writeByte(0x0001, 0xB0);
            mem.writeByte(0x00B0, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_zp_overflow_set_no_carry_7f_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x7F;

            // $0000 ADC $00B0
            mem.writeByte(0x0000, 0x65);
            mem.writeByte(0x0001, 0xB0);
            mem.writeByte(0x00B0, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_zp_overflow_set_no_carry_80_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x80;

            // $0000 ADC $00B0
            mem.writeByte(0x0000, 0x65);
            mem.writeByte(0x0001, 0xB0);
            mem.writeByte(0x00B0, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x7F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_zp_overflow_set_on_40_plus_40', function () {
            cpu.getRegister().flags &= ~Register.FLAG_V;
            cpu.getRegister().ac = 0x40;

            // $0000 ADC $00B0
            mem.writeByte(0x0000, 0x65);
            mem.writeByte(0x0001, 0xB0);
            mem.writeByte(0x00B0, 0x40);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ADC Immediate', function () {

        it('test_adc_bcd_off_immediate_carry_clear_in_accumulator_zeroes', function () {
            cpu.getRegister().ac = 0x00;

            // $0000 ADC #$00
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_adc_bcd_off_immediate_carry_set_in_accumulator_zero', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ADC #$00
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            // functional test => carry has to be clear
            //assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_adc_bcd_off_immediate_carry_clear_in_no_carry_clear_out', function () {
            cpu.getRegister().ac = 0x01;

            // $0000 ADC #$FE
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0xFE);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_immediate_carry_clear_in_carry_set_out', function () {
            cpu.getRegister().ac = 0x02;

            // $0000 ADC #$FF
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_immediate_overflow_clr_no_carry_01_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;

            // $0000 ADC #$01
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_immediate_overflow_clr_no_carry_01_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;

            // $0000 ADC #$FF
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_immediate_overflow_set_no_carry_7f_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x7f;

            // $0000 ADC #$01
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_immediate_overflow_set_no_carry_80_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x80;

            // $0000 ADC #$FF
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x7F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_immediate_overflow_set_on_40_plus_40', function () {
            cpu.getRegister().flags &= ~Register.FLAG_V;
            cpu.getRegister().ac = 0x40;

            // $0000 ADC #$40
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0x40);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_on_immediate_79_plus_00_carry_set', function () {
            cpu.getRegister().flags |= Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C;
            cpu.getRegister().ac = 0x79;

            // $0000 ADC #$00
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_adc_bcd_on_immediate_6f_plus_00_carry_set', function () {
            cpu.getRegister().flags |= Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C;
            cpu.getRegister().ac = 0x6F;

            // $0000 ADC #$00
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x76);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_adc_bcd_on_immediate_9c_plus_9d', function () {
            cpu.getRegister().flags |= Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x9C;

            // $0000 ADC #$9D
            // $0002 ADC #$9D
            mem.writeByte(0x0000, 0x69);
            mem.writeByte(0x0001, 0x9D);
            mem.writeByte(0x0002, 0x69);
            mem.writeByte(0x0003, 0x9D);
            cpu.step();
            assert.equal(cpu.getRegister().ac, 0x9F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0004);
            assert.equal(cpu.getRegister().ac, 0x93);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('ADC Absolute, X-Indexed', function () {

        it('test_adc_bcd_off_abs_x_carry_clear_in_accumulator_zeroes', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $C000,X
            mem.writeByte(0x0000, 0x7D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);

            mem.writeByte(0xC000 + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_adc_bcd_off_abs_x_carry_set_in_accumulator_zero', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ADC $C000,X
            mem.writeByte(0x0000, 0x7D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            // functional test => carry has to be clear
            //assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_adc_bcd_off_abs_x_carry_clear_in_no_carry_clear_out', function () {
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $C000,X
            mem.writeByte(0x0000, 0x7D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().x, 0xFE);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_abs_x_carry_clear_in_carry_set_out', function () {
            cpu.getRegister().ac = 0x02;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $C000,X
            mem.writeByte(0x0000, 0x7D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_abs_x_overflow_clr_no_carry_01_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $C000,X
            mem.writeByte(0x0000, 0x7D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().x, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_abs_x_overflow_clr_no_carry_01_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $C000,X
            mem.writeByte(0x0000, 0x7D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_abs_x_overflow_set_no_carry_7f_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x7F;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $C000,X
            mem.writeByte(0x0000, 0x7D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().x, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_abs_x_overflow_set_no_carry_80_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x80;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $C000,X
            mem.writeByte(0x0000, 0x7D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x7F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_abs_x_overflow_set_on_40_plus_40', function () {
            cpu.getRegister().flags &= ~Register.FLAG_V;
            cpu.getRegister().ac = 0x40;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $C000,X
            mem.writeByte(0x0000, 0x7D);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().x, 0x40);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ADC Absolute, Y-Indexed', function () {

        it('test_adc_bcd_off_abs_y_carry_clear_in_accumulator_zeroes', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;

            // $0000 ADC $C000,Y
            mem.writeByte(0x0000, 0x79);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);

            mem.writeByte(0xC000 + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_adc_bcd_off_abs_y_carry_set_in_accumulator_zero', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ADC $C000,Y
            mem.writeByte(0x0000, 0x79);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            // functional test => carry has to be clear
            //assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_adc_bcd_off_abs_y_carry_clear_in_no_carry_clear_out', function () {
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().y = 0x03;

            // $0000 ADC $C000,Y
            mem.writeByte(0x0000, 0x79);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().y, 0xFE);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_abs_y_carry_clear_in_carry_set_out', function () {
            cpu.getRegister().ac = 0x02;
            cpu.getRegister().y = 0x03;

            // $0000 ADC $C000,Y
            mem.writeByte(0x0000, 0x79);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().y, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_abs_y_overflow_clr_no_carry_01_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().y = 0x03;

            // $0000 ADC $C000,Y
            mem.writeByte(0x0000, 0x79);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().y, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_abs_y_overflow_clr_no_carry_01_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().y = 0x03;

            // $0000 ADC $C000,Y
            mem.writeByte(0x0000, 0x79);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().y, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_abs_y_overflow_set_no_carry_7f_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x7F;
            cpu.getRegister().y = 0x03;

            // $0000 ADC $C000,Y
            mem.writeByte(0x0000, 0x79);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().y, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_abs_y_overflow_set_no_carry_80_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x80;
            cpu.getRegister().y = 0x03;

            // $0000 ADC $C000,Y
            mem.writeByte(0x0000, 0x79);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().y, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x7F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_abs_y_overflow_set_on_40_plus_40', function () {
            cpu.getRegister().flags &= ~Register.FLAG_V;
            cpu.getRegister().ac = 0x40;
            cpu.getRegister().y = 0x03;

            // $0000 ADC $C000,Y
            mem.writeByte(0x0000, 0x79);
            mem.writeByte(0x0001, 0x00);
            mem.writeByte(0x0002, 0xC0);
            mem.writeByte(0xC000 + cpu.getRegister().y, 0x40);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ADC Zero Page, X-Indexed', function () {

        it('test_adc_bcd_off_zp_x_carry_clear_in_accumulator_zeroes', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $0010,X
            mem.writeByte(0x0000, 0x75);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_adc_bcd_off_zp_x_carry_set_in_accumulator_zero', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ADC $0010,X
            mem.writeByte(0x0000, 0x75);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            // functional test => carry has to be clear
            //assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_adc_bcd_off_zp_x_carry_clear_in_no_carry_clear_out', function () {
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $0010,X
            mem.writeByte(0x0000, 0x75);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFE);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_zp_x_carry_clear_in_carry_set_out', function () {
            cpu.getRegister().ac = 0x02;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $0010,X
            mem.writeByte(0x0000, 0x75);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_zp_x_overflow_clr_no_carry_01_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $0010,X
            mem.writeByte(0x0000, 0x75);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_zp_x_overflow_clr_no_carry_01_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $0010,X
            mem.writeByte(0x0000, 0x75);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_zp_x_overflow_set_no_carry_7f_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x7F;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $0010,X
            mem.writeByte(0x0000, 0x75);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_zp_x_overflow_set_no_carry_80_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x80;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $0010,X
            mem.writeByte(0x0000, 0x75);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x7F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_zp_x_overflow_set_on_40_plus_40', function () {
            cpu.getRegister().flags &= ~Register.FLAG_V;
            cpu.getRegister().ac = 0x40;
            cpu.getRegister().x = 0x03;

            // $0000 ADC $0010,X
            mem.writeByte(0x0000, 0x75);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x40);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ADC Indirect, Indexed (X)', function () {

        it('test_adc_bcd_off_ind_indexed_carry_clear_in_accumulator_zeroes', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 ADC ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);
            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_adc_bcd_off_ind_indexed_carry_set_in_accumulator_zero', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ADC ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);
            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            // functional test => carry has to be clear
            //assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_adc_bcd_off_ind_indexed_carry_clear_in_no_carry_clear_out', function () {
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 ADC ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);
            mem.writeByte(0xABCD, 0xFE);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_ind_indexed_carry_clear_in_carry_set_out', function () {
            cpu.getRegister().ac = 0x02;
            cpu.getRegister().x = 0x03;

            // $0000 ADC ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);
            mem.writeByte(0xABCD, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_ind_indexed_overflow_clr_no_carry_01_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 ADC ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);
            mem.writeByte(0xABCD, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_ind_indexed_overflow_clr_no_carry_01_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 ADC ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);
            mem.writeByte(0xABCD, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_ind_indexed_overflow_set_no_carry_7f_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x7F;
            cpu.getRegister().x = 0x03;

            // $0000 ADC ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);
            mem.writeByte(0xABCD, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_ind_indexed_overflow_set_no_carry_80_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x80;
            cpu.getRegister().x = 0x03;

            // $0000 ADC ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);
            mem.writeByte(0xABCD, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x7F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_ind_indexed_overflow_set_on_40_plus_40', function () {
            cpu.getRegister().flags &= ~Register.FLAG_V;
            cpu.getRegister().ac = 0x40;
            cpu.getRegister().x = 0x03;

            // $0000 ADC ($0010,X)
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xCD);
            mem.writeByte(0x0014, 0xAB);
            mem.writeByte(0xABCD, 0x40);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })

    describe('ADC Indexed, Indirect (Y)', function () {

        it('test_adc_bcd_off_indexed_ind_carry_clear_in_accumulator_zeroes', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;

            // $0000 ADC ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x71);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_adc_bcd_off_indexed_ind_carry_set_in_accumulator_zero', function () {
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;
            cpu.getRegister().flags |= Register.FLAG_C;

            // $0000 ADC ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x61);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            // functional test => carry has to be clear
            //assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_adc_bcd_off_indexed_ind_carry_clear_in_no_carry_clear_out', function () {
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().y = 0x03;

            // $0000 ADC ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x71);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().y, 0xFE);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_indexed_ind_carry_clear_in_carry_set_out', function () {
            cpu.getRegister().ac = 0x02;
            cpu.getRegister().y = 0x03;

            // $0000 ADC ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x71);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().y, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_adc_bcd_off_indexed_ind_overflow_clr_no_carry_01_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().y = 0x03;

            // $0000 ADC ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x71);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().y, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x02);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_indexed_ind_overflow_clr_no_carry_01_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().y = 0x03;

            // $0000 ADC ($0010),Y
            // $0013 Vector to $ABCD
            mem.writeByte(0x0000, 0x71);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().y, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })

        it('test_adc_bcd_off_indexed_ind_overflow_set_no_carry_7f_plus_01', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x7F;
            cpu.getRegister().y = 0x03;

            // $0000 ADC ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x71);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().y, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_indexed_ind_overflow_set_no_carry_80_plus_ff', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().ac = 0x80;
            cpu.getRegister().y = 0x03;

            // $0000 ADC ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x71);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().y, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x7F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        })

        it('test_adc_bcd_off_indexed_ind_overflow_set_on_40_plus_40', function () {
            cpu.getRegister().flags &= ~Register.FLAG_V;
            cpu.getRegister().ac = 0x40;
            cpu.getRegister().x = 0x03;

            // $0000 ADC ($0010),Y
            // $0010 Vector to $ABCD
            mem.writeByte(0x0000, 0x71);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xCD);
            mem.writeByte(0x0011, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().y, 0x40);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })
    })
})