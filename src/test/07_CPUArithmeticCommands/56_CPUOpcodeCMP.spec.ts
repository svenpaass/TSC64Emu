import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('56: CPU - CMP (compare (with accumulator)) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('CMP Indirect, Indexed (X)', function () {

        it('test_cmp_ind_x_has_page_wrap_bug', function () {
            cpu.getRegister().flags = 0x00;
            cpu.getRegister().ac = 0x42;
            cpu.getRegister().x = 0xFF;

            // $0000 CMP ($80,X)
            mem.writeByte(0x0000, 0xC1);
            mem.writeByte(0x0001, 0x80);

            // $007f Vector to $BBBB (read if page wrapped)
            mem.writeByte(0x007F, 0xBB);
            mem.writeByte(0x0080, 0xBB);

            // $017f Vector to $ABCD (read if no page wrap)
            mem.writeByte(0x017F, 0xCD);
            mem.writeByte(0x0180, 0xAB);

            // Data
            mem.writeByte(0xABCD, 0x00);
            mem.writeByte(0xBBBB, 0x42);

            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })
    })

    describe('CMP Indexed, Indirect (Y)', function () {

        it('test_cmp_indexed_ind_y_has_page_wrap_bug', function () {
            cpu.getRegister().pc = 0x1000;
            cpu.getRegister().flags = 0x00;
            cpu.getRegister().ac = 0x42;
            cpu.getRegister().y = 0x02;

            // $1000 CMP ($FF,Y)
            mem.writeByte(0x1000, 0xD1);
            mem.writeByte(0x1001, 0xFF);
            // $00FF Vector
            mem.writeByte(0x00FF, 0x10); // low byte
            mem.writeByte(0x0100, 0x20); // high byte if no page wrap
            mem.writeByte(0x0000, 0x00); // high byte if page wrapped

            // Data
            mem.writeByte(0x2012, 0x14); // read if no page wrap
            mem.writeByte(0x0012, 0x42); // read if page wrapped

            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })
    })
})