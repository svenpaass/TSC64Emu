import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('61: CPU - DEY (decrement Y) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('DEY', function () {

        it('test_dey_decrements_y', function () {
            cpu.getRegister().y = 0x10;

            // $0000 DEY
            mem.writeByte(0x0000, 0x88);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().y, 0x0F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_dey_below_00_rolls_over_and_sets_negative_flag', function () {
            cpu.getRegister().y = 0x00;

            // $0000 DEY
            mem.writeByte(0x0000, 0x88);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().y, 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })

        it('test_dey_sets_zero_flag_when_decrementing_to_zero', function () {
            cpu.getRegister().y = 0x01;

            // $0000 DEY
            mem.writeByte(0x0000, 0x88);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })
    })
})