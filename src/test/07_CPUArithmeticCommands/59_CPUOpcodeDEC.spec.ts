import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('59: CPU - DEC (decrement) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('DEC Absolute', function () {

        it('test_dec_abs_decrements_memory', function () {
            // $0000 DEC $ABCD
            mem.writeByte(0x0000, 0xCE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x10);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x0F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_dec_abs_below_00_rolls_over_and_sets_negative_flag', function () {
            // $0000 DEC $ABCD
            mem.writeByte(0x0000, 0xCE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })

        it('test_dec_abs_sets_zero_flag_when_decrementing_to_zero', function () {
            // $0000 DEC $ABCD
            mem.writeByte(0x0000, 0xCE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })
    })

    describe('DEC Zero Page', function () {

        it('test_dec_zp_decrements_memory', function () {
            // $0000 DEC $0010
            mem.writeByte(0x0000, 0xC6);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x10);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x0F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_dec_zp_below_00_rolls_over_and_sets_negative_flag', function () {
            // $0000 DEC $0010
            mem.writeByte(0x0000, 0xC6);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })

        it('test_dec_zp_sets_zero_flag_when_decrementing_to_zero', function () {
            // $0000 DEC $0010
            mem.writeByte(0x0000, 0xC6);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })
    })

    describe('DEC Absolute, X-Indexed', function () {

        it('test_dec_abs_x_decrements_memory', function () {
            cpu.getRegister().x = 0x03;

            // $0000 DEC $ABCD,X
            mem.writeByte(0x0000, 0xDE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x10);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x0F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_dec_abs_x_below_00_rolls_over_and_sets_negative_flag', function () {
            cpu.getRegister().x = 0x03;

            // $0000 DEC $ABCD,X
            mem.writeByte(0x0000, 0xDE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })

        it('test_dec_abs_x_sets_zero_flag_when_decrementing_to_zero', function () {
            cpu.getRegister().x = 0x03;

            // $0000 DEC $ABCD,X
            mem.writeByte(0x0000, 0xDE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);

            mem.writeByte(0xABCD + cpu.getRegister().x, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })
    })

    describe('DEC Zero Page, X-Indexed', function () {

        it('test_dec_zp_x_decrements_memory', function () {
            cpu.getRegister().x = 0x03;

            // $0000 DEC $0010,X
            mem.writeByte(0x0000, 0xD6);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x10);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x0F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
        })

        it('test_dec_zp_x_below_00_rolls_over_and_sets_negative_flag', function () {
            cpu.getRegister().x = 0x03;

            // $0000 DEC $0010,X
            mem.writeByte(0x0000, 0xD6);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0xFF);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })

        it('test_dec_zp_x_sets_zero_flag_when_decrementing_to_zero', function () {
            cpu.getRegister().x = 0x03;

            // $0000 DEC $0010,X
            mem.writeByte(0x0000, 0xD6);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010 + cpu.getRegister().x, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })
    })
})