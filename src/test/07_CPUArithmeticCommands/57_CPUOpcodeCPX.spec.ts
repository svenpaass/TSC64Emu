import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('57: CPU - CPX (compare with X) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('CPX Immediate', function () {
        it('test_cpx_immediate_zero_and_carry_set_negativ_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;

            // $0000 CPX #$42
            cpu.getRegister().x = 0x42;
            mem.writeByte(0x0000, 0xE0);
            mem.writeByte(0x0001, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().x, 0x42);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C); // 0x42 >= 0x42
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpx_immediate_carry_set_zero_and_negative_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().x = 0x43;

            // $0000 CPX #$42
            mem.writeByte(0x0000, 0xE0);
            mem.writeByte(0x0001, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().x, 0x43);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpx_immediate_negative_set_carry_and_zero_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().x = 0x41;

            // $0000 CPX #$42
            mem.writeByte(0x0000, 0xE0);
            mem.writeByte(0x0001, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().x, 0x41);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });
    });

    describe('CPX Absolute', function () {
        it('test_cpx_absolute_zero_and_carry_set_negativ_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 CPX $ABCD
            cpu.getRegister().x = 0x42;
            mem.writeByte(0x0000, 0xEC);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x42);

            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x003);
            assert.equal(cpu.getRegister().x, 0x42);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C); // 0x42 >= 0x42
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpx_absolute_carry_set_zero_and_negative_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().x = 0x43;

            // $0000 CPX $ABCD
            mem.writeByte(0x0000, 0xEC);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x003);
            assert.equal(cpu.getRegister().x, 0x43);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpx_absolute_negative_set_carry_and_zero_clear', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().x = 0x41;

            // $0000 CPX $ABCD
            mem.writeByte(0x0000, 0xEC);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x003);
            assert.equal(cpu.getRegister().x, 0x41);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });
    });

    describe('CPX Zero Page', function () {
        it('test_cpx_zp_zero_and_carry_set_negativ_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 CPX $0010
            cpu.getRegister().x = 0x42;
            mem.writeByte(0x0000, 0xE4);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x42);

            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().x, 0x42);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C); // 0x42 >= 0x42
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpx_zp_carry_set_zero_and_negative_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().x = 0x43;

            // $0000 CPX $0010
            mem.writeByte(0x0000, 0xE4);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().x, 0x43);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpx_zp_negative_set_carry_and_zero_clear', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().x = 0x41;

            // $0000 CPX $0010
            mem.writeByte(0x0000, 0xE4);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().x, 0x41);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });
    });
})