import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('64: CPU - INX (increment Y) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('INY', function () {

        it('test_iny_increments_y', function () {
            cpu.getRegister().y = 0x09;

            // $0000 INY
            mem.writeByte(0x0000, 0xC8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().y, 0x0A);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_iny_above_FF_rolls_over_and_sets_zero_flag', function () {
            cpu.getRegister().y = 0xFF;

            // $0000 INY
            mem.writeByte(0x0000, 0xC8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().y, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_iny_sets_negative_flag_when_incrementing_above_7F', function () {
            cpu.getRegister().y = 0x7F;

            // $0000 INY
            mem.writeByte(0x0000, 0xC8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().y, 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })
    })
})