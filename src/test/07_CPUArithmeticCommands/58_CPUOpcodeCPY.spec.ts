import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('58: CPU - CPY (compare with Y) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('CPY Immediate', function () {
        it('test_cpy_immediate_zero_and_carry_set_negativ_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;

            // $0000 CPY #$42
            cpu.getRegister().y = 0x42;
            mem.writeByte(0x0000, 0xC0);
            mem.writeByte(0x0001, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().y, 0x42);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C); // 0x42 >= 0x42
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpy_immediate_carry_set_zero_and_negative_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().y = 0x43;

            // $0000 CPY #$42
            mem.writeByte(0x0000, 0xC0);
            mem.writeByte(0x0001, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().y, 0x43);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpy_immediate_negative_set_carry_and_zero_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().y = 0x41;

            // $0000 CPY #$42
            mem.writeByte(0x0000, 0xC0);
            mem.writeByte(0x0001, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().y, 0x41);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });
    });

    describe('CPY Absolute', function () {
        it('test_cpy_absolute_zero_and_carry_set_negativ_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 CPY $ABCD
            cpu.getRegister().y = 0x42;
            mem.writeByte(0x0000, 0xCC);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x42);

            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x003);
            assert.equal(cpu.getRegister().y, 0x42);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C); // 0x42 >= 0x42
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpy_absolute_carry_set_zero_and_negative_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().y = 0x43;

            // $0000 CPY $ABCD
            mem.writeByte(0x0000, 0xCC);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x003);
            assert.equal(cpu.getRegister().y, 0x43);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpy_absolute_negative_set_carry_and_zero_clear', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().y = 0x41;

            // $0000 CPY $ABCD
            mem.writeByte(0x0000, 0xCC);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x003);
            assert.equal(cpu.getRegister().y, 0x41);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });
    });

    describe('CPY Zero Page', function () {
        it('test_cpy_zp_zero_and_carry_set_negativ_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_Z;
            cpu.getRegister().flags &= ~Register.FLAG_C;

            // $0000 CPY $0010
            cpu.getRegister().y = 0x42;
            mem.writeByte(0x0000, 0xC4);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x42);

            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().y, 0x42);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C); // 0x42 >= 0x42
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpy_zp_carry_set_zero_and_negative_clear', function () {
            cpu.getRegister().flags &= ~Register.FLAG_C;
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().y = 0x43;

            // $0000 CPY $0010
            mem.writeByte(0x0000, 0xC4);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().y, 0x43);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_cpy_zp_negative_set_carry_and_zero_clear', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().y = 0x41;

            // $0000 CPY $0010
            mem.writeByte(0x0000, 0xC4);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x42);
            cpu.step();

            assert.equal(cpu.getRegister().pc, 0x002);
            assert.equal(cpu.getRegister().y, 0x41);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });
    });
})