import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('62: CPU - INC (increment) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('INC Absolute', function () {

        it('test_inc_abs_increments_memory', function () {
            // $0000 INC $ABCD
            mem.writeByte(0x0000, 0xEE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x09);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x0A);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_inc_abs_increments_memory_rolls_over_and_sets_zero_flag', function () {
            // $0000 INC $ABCD
            mem.writeByte(0x0000, 0xEE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_inc_abs_sets_negative_flag_when_incrementing_above_7F', function () {
            // $0000 INC $ABCD
            mem.writeByte(0x0000, 0xEE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x7F);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })
    })

    describe('INC Zero Page', function () {

        it('test_inc_zp_increments_memory', function () {
            // $0000 INC $0010
            mem.writeByte(0x0000, 0xE6);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x09);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x0A);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_inc_zp_increments_memory_rolls_over_and_sets_zero_flag', function () {
            // $0000 INC $0010
            mem.writeByte(0x0000, 0xE6);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_inc_zp_sets_negative_flag_when_incrementing_above_7F', function () {
            // $0000 INC $0010
            mem.writeByte(0x0000, 0xE6);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x7F);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })
    })

    describe('INC Absolute, X-Indexed', function () {

        it('test_inc_abs_x_increments_memory', function () {
            cpu.getRegister().x = 0x03;

            // $0000 INC $ABCD,X
            mem.writeByte(0x0000, 0xFE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x09);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x0A);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_inc_abs_x_increments_memory_rolls_over_and_sets_zero_flag', function () {
            cpu.getRegister().x = 0x03;

            // $0000 INC $ABCD,X
            mem.writeByte(0x0000, 0xFE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_inc_abs_x_sets_negative_flag_when_incrementing_above_7F', function () {
            cpu.getRegister().x = 0x03;

            // $0000 INC $ABCD,X
            mem.writeByte(0x0000, 0xFE);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD + cpu.getRegister().x, 0x7F);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(mem.readByte(0xABCD + cpu.getRegister().x), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })
    })

    describe('INC Zero Page, X-Indexed', function () {

        it('test_inc_zp_x_increments_memory', function () {
            cpu.getRegister().x = 0x03;

            // $0000 INC $0010,X
            mem.writeByte(0x0000, 0xF6);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x09);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x0A);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_inc_zp_x_increments_memory_rolls_over_and_sets_zero_flag', function () {
            cpu.getRegister().x = 0x03;

            // $0000 INC $0010,X
            mem.writeByte(0x0000, 0xF6);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0xFF);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        })

        it('test_inc_zp_x_sets_negative_flag_when_incrementing_above_7F', function () {
            cpu.getRegister().x = 0x03;

            // $0000 INC $0010,X
            mem.writeByte(0x0000, 0xF6);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010 + cpu.getRegister().x, 0x7F);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(mem.readByte(0x0010 + cpu.getRegister().x), 0x80);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        })
    })
})