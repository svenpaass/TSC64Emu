import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('65: CPU - SBC (subtract with carry) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('SBC Absolute', function () {

        it('test_sbc_abs_all_zeros_and_no_borrow_is_zero', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x00;

            // $0000 SBC $ABCD
            mem.writeByte(0x0000, 0xED);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_abs_downto_zero_no_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x01;

            // $0000 SBC $ABCD
            mem.writeByte(0x0000, 0xED);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_abs_downto_zero_with_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x01;

            // $0000 SBC $ABCD
            mem.writeByte(0x0000, 0xED);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_abs_downto_four_with_borrow_clears_z_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x07;

            // $0000 SBC $ABCD
            mem.writeByte(0x0000, 0xED);
            mem.writeByte(0x0001, 0xCD);
            mem.writeByte(0x0002, 0xAB);
            mem.writeByte(0xABCD, 0x02);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x04);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('SBC Zero Page', function () {

        it('test_sbc_zp_all_zeros_and_no_borrow_is_zero', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x00;

            // $0000 SBC $10
            mem.writeByte(0x0000, 0xE5);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_zp_downto_zero_no_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x01;

            // $0000 SBC $10
            mem.writeByte(0x0000, 0xE5);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_zp_downto_zero_with_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x01;

            // $0000 SBC $10
            mem.writeByte(0x0000, 0xE5);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_zp_downto_four_with_borrow_clears_z_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x07;

            // $0000 SBC $10
            mem.writeByte(0x0000, 0xE5);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0x02);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x04);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('SBC Immediate', function () {

        it('test_sbc_imm_all_zeros_and_no_borrow_is_zero', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x00;

            // $0000 SBC #$00
            mem.writeByte(0x0000, 0xE9);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_imm_downto_zero_no_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x01;

            // $0000 SBC #$01
            mem.writeByte(0x0000, 0xE9);
            mem.writeByte(0x0001, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_imm_downto_zero_with_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x01;

            // $0000 SBC #$00
            mem.writeByte(0x0000, 0xE9);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_imm_downto_four_with_borrow_clears_z_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x07;

            // $0000 SBC #$02
            mem.writeByte(0x0000, 0xE9);
            mem.writeByte(0x0001, 0x02);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x04);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })

        it('test_sbc_bcd_on_immediate_0a_minus_00_carry_set', function () {
            cpu.getRegister().flags |= Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x0a;

            // $0000 SBC #$00
            mem.writeByte(0x0000, 0xE9);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x0a);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })

        it('test_sbc_bcd_on_immediate_9a_minus_00_carry_set', function () {
            cpu.getRegister().flags |= Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x9a;

            // $0000 SBC #$00
            mem.writeByte(0x0000, 0xE9);
            mem.writeByte(0x0001, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x9a);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })

        it('test_sbc_bcd_on_immediate_00_minus_01_carry_set', function () {
            cpu.getRegister().flags |= Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_V;
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x00;

            // $0000 SBC #$00
            mem.writeByte(0x0000, 0xE9);
            mem.writeByte(0x0001, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x99);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })

        it('test_sbc_bcd_on_immediate_20_minus_0a_carry_unset', function () {
            cpu.getRegister().flags |= Register.FLAG_D;
            cpu.getRegister().ac = 0x20;

            // $0000 SBC #$00
            mem.writeByte(0x0000, 0xE9);
            mem.writeByte(0x0001, 0x0a);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x1F);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('SBC Absolute, X-Indexed', function () {

        it('test_sbc_abs_x_all_zeros_and_no_borrow_is_zero', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x0D;

            // $0000 SBC $FEE0,X
            mem.writeByte(0x0000, 0xFD);
            mem.writeByte(0x0001, 0xE0);
            mem.writeByte(0x0002, 0xFE);
            mem.writeByte(0xFEED, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_abs_x_downto_zero_no_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x0D;

            // $0000 SBC $FEE0,X
            mem.writeByte(0x0000, 0xFD);
            mem.writeByte(0x0001, 0xE0);
            mem.writeByte(0x0002, 0xFE);
            mem.writeByte(0xFEED, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_abs_x_downto_zero_with_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x0D;

            // $0000 SBC $FEE0,X
            mem.writeByte(0x0000, 0xFD);
            mem.writeByte(0x0001, 0xE0);
            mem.writeByte(0x0002, 0xFE);
            mem.writeByte(0xFEED, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_abs_x_downto_four_with_borrow_clears_z_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x07;
            cpu.getRegister().x = 0x0D;

            // $0000 SBC $FEE0,X
            mem.writeByte(0x0000, 0xFD);
            mem.writeByte(0x0001, 0xE0);
            mem.writeByte(0x0002, 0xFE);
            mem.writeByte(0xFEED, 0x02);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x04);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('SBC Absolute, Y-Indexed', function () {

        it('test_sbc_abs_y_all_zeros_and_no_borrow_is_zero', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x0D;

            // $0000 SBC $FEE0,Y
            mem.writeByte(0x0000, 0xF9);
            mem.writeByte(0x0001, 0xE0);
            mem.writeByte(0x0002, 0xFE);
            mem.writeByte(0xFEED, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_abs_y_downto_zero_no_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().y = 0x0D;

            // $0000 SBC $FEE0,Y
            mem.writeByte(0x0000, 0xF9);
            mem.writeByte(0x0001, 0xE0);
            mem.writeByte(0x0002, 0xFE);
            mem.writeByte(0xFEED, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_abs_y_downto_zero_with_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().y = 0x0D;

            // $0000 SBC $FEE0,Y
            mem.writeByte(0x0000, 0xF9);
            mem.writeByte(0x0001, 0xE0);
            mem.writeByte(0x0002, 0xFE);
            mem.writeByte(0xFEED, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_abs_y_downto_four_with_borrow_clears_z_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x07;
            cpu.getRegister().y = 0x0D;

            // $0000 SBC $FEE0,Y
            mem.writeByte(0x0000, 0xF9);
            mem.writeByte(0x0001, 0xE0);
            mem.writeByte(0x0002, 0xFE);
            mem.writeByte(0xFEED, 0x02);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0003);
            assert.equal(cpu.getRegister().ac, 0x04);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('SBC Indirect, Indexed (X)', function () {

        it('test_sbc_ind_x_all_zeros_and_no_borrow_is_zero', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x03;

            // $0000 SBC ($10,X)
            // $0013 Vector to $FEED
            mem.writeByte(0x0000, 0xE1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xED);
            mem.writeByte(0x0014, 0xFE);
            mem.writeByte(0xFEED, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_ind_x_downto_zero_no_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 SBC ($10,X)
            // $0013 Vector to $FEED
            mem.writeByte(0x0000, 0xE1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xED);
            mem.writeByte(0x0014, 0xFE);
            mem.writeByte(0xFEED, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_ind_x_downto_zero_with_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x03;

            // $0000 SBC ($10,X)
            // $0013 Vector to $FEED
            mem.writeByte(0x0000, 0xE1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xED);
            mem.writeByte(0x0014, 0xFE);
            mem.writeByte(0xFEED, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_ind_x_downto_four_with_borrow_clears_z_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x07;
            cpu.getRegister().x = 0x03;

            // $0000 SBC ($10,X)
            // $0013 Vector to $FEED
            mem.writeByte(0x0000, 0xE1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xED);
            mem.writeByte(0x0014, 0xFE);
            mem.writeByte(0xFEED, 0x02);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x04);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('SBC Indexed, Indirect (Y)', function () {

        it('test_sbc_ind_y_all_zeros_and_no_borrow_is_zero', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().y = 0x03;

            // $0000 SBC ($10),Y
            // $0010 Vector to $FEED
            mem.writeByte(0x0000, 0xF1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xED);
            mem.writeByte(0x0014, 0xFE);
            mem.writeByte(0xFEED + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_ind_y_downto_zero_no_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().y = 0x03;

            // $0000 SBC ($10),Y
            // $0010 Vector to $FEED
            mem.writeByte(0x0000, 0xF1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xED);
            mem.writeByte(0x0011, 0xFE);
            mem.writeByte(0xFEED + cpu.getRegister().y, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_ind_y_downto_zero_with_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().y = 0x03;

            // $0000 SBC ($10),Y
            // $0010 Vector to $FEED
            mem.writeByte(0x0000, 0xF1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0013, 0xED);
            mem.writeByte(0x0014, 0xFE);
            mem.writeByte(0xFEED + cpu.getRegister().y, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_ind_y_downto_four_with_borrow_clears_z_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x07;
            cpu.getRegister().y = 0x03;

            // $0000 SBC ($10),Y
            // $0010 Vector to $FEED
            mem.writeByte(0x0000, 0xF1);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x0010, 0xED);
            mem.writeByte(0x0011, 0xFE);
            mem.writeByte(0xFEED + cpu.getRegister().y, 0x02);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x04);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })

    describe('SBC Zero Page, X-Indexed', function () {

        it('test_sbc_zp_x_all_zeros_and_no_borrow_is_zero', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x00;
            cpu.getRegister().x = 0x0D;

            // $0000 SBC $10,X
            mem.writeByte(0x0000, 0xF5);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x001D, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_zp_x_downto_zero_no_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags |= Register.FLAG_C; // borrow = 0
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x0D;

            // $0000 SBC $10,X
            mem.writeByte(0x0000, 0xF5);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x001D, 0x01);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_zp_x_downto_zero_with_borrow_sets_z_clears_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x01;
            cpu.getRegister().x = 0x0D;

            // $0000 SBC $10,X
            mem.writeByte(0x0000, 0xF5);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x001D, 0x00);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x00);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
        })

        it('test_sbc_zp_x_downto_four_with_borrow_clears_z_n', function () {
            cpu.getRegister().flags &= ~Register.FLAG_D;
            cpu.getRegister().flags &= ~Register.FLAG_C; // borrow = 1
            cpu.getRegister().ac = 0x07;
            cpu.getRegister().x = 0x0D;

            // $0000 SBC $10,X
            mem.writeByte(0x0000, 0xF5);
            mem.writeByte(0x0001, 0x10);
            mem.writeByte(0x001D, 0x02);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            assert.equal(cpu.getRegister().ac, 0x04);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })
})