import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('22: CPU - PHA (push accumulator) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('PHA', function () {

        it('test_pha_pushes_a_and_updates_sp', function () {
            cpu.getRegister().ac = 0xAB;
            // $0000 PHA
            mem.writeByte(0x0000, 0x48);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0xAB);
            assert.equal(mem.readByte(0x01FF), 0xAB);
            assert.equal(cpu.getRegister().sp, 0xFE);
        });
    });
});
