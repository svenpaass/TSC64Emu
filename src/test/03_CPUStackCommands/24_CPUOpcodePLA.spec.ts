import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('24: CPU - PLA (pull accumulator) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('PLA', function () {

        it('test_pla_pulls_top_byte_from_stack_into_a_and_updates_sp', function () {
            // $0000 PLA
            mem.writeByte(0x0000, 0x68);
            mem.writeByte(0x01FF, 0xAB);
            cpu.getRegister().sp = 0xFE;

            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().ac, 0xAB);
            assert.equal(cpu.getRegister().sp, 0xFF);
        });
    });
});
