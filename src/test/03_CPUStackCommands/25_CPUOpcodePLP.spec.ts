import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('25: CPU - PLP (pull processor status (SR)) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('PLP', function () {

        it('test_plp_pulls_top_byte_from_stack_into_flags_and_updates_sp', function () {
// TODO: Test prüfen!
            cpu.getRegister().sp = 0xFE;

            // $0000 PLP
            mem.writeByte(0x0000, 0x28);
            mem.writeByte(0x01FF, 0xBA); // must have FLAG_B and FLAG_1 set
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().flags, 0xBA);
            assert.equal(cpu.getRegister().sp, 0xFF);
        });
    });
});
