import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('23: CPU - PHP (push processor status (SR)) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('PHP', function () {

        it('test_php_pushes_processor_status_and_updates_sp', function () {
            let flags = cpu.getRegister().flags | Register.FLAG_B | Register.FLAG_1;
            cpu.getRegister().flags = flags;
            // $0000 PHP
            mem.writeByte(0x0000, 0x08);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(mem.readByte(0x01FF), flags | Register.FLAG_B | Register.FLAG_1);
            assert.equal(cpu.getRegister().sp, 0xFE);
        });
    });
});
