import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('26: CPU - BCC (branch on carry clear) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('BCC', function () {

        it('test_bcc_carry_clear_branches_relative_forward', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_C);
            // $0000 BCC +6
            mem.writeByte(0x0000, 0x90);
            mem.writeByte(0x0001, 0x06);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002 + 0x06);
        });

        it('test_bcc_carry_clear_branches_relative_backward', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_C);
            cpu.getRegister().pc = 0x0050;
            // $0050 BCC -6
            const rel = (0x06 ^ 0xFF) + 1;  // two's complement of 6
            mem.writeByte(0x0050, 0x90);
            mem.writeByte(0x0051, rel);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0052 - 0x06);
        });

        it('test_bcc_carry_clear_branches_7F_relative_backward', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_C);
            cpu.getRegister().pc = 0x0100;
            // $0100 BCC 0x81 (-7F)
            const rel = (0x7F ^ 0xFF) + 1;  // two's complement of 6
            mem.writeByte(0x0100, 0x90);
            mem.writeByte(0x0101, rel);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0102 - 0x7F);
        });

        it('test_bcc_carry_clear_branches_7F_relative_forward', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_C);
            cpu.getRegister().pc = 0x0100;
            // $0100 BCC 0x7F
            const rel = 0x7F;
            mem.writeByte(0x0100, 0x90);
            mem.writeByte(0x0101, rel);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0102 + 0x7F);
        });

        it('test_bcc_carry_set_does_not_branch', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            // $0000 BCC +6
            mem.writeByte(0x0000, 0x90);
            mem.writeByte(0x0001, 0x06);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
        });
    });
});
