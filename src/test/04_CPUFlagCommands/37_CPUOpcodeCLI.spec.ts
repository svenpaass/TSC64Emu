import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('37: CPU - CLI (clear interrupt disable) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('CLI', function () {

        it('test_cli_clears_interrupt_mask_flag', function () {
            cpu.getRegister().flags |= Register.FLAG_I;
            // $0000 CLI
            mem.writeByte(0x0000, 0x58);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().flags & Register.FLAG_I, 0);
        })
    })
})