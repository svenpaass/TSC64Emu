import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('41: CPU - SED (set decimal) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('SED', function () {

        it('test_sed_sets_decimal_mode_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_D);
            // $0000 SED
            mem.writeByte(0x0000, 0xF8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().flags & Register.FLAG_D, Register.FLAG_D);
        })
    })
})