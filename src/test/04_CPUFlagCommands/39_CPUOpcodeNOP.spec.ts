import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
let mem: Memory;
let cpu: CPU;

describe('39: CPU - NOP (no operation) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('NOP', function () {

        it('test_nop_does_nothing', function () {
            // $0000 NOP
            mem.writeByte(0x0000, 0xEA);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
        })
    })
})