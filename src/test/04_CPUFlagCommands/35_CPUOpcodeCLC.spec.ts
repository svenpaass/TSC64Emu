import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('35: CPU - CLC (clear carry) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('CLC', function () {

        it('test_clc_clears_carry_flag', function () {
            cpu.getRegister().flags |= Register.FLAG_C;
            // $0000 CLC
            mem.writeByte(0x0000, 0x18);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, 0);
        })
    })
})