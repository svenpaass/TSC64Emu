import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('40: CPU - SEC (set carry) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('SEC', function () {

        it('test_sec_sets_carry_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_C);
            // $0000 SEC
            mem.writeByte(0x0000, 0x38);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().flags & Register.FLAG_C, Register.FLAG_C);
        })
    })
})