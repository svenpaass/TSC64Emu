import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('36: CPU - CLD (clear decimal) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('CLD', function () {

        it('test_cld_clears_decimal_flag', function () {
            cpu.getRegister().flags |= Register.FLAG_D;
            // $0000 CLD
            mem.writeByte(0x0000, 0xD8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().flags & Register.FLAG_D, 0);
        })
    })
})