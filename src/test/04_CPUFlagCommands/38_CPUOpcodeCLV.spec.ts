import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('38: CPU - CLV (clear overflow) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('CLV', function () {

        it('test_clv_clears_overflow_flag', function () {
            cpu.getRegister().flags |= Register.FLAG_V;
            // $0000 CLV
            mem.writeByte(0x0000, 0xB8);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        })
    })
})