import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('28: CPU - BEQ (branch on equal (zero set)) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('BEQ', function () {

        it('test_beq_zero_set_branches_relative_forward', function () {
            cpu.getRegister().flags |= Register.FLAG_Z;
            // $0000 BEQ +6
            mem.writeByte(0x0000, 0xF0);
            mem.writeByte(0x0001, 0x06);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002 + 0x06);
        });

        it('test_beq_zero_set_branches_relative_backward', function () {
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().pc = 0x0050;
            // $0050 BEQ -6
            const rel = (0x06 ^ 0xFF) + 1;  // two's complement of 6
            mem.writeByte(0x0050, 0xF0);
            mem.writeByte(0x0051, rel);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0052 - 0x06);
        });

        it('test_beq_carry_clear_branches_7F_relative_backward', function () {
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().pc = 0x0100;
            // $0100 BCS 0x81 (-7F)
            const rel = (0x7F ^ 0xFF) + 1;  // two's complement of 6
            mem.writeByte(0x0100, 0xF0);
            mem.writeByte(0x0101, rel);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0102 - 0x7F);
        });

        it('test_beq_carry_clear_branches_7F_relative_forward', function () {
            cpu.getRegister().flags |= Register.FLAG_Z;
            cpu.getRegister().pc = 0x0100;
            // $0100 BCS 0x7F
            const rel = 0x7F;
            mem.writeByte(0x0100, 0xF0);
            mem.writeByte(0x0101, rel);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0102 + 0x7F);
        });

        it('test_beq_zero_clear_does_not_branch', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_C);
            // $0000 BEQ +6
            mem.writeByte(0x0000, 0xF0);
            mem.writeByte(0x0001, 0x06);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
        });
    });
});

