import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('42: CPU - SEI (set interrupt disable) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('SEI', function () {

        it('test_sei_sets_interrupt_disable_flag', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_I);
            // $0000 SEI
            mem.writeByte(0x0000, 0x78);
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0001);
            assert.equal(cpu.getRegister().flags & Register.FLAG_I, Register.FLAG_I);
        })
    })
})