import Memory from '../../memory/Memory';
import CPU from '../../cpu/CPU';

import { assert } from 'chai';
import Register from '../../cpu/Register';
let mem: Memory;
let cpu: CPU;

describe('29: CPU - BIT (bit test) Instruction tests', function () {

    beforeEach(() => {
        mem = new Memory(64 * 1024);
        cpu = new CPU(mem);
    });

    describe('BIT (Absolute)', function () {

        it('test_bit_abs_copies_bit_7_of_memory_to_n_flag_when_0', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_N);

            // $0000 BIT $FEED
            mem.writeByte(0x0000, 0x2C);
            mem.writeByte(0x0001, 0xED);
            mem.writeByte(0x0002, 0xFE);

            mem.writeByte(0xFEED, 0xFF);
            cpu.getRegister().ac = 0xFF;
            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });

        it('test_bit_abs_copies_bit_7_of_memory_to_n_flag_when_1', function () {
            cpu.getRegister().flags |= Register.FLAG_N;

            // $0000 BIT $FEED
            mem.writeByte(0x0000, 0x2C);
            mem.writeByte(0x0001, 0xED);
            mem.writeByte(0x0002, 0xFE);

            mem.writeByte(0xFEED, 0x00);
            cpu.getRegister().ac = 0xFF;
            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_bit_abs_copies_bit_6_of_memory_to_v_flag_when_0', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_V);

            // $0000 BIT $FEED
            mem.writeByte(0x0000, 0x2C);
            mem.writeByte(0x0001, 0xED);
            mem.writeByte(0x0002, 0xFE);

            mem.writeByte(0xFEED, 0xFF);
            cpu.getRegister().ac = 0xFF;
            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        });

        it('test_bit_abs_copies_bit_6_of_memory_to_v_flag_when_1', function () {
            cpu.getRegister().flags |= Register.FLAG_V;

            // $0000 BIT $FEED
            mem.writeByte(0x0000, 0x2C);
            mem.writeByte(0x0001, 0xED);
            mem.writeByte(0x0002, 0xFE);

            mem.writeByte(0xFEED, 0x00);
            cpu.getRegister().ac = 0xFF;
            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        });

        it('test_bit_abs_stores_result_of_and_in_z_preserves_a_when_1', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_Z);

            // $0000 BIT $FEED
            mem.writeByte(0x0000, 0x2C);
            mem.writeByte(0x0001, 0xED);
            mem.writeByte(0x0002, 0xFE);

            mem.writeByte(0xFEED, 0x00);
            cpu.getRegister().ac = 0x01;
            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(mem.readByte(0xFEED), 0x00);
        });

        it('test_bit_abs_stores_result_of_and_when_nonzero_in_z_preserves_a', function () {
            cpu.getRegister().flags |= Register.FLAG_Z;

            // $0000 BIT $FEED
            mem.writeByte(0x0000, 0x2C);
            mem.writeByte(0x0001, 0xED);
            mem.writeByte(0x0002, 0xFE);

            mem.writeByte(0xFEED, 0x01);
            cpu.getRegister().ac = 0x01;
            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0); // result of AND is non-zero
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(mem.readByte(0xFEED), 0x01);
        });

        it('test_bit_abs_stores_result_of_and_when_zero_in_z_preserves_a', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_Z);

            // $0000 BIT $FEED
            mem.writeByte(0x0000, 0x2C);
            mem.writeByte(0x0001, 0xED);
            mem.writeByte(0x0002, 0xFE);

            mem.writeByte(0xFEED, 0x00);
            cpu.getRegister().ac = 0x01;
            cpu.step();
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z); // result of AND is zero
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(mem.readByte(0xFEED), 0x00);
        });
    });

    describe('BIT (Zero Page)', function () {

        it('test_bit_zp_copies_bit_7_of_memory_to_n_flag_when_0', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_N);

            // $0000 BIT $0010
            mem.writeByte(0x0000, 0x24);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0xFF);
            cpu.getRegister().ac = 0xFF;
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            // TODO: assert.equal(cpu.getCycles(), 3);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, Register.FLAG_N);
        });

        it('test_bit_zp_copies_bit_7_of_memory_to_n_flag_when_1', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_N);

            // $0000 BIT $0010
            mem.writeByte(0x0000, 0x24);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.getRegister().ac = 0xFF;
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            // TODO: assert.equal(cpu.getCycles(), 3);
            assert.equal(cpu.getRegister().flags & Register.FLAG_N, 0);
        });

        it('test_bit_zp_copies_bit_6_of_memory_to_v_flag_when_0', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_V);

            // $0000 BIT $0010
            mem.writeByte(0x0000, 0x24);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0xFF);
            cpu.getRegister().ac = 0xFF;
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            // TODO: assert.equal(cpu.getCycles(), 3);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, Register.FLAG_V);
        });

        it('test_bit_zp_copies_bit_6_of_memory_to_v_flag_when_1', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_V);

            // $0000 BIT $0010
            mem.writeByte(0x0000, 0x24);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.getRegister().ac = 0xFF;
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            // TODO: assert.equal(cpu.getCycles(), 3);
            assert.equal(cpu.getRegister().flags & Register.FLAG_V, 0);
        });

        it('test_bit_zp_stores_result_of_and_in_z_preserves_a_when_1', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_Z);

            // $0000 BIT $0010
            mem.writeByte(0x0000, 0x24);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.getRegister().ac = 0x01;
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            // TODO: assert.equal(cpu.getCycles(), 3);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(mem.readByte(0x0010), 0x00);
        });

        it('test_bit_zp_stores_result_of_and_when_nonzero_in_z_preserves_a', function () {
            cpu.getRegister().flags |= Register.FLAG_Z;

            // $0000 BIT $0010
            mem.writeByte(0x0000, 0x24);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x01);
            cpu.getRegister().ac = 0x01;
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            // TODO: assert.equal(cpu.getCycles(), 3);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, 0);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(mem.readByte(0x0010), 0x01);
        });

        it('test_bit_zp_stores_result_of_and_when_zero_in_z_preserves_a', function () {
            cpu.getRegister().flags &= ~(Register.FLAG_Z);

            // $0000 BIT $0010
            mem.writeByte(0x0000, 0x24);
            mem.writeByte(0x0001, 0x10);

            mem.writeByte(0x0010, 0x00);
            cpu.getRegister().ac = 0x01;
            cpu.step();
            assert.equal(cpu.getRegister().pc, 0x0002);
            // TODO: assert.equal(cpu.getCycles(), 3);
            assert.equal(cpu.getRegister().flags & Register.FLAG_Z, Register.FLAG_Z);
            assert.equal(cpu.getRegister().ac, 0x01);
            assert.equal(mem.readByte(0x0010), 0x00);
        });
    });
});

