import Utils from '../utils/Utils';
import Register from './Register';
import Memory from '../memory/Memory';
import AddressingMode from './AddressingMode';
import OpcodeTable from './OpcodeTable';

import { Opcode } from './Interfaces';

class CPU {

  // array with all opcodes, memory and registers
  private opcodes: Opcode[];
  private mem: Memory;
  private reg: Register;
  private cycleCount: number;
  private interruptOccured: boolean;

  constructor(mem: Memory) {
    this.reg = new Register();
    this.reg.sp = 0xff;
    this.reg.setFlag(Register.FLAG_1 | Register.FLAG_B | Register.FLAG_I);

    this.mem = mem;
    this.opcodes = OpcodeTable.getInstance().opcodes;
    this.interruptOccured = false;
    this.cycleCount = 0;
  }

  /**
   * returns the cpu registers
   */
  public getRegister(): Register {
    return this.reg;
  }

  /**
   * returns the number of executed cycles
   */
  public getCycleCount(): number {
    return this.cycleCount;
  }

  public setInterruptOccured(): void {
    this.interruptOccured = true;
  }

  /**
   * handles cpu reset signal
   *
   * pc = ($fffc/$fffd)
   */
  public reset(): void {
    this.reg.pc = this.mem.readByte(0xfffd) * 256 + this.mem.readByte(0xfffc);
    this.cycleCount = 0;
  }

  /**
   * executes one cpu cycle (fetch - decode - execute)
   */
  public step(): void {
    if (this.interruptOccured && !(this.reg.flags & Register.FLAG_I)) {
      this.interruptOccured = false;

      this.mem.writeByte(0x0100 | this.reg.sp, (this.reg.pc & 0xff00) >> 8);
      this.reg.sp--;
      this.reg.sp &= 0xff;
      this.mem.writeByte(0x0100 | this.reg.sp, this.reg.pc & 0xff);
      this.reg.sp--;
      this.reg.sp &= 0xff;
      this.reg.clearFlag(Register.FLAG_B);  // don't push break on stack
      this.mem.writeByte(0x0100 | this.reg.sp, this.reg.flags);
      this.reg.setFlag(Register.FLAG_B);

      this.reg.sp--;
      this.reg.sp &= 0xff;
      this.reg.pc = this.mem.readByte(0xfffe) | (this.mem.readByte(0xffff) * 256);
      this.reg.setFlag(Register.FLAG_I);
    }
    const opcode = this.fetch();
    this.execute(opcode);
  }

  /**
   * fetches the next opcode to be executed
   *
   * @return opcode to be executed
   */
  private fetch(): number {
    return this.mem.readByte(this.reg.pc++);
  }

  // elegant adc/sbc implementation
  // https://stackoverflow.com/a/29224684
  private adc(arg1: number): number {
    let sum = this.reg.ac + arg1 + ((this.reg.flags & Register.FLAG_C) ? 1 : 0);
    (sum > 0xFF) ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);
    // The overflow flag is set when the sign of the addends is the same and
    // differs from the sign of the sum

    (~(this.reg.ac ^ arg1) & (this.reg.ac ^ sum) & 0x80) ? this.reg.setFlag(Register.FLAG_V) : this.reg.clearFlag(Register.FLAG_V);
    (sum & 0xff) == 0x00 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
    sum & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
    return sum & 0xff;
  }

  // source: https://github.com/hoglet67/Optima/blob/master/src/6502.c
  private adc_bcd(arg1: number): number {
    let ah = 0;
    this.reg.flags &= ~(Register.FLAG_Z | Register.FLAG_N);
    let tempb = this.reg.ac + arg1 + ((this.reg.flags & Register.FLAG_C) ? 1 : 0);
    if (!tempb) {
      this.reg.flags |= Register.FLAG_Z;
    }
    let al = (this.reg.ac & 0xF) + (arg1 & 0xF) + ((this.reg.flags & Register.FLAG_C) ? 1 : 0);
    if (al > 9) {
      al -= 10;
      al &= 0xF;
      ah = 1;
    }
    ah += ((this.reg.ac >> 4) + (arg1 >> 4));
    if (ah & 8) {
      this.reg.flags |= Register.FLAG_N;
    }
    (((ah << 4) ^ this.reg.ac) & 128) && (!((this.reg.ac ^ arg1) & 128)) ? this.reg.flags |= Register.FLAG_V : this.reg.flags &= ~Register.FLAG_V;
    this.reg.flags &= ~Register.FLAG_C;
    if (ah > 9) {
      this.reg.flags |= Register.FLAG_C;
      ah -= 10;
      ah &= 0xF;
    }
    return (al & 0xF) | (ah << 4);
  }

  private sbc(arg1: number): number {
    return this.adc(~arg1 & 0xff);
  }

  // source: https://github.com/hoglet67/Optima/blob/master/src/6502.c
  private sbc_bcd(arg1: number): number {
    let hc6 = 0;
    this.reg.flags &= ~(Register.FLAG_Z | Register.FLAG_N);
    let tempb = this.reg.ac - arg1 - ((this.reg.flags & Register.FLAG_C) ? 0 : 1);
    if (!tempb) {
      this.reg.flags |= Register.FLAG_Z;
    }
    let al = (this.reg.ac & 15) - (arg1 & 15) - ((this.reg.flags & Register.FLAG_C) ? 0 : 1);
    if (al & 16) {
      al -= 6;
      al &= 0xF;
      hc6 = 1;
    }
    let ah = (this.reg.ac >> 4) - (arg1 >> 4);
    if (hc6) ah--;
    if ((this.reg.ac - (arg1 + ((this.reg.flags & Register.FLAG_C) ? 0 : 1))) & 0x80) {
      this.reg.flags |= Register.FLAG_N;
    }
    (((this.reg.ac ^ arg1) & 0x80) && ((this.reg.ac ^ tempb) & 0x808)) ? this.reg.flags |= Register.FLAG_V : this.reg.flags &= ~Register.FLAG_V;
    this.reg.flags |= Register.FLAG_C;
    if (ah & 16) {
      this.reg.flags &= ~Register.FLAG_C;
      ah -= 6;
      ah &= 0xF;
    }
    return (al & 0xF) | ((ah & 0xF) << 4);
  }

  /**
   * executes the given opcode
   *
   * @param opcode
   */
  private execute(opcode: number): void {
    let arg1: number;
    let arg2: number;
    let value: number;
    let carry: boolean;

    // read parameters and calculate addressing mode for the given opcode
    const instructionLength = this.opcodes[opcode].length;
    if (instructionLength > 1) {
      arg1 = this.mem.readByte(this.reg.pc++);
    }
    if (instructionLength > 2) {
      arg2 = this.mem.readByte(this.reg.pc++);
    }
    let effectiveAddress = AddressingMode.calculateEffectiveAdd(opcode, this.opcodes[opcode].mode, this.mem, this.reg, arg1, arg2);
    this.cycleCount += this.opcodes[opcode].cycles;

    // execute the opcode
    switch (opcode) {
      /* ADC */
      case 0x69:
        if (!(this.reg.flags & Register.FLAG_D)) {
          this.reg.ac = this.adc(arg1);
        } else {
          this.reg.ac = this.adc_bcd(arg1);
        }
        break;
      case 0x65:
      case 0x75:
      case 0x6d:
      case 0x7d:
      case 0x79:
      case 0x61:
      case 0x71:
        if (!(this.reg.flags & Register.FLAG_D)) {
          this.reg.ac = this.adc(this.mem.readByte(effectiveAddress));
        } else {
          this.reg.ac = this.adc_bcd(this.mem.readByte(effectiveAddress));
        }
        break;

      /* AND */
      case 0x29:
        this.reg.ac &= arg1;
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;
      case 0x25:
      case 0x35:
      case 0x2d:
      case 0x3d:
      case 0x39:
      case 0x21:
      case 0x31:
        this.reg.ac &= this.mem.readByte(effectiveAddress);
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* ASL */
      case 0x0a:
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);

        this.reg.ac <<= 1;
        this.reg.ac &= 0xff;

        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      case 0x06:
      case 0x16:
      case 0x0e:
      case 0x1e:
        value = this.mem.readByte(effectiveAddress);
        value & 0x80 ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);

        value <<= 1;
        value &= 0xff;

        value == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        this.mem.writeByte(effectiveAddress, value);
        break;

      /* BIT */
      case 0x24:
      case 0x2c:
        value = this.mem.readByte(effectiveAddress);
        (value & this.reg.ac) == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        value & 0x40 ? this.reg.setFlag(Register.FLAG_V) : this.reg.clearFlag(Register.FLAG_V);
        break;

      /* BPL */
      case 0x10:
        if ((this.reg.flags & Register.FLAG_N) == 0) {
          this.reg.pc = effectiveAddress;
        }
        break;

      /* BMI */
      case 0x30:
        if ((this.reg.flags & Register.FLAG_N) != 0) {
          this.reg.pc = effectiveAddress;
        }
        break;

      /* BRK */
      case 0x00:
        this.reg.pc++;
        this.reg.setFlag(Register.FLAG_1 | Register.FLAG_B);
        this.mem.writeByte(0x0100 | this.reg.sp, (this.reg.pc & 0xff00) >> 8);
        this.reg.sp--;
        this.reg.sp &= 0xff;
        this.mem.writeByte(0x0100 | this.reg.sp, this.reg.pc & 0xff);
        this.reg.sp--;
        this.reg.sp &= 0xff;
        this.mem.writeByte(0x0100 | this.reg.sp, this.reg.flags);
        this.reg.sp--;
        this.reg.sp &= 0xff;
        this.reg.pc = this.mem.readByte(0xfffe) | (this.mem.readByte(0xffff) * 256);
        this.reg.setFlag(Register.FLAG_I);
        break;

      /* BVC */
      case 0x50:
        if ((this.reg.flags & Register.FLAG_V) == 0) {
          this.reg.pc = effectiveAddress;
        }
        break;

      /* BVS */
      case 0x70:
        if ((this.reg.flags & Register.FLAG_V) != 0) {
          this.reg.pc = effectiveAddress;
        }
        break;

      /* BCC */
      case 0x90:
        if ((this.reg.flags & Register.FLAG_C) == 0) {
          this.reg.pc = effectiveAddress;
        }
        break;

      /* BCS */
      case 0xb0:
        if ((this.reg.flags & Register.FLAG_C) != 0) {
          this.reg.pc = effectiveAddress;
        }
        break;

      /* BNE */
      case 0xd0:
        if ((this.reg.flags & Register.FLAG_Z) == 0) {
          this.reg.pc = effectiveAddress;
        }
        break;

      /* BEQ */
      case 0xf0:
        if ((this.reg.flags & Register.FLAG_Z) != 0) {
          this.reg.pc = effectiveAddress;
        }
        break;

      /* CLC */
      case 0x18:
        this.reg.clearFlag(Register.FLAG_C);
        break;

      /* CLD */
      case 0xd8:
        this.reg.clearFlag(Register.FLAG_D);
        break;

      /* CLI */
      case 0x58:
        this.reg.clearFlag(Register.FLAG_I);
        break;

      /* CLV */
      case 0xb8:
        this.reg.clearFlag(Register.FLAG_V);
        break;

      /* CMP */
      case 0xc9:
        (this.reg.ac >= arg1) ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);
        (this.reg.ac == arg1) ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value = this.reg.ac - arg1;
        value &= 0xff;
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;
      case 0xc5:
      case 0xd5:
      case 0xcd:
      case 0xdd:
      case 0xd9:
      case 0xc1:
      case 0xd1:
        (this.reg.ac >= this.mem.readByte(effectiveAddress)) ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);
        (this.reg.ac == this.mem.readByte(effectiveAddress)) ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value = this.reg.ac - this.mem.readByte(effectiveAddress);
        value &= 0xff;
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* CPX */
      case 0xe0:
        (this.reg.x >= arg1) ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);
        (this.reg.x == arg1) ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value = this.reg.x - arg1;
        value &= 0xff;
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;
      case 0xe4:
      case 0xec:
        (this.reg.x >= this.mem.readByte(effectiveAddress)) ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);
        (this.reg.x == this.mem.readByte(effectiveAddress)) ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value = this.reg.x - this.mem.readByte(effectiveAddress);
        value &= 0xff;
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* CPY */
      case 0xc0:
        (this.reg.y >= arg1) ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);
        (this.reg.y == arg1) ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value = this.reg.y - arg1;
        value &= 0xff;
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;
      case 0xc4:
      case 0xcc:
        (this.reg.y >= this.mem.readByte(effectiveAddress)) ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);
        (this.reg.y == this.mem.readByte(effectiveAddress)) ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value = this.reg.y - this.mem.readByte(effectiveAddress);
        value &= 0xff;
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* DEC */
      case 0xc6:
      case 0xd6:
      case 0xce:
      case 0xde:
        value = (this.mem.readByte(effectiveAddress) - 1) & 0xff;
        value == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        this.mem.writeByte(effectiveAddress, value);
        break;

      /* DEX */
      case 0xca:
        this.reg.x--;
        this.reg.x &= 0xff;
        this.reg.x == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.x & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* DEY */
      case 0x88:
        this.reg.y--;
        this.reg.y &= 0xff;
        this.reg.y == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.y & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* EOR */
      case 0x49:
        this.reg.ac ^= arg1;
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;
      case 0x45:
      case 0x55:
      case 0x4d:
      case 0x5d:
      case 0x59:
      case 0x41:
      case 0x51:
        this.reg.ac ^= this.mem.readByte(effectiveAddress);
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* INC */
      case 0xe6:
      case 0xf6:
      case 0xee:
      case 0xfe:
        value = (this.mem.readByte(effectiveAddress) + 1) & 0xff;
        value == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        this.mem.writeByte(effectiveAddress, value);
        break;

      /* INX */
      case 0xe8:
        this.reg.x++;
        this.reg.x &= 0xff;
        this.reg.x == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.x & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* INY */
      case 0xc8:
        this.reg.y++;
        this.reg.y &= 0xff;
        this.reg.y == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.y & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* JMP */
      case 0x4c:
      case 0x6c:
        this.reg.pc = effectiveAddress;
        break;

      /* JSR */
      case 0x20:
        let tempValue = this.reg.pc - 1; // Return Point -1
        this.mem.writeByte(0x0100 | (this.reg.sp & 0xff), tempValue >> 8);
        this.reg.sp--;
        this.mem.writeByte(0x0100 | (this.reg.sp & 0xff), tempValue & 0xff);
        this.reg.sp--;
        this.reg.sp &= 0xff;
        this.reg.pc = effectiveAddress;
        break;

      /* LDA */
      case 0xa9:
        this.reg.ac = arg1;
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;
      case 0xa5:
      case 0xb5:
      case 0xad:
      case 0xbd:
      case 0xb9:
      case 0xa1:
      case 0xb1:
        this.reg.ac = this.mem.readByte(effectiveAddress);
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* LDX */
      case 0xa2:
        this.reg.x = arg1;
        this.reg.x == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.x & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;
      case 0xa6:
      case 0xb6:
      case 0xae:
      case 0xbe:
        this.reg.x = this.mem.readByte(effectiveAddress);
        this.reg.x == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.x & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* LDY */
      case 0xa0:
        this.reg.y = arg1;
        this.reg.y == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.y & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;
      case 0xa4:
      case 0xb4:
      case 0xac:
      case 0xbc:
        this.reg.y = this.mem.readByte(effectiveAddress);
        this.reg.y == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.y & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* LSR */
      case 0x4a:
        (this.reg.ac & 0x1) != 0 ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);

        this.reg.ac >>= 1;
        this.reg.ac &= 0xff;

        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.clearFlag(Register.FLAG_N);
        break;

      case 0x46:
      case 0x56:
      case 0x4e:
      case 0x5e:
        value = this.mem.readByte(effectiveAddress);
        (value & 0x1) != 0 ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);

        value >>= 1;
        value &= 0xff;

        value == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.clearFlag(Register.FLAG_N);
        this.mem.writeByte(effectiveAddress, value);
        break;

      /* NOP */
      case 0xea:
        break;

      /* ORA */
      case 0x09:
        this.reg.ac |= arg1;
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;
      case 0x05:
      case 0x15:
      case 0x0d:
      case 0x1d:
      case 0x19:
      case 0x01:
      case 0x11:
        this.reg.ac |= this.mem.readByte(effectiveAddress);
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* PLA */
      case 0x68:
        this.reg.sp++;
        this.reg.sp &= 0xff;

        this.reg.ac = this.mem.readByte(0x0100 | this.reg.sp);
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* PHA */
      case 0x48:
        this.mem.writeByte(0x0100 | this.reg.sp & 0xff, this.reg.ac);
        this.reg.sp--;
        this.reg.sp &= 0xff;
        break;

      /* PLP */
      case 0x28:
        this.reg.sp++;
        this.reg.sp &= 0xff;
        this.reg.flags = this.mem.readByte(0x0100 | this.reg.sp);
        break;

      /* PHP */
      case 0x08:
        /// software instructions BRK & PHP will push the B flag as being 1.
        /// http://visual6502.org/wiki/index.php?title=6502_BRK_and_B_bit
        this.mem.writeByte(0x0100 | this.reg.sp & 0xff, this.reg.flags | Register.FLAG_B);
        ///
        this.reg.sp--;
        this.reg.sp &= 0xff;
        break;

      /* ROL */
      case 0x2a:
        carry = (this.reg.flags & Register.FLAG_C) != 0;
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);

        this.reg.ac <<= 1;
        this.reg.ac &= 0xff;

        if (carry) {
          this.reg.ac |= 0x01;
        }

        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      case 0x26:
      case 0x36:
      case 0x2e:
      case 0x3e:
        value = this.mem.readByte(effectiveAddress);
        carry = (this.reg.flags & Register.FLAG_C) != 0;
        value & 0x80 ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);

        value <<= 1;
        value &= 0xff;

        if (carry) {
          value |= 0x01;
        }

        value == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        this.mem.writeByte(effectiveAddress, value);
        break;

      /* ROR */
      case 0x6a:
        carry = (this.reg.flags & Register.FLAG_C) != 0;
        this.reg.ac & 0x01 ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);

        this.reg.ac >>= 1;
        this.reg.ac &= 0xff;

        if (carry) {
          this.reg.ac |= 0x80;
        }

        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      case 0x66:
      case 0x76:
      case 0x6e:
      case 0x7e:
        value = this.mem.readByte(effectiveAddress);
        carry = (this.reg.flags & Register.FLAG_C) != 0;
        value & 0x01 ? this.reg.setFlag(Register.FLAG_C) : this.reg.clearFlag(Register.FLAG_C);

        value >>= 1;
        value &= 0xff;

        if (carry) {
          value |= 0x80;
        }

        value == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        value & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        this.mem.writeByte(effectiveAddress, value);
        break;

      /* RTI */
      case 0x40:
        this.reg.pc++;
        this.reg.sp++;
        this.reg.sp &= 0xff;
        this.reg.flags = this.mem.readByte(0x0100 | this.reg.sp);
        this.reg.flags |= Register.FLAG_1 | Register.FLAG_B;

        this.reg.sp++;
        this.reg.sp &= 0xff;
        this.reg.pc = this.mem.readByte(0x0100 | this.reg.sp) & 0xff;

        this.reg.sp++;
        this.reg.sp &= 0xff;
        this.reg.pc |= (this.mem.readByte(0x0100 | this.reg.sp)) * 256;
        break;

      /* RTS */
      case 0x60:
        this.reg.sp++;
        this.reg.sp &= 0xff;
        this.reg.pc = this.mem.readByte(0x0100 | this.reg.sp);
        this.reg.sp++;
        this.reg.sp &= 0xff;
        this.reg.pc |= (this.mem.readByte(0x0100 | this.reg.sp)) * 256;
        // before returning increment pc and wrap around top of memory
        this.reg.pc++;
        this.reg.pc &= 0xffff;
        break;

      /* SBC */
      case 0xe9:
        if (!(this.reg.flags & Register.FLAG_D)) {
          this.reg.ac = this.sbc(arg1);
        } else {
          this.reg.ac = this.sbc_bcd(arg1);
        }
        break;

      case 0xe5:
      case 0xf5:
      case 0xed:
      case 0xfd:
      case 0xf9:
      case 0xe1:
      case 0xf1:
        if (!(this.reg.flags & Register.FLAG_D)) {
          this.reg.ac = this.sbc(this.mem.readByte(effectiveAddress));
        } else {
          this.reg.ac = this.sbc_bcd(this.mem.readByte(effectiveAddress));
        }
        break;

      /* SEC */
      case 0x38:
        this.reg.setFlag(Register.FLAG_C);
        break;

      /* SED */
      case 0xf8:
        this.reg.setFlag(Register.FLAG_D);
        break;

      /* SEI */
      case 0x78:
        this.reg.setFlag(Register.FLAG_I);
        break;

      /* STA */
      case 0x85:
      case 0x95:
      case 0x8d:
      case 0x9d:
      case 0x99:
      case 0x81:
      case 0x91:
        this.mem.writeByte(effectiveAddress, this.reg.ac);
        break;

      /* STX */
      case 0x86:
      case 0x96:
      case 0x8e:
        this.mem.writeByte(effectiveAddress, this.reg.x);
        break;

      /* STY */
      case 0x84:
      case 0x94:
      case 0x8c:
        this.mem.writeByte(effectiveAddress, this.reg.y);
        break;

      /* TAX */
      case 0xaa:
        this.reg.x = this.reg.ac;
        this.reg.x == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.x & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* TXA */
      case 0x8a:
        this.reg.ac = this.reg.x;
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* TAY */
      case 0xa8:
        this.reg.y = this.reg.ac;
        this.reg.y == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.y & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* TYA */
      case 0x98:
        this.reg.ac = this.reg.y;
        this.reg.ac == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.ac & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* TSX */
      case 0xba:
        this.reg.x = this.reg.sp;
        this.reg.x == 0 ? this.reg.setFlag(Register.FLAG_Z) : this.reg.clearFlag(Register.FLAG_Z);
        this.reg.x & 0x80 ? this.reg.setFlag(Register.FLAG_N) : this.reg.clearFlag(Register.FLAG_N);
        break;

      /* TXS */
      case 0x9a:
        this.reg.sp = this.reg.x;
        break;

      default:
        console.log('UNKNOWN OPCODE: ' + Utils.decimalToHex(opcode, 2) + ' - ' + this.opcodes[opcode].label);
        return;
    }
  }
}

export default CPU;