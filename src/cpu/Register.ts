class Register {
    public static FLAG_N = 0x80;
    public static FLAG_V = 0x40;
    public static FLAG_1 = 0x20;
    public static FLAG_B = 0x10;
    public static FLAG_D = 0x08;
    public static FLAG_I = 0x04;
    public static FLAG_Z = 0x02;
    public static FLAG_C = 0x01;

    public pc : number;
    public sp : number;
    public ac : number;
    public x : number;
    public y : number;
    public flags : number;

    constructor() {
        this.pc = 0x0000;
        this.sp = 0x00;
        this.ac = 0x00;
        this.x = 0x00;
        this.y = 0x00;
        this.flags = 0x00;
    }

    public setFlag(f : number) : void {
      this.flags |= f;
    }

    public clearFlag(f : number) : void {
      this.flags &= ~f;
    }
}

export default Register;