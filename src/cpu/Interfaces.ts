export interface Opcode {
    opcode : number;
    label  : string;
    length : number;
    cycles : number;
    mode   : number;
}
