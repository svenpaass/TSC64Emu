import Memory from '../memory/Memory';
import Register from './Register';

class AddressingMode {
    /**
     * define addressing modes
     */
    public static ACCUMULATOR = 0;
    public static IMPLIED = 1;
    public static IMMEDIATE = 2;
    public static RELATIVE = 3;
    public static ABSOLUTE = 4;
    public static ABSOLUTE_INDEXED_BY_X = 5;
    public static ABSOLUTE_INDEXED_BY_Y = 6;
    public static ABSOLUTE_INDIRECT = 7;
    public static ZERO_PAGE = 8;
    public static ZERO_PAGE_INDEXED_BY_X = 9;
    public static ZERO_PAGE_INDEXED_BY_Y = 10;
    public static ZERO_PAGE_INDIRECT_Y_INDEXED = 11;
    public static ZERO_PAGE_X_INDEXED_INDIRECT = 12;

    /**
     * returns the effective address for a given addressing mode
     *
     * @param mode
     * @param argbyte1
     * @param argbyte2
     */
    public static calculateEffectiveAdd(opcode : number, mode : number, mem : Memory, reg : Register, argbyte1 : number, argbyte2 : number) : number {
        var tempAddress = 0;

        switch ( mode ) {
            case this.ACCUMULATOR:
              return undefined;

            case this.IMPLIED:
              return undefined;

            case this.IMMEDIATE:
              return undefined;

            case this.RELATIVE:
              // if bit7 set, then offset is negative
              let newpc = (argbyte1 > 127) ? (argbyte1 - 256) : argbyte1;
              return newpc + reg.pc;

            case this.ABSOLUTE:
              return ((argbyte2 * 256) + argbyte1) & 0xffff;

            case this.ABSOLUTE_INDEXED_BY_X:
              tempAddress = ((argbyte2 * 256) + argbyte1 + reg.x) & 0xffff;
              return tempAddress;

            case this.ABSOLUTE_INDEXED_BY_Y:
              tempAddress = ((argbyte2 * 256) + argbyte1 + reg.y) & 0xffff;
              return tempAddress;

            case this.ABSOLUTE_INDIRECT:
              tempAddress = (argbyte2 * 256) + argbyte1;

              return argbyte2 == 0x00 ?
                mem.readByte((tempAddress + 1) & 0xff) * 256 + mem.readByte(tempAddress) : // page wrap bug zero page
                mem.readByte(tempAddress + 1) * 256 + mem.readByte(tempAddress);

            case this.ZERO_PAGE:
              return argbyte1 & 0xff;

            case this.ZERO_PAGE_INDEXED_BY_X:
              return (argbyte1 + reg.x) & 0xff;

            case this.ZERO_PAGE_INDEXED_BY_Y:
              return (argbyte1 + reg.y) & 0xff;

            case this.ZERO_PAGE_INDIRECT_Y_INDEXED:
              tempAddress = (mem.readByte((argbyte1 + 1) & 0xff)  * 256 + mem.readByte(argbyte1) + reg.y);
              return tempAddress;

            case this.ZERO_PAGE_X_INDEXED_INDIRECT:
              tempAddress = (argbyte1 + reg.x) & 0xff;
              return mem.readByte(tempAddress+1) * 256 + mem.readByte(tempAddress);
        }

        // return no address for all other address modes
        return undefined;
    }
}

export default AddressingMode;
