import AddressingMode from './AddressingMode';
import { Opcode } from './Interfaces';

class OpcodeTable {

    private static _instance : OpcodeTable = new OpcodeTable();

    constructor() {
        if(OpcodeTable._instance){
            throw new Error("Error: Instantiation failed: Use OpcodeTable.getInstance() instead of new.");
        }
        OpcodeTable._instance = this;
        this.opcodes = new Array(256);

        // build the final opcode array from the OpcodeTable data.
        for (let i = 0; i < this.opcodes_raw.length; i++) {
            this.opcodes[this.opcodes_raw[i].opcode] = this.opcodes_raw[i];
        }

        // fill non existing opcodes
        for (let i = 0; i < this.opcodes.length; i++) {
            if (this.opcodes[i] == undefined) {
                this.opcodes[i] = { opcode: undefined, label: '', length: 1, cycles: 0, mode: AddressingMode.IMPLIED }
            }
        }
    }

    public static getInstance() : OpcodeTable
    {
        return OpcodeTable._instance;
    }

    public opcodes: Opcode[];

    private opcodes_raw: Opcode[] = [
        { opcode: 0x69, label: 'ADC', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0x65, label: 'ADC', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x75, label: 'ADC', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0x6D, label: 'ADC', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },
        { opcode: 0x7D, label: 'ADC', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },
        { opcode: 0x79, label: 'ADC', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_Y },
        { opcode: 0x61, label: 'ADC', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT },
        { opcode: 0x71, label: 'ADC', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED },

        { opcode: 0x29, label: 'AND', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0x25, label: 'AND', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x35, label: 'AND', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0x2D, label: 'AND', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },
        { opcode: 0x3D, label: 'AND', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },
        { opcode: 0x39, label: 'AND', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_Y },
        { opcode: 0x21, label: 'AND', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT },
        { opcode: 0x31, label: 'AND', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED },

        { opcode: 0x0a, label: 'ASL', length: 1, cycles: 2, mode: AddressingMode.ACCUMULATOR },
        { opcode: 0x06, label: 'ASL', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x16, label: 'ASL', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0x0e, label: 'ASL', length: 3, cycles: 6, mode: AddressingMode.ABSOLUTE },
        { opcode: 0x1e, label: 'ASL', length: 3, cycles: 7, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },

        { opcode: 0x90, label: 'BCC', length: 2, cycles: 2, mode: AddressingMode.RELATIVE },

        { opcode: 0xb0, label: 'BCS', length: 2, cycles: 2, mode: AddressingMode.RELATIVE },

        { opcode: 0xf0, label: 'BEQ', length: 2, cycles: 2, mode: AddressingMode.RELATIVE },

        { opcode: 0x24, label: 'BIT', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x2c, label: 'BIT', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },

        { opcode: 0x30, label: 'BMI', length: 2, cycles: 2, mode: AddressingMode.RELATIVE },

        { opcode: 0xd0, label: 'BNE', length: 2, cycles: 2, mode: AddressingMode.RELATIVE },

        { opcode: 0x10, label: 'BPL', length: 2, cycles: 2, mode: AddressingMode.RELATIVE },

        { opcode: 0x00, label: 'BRK', length: 1, cycles: 7, mode: AddressingMode.IMPLIED },

        { opcode: 0x50, label: 'BVC', length: 2, cycles: 2, mode: AddressingMode.RELATIVE },

        { opcode: 0x70, label: 'BVS', length: 2, cycles: 2, mode: AddressingMode.RELATIVE },

        { opcode: 0x18, label: 'CLC', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0xd8, label: 'CLD', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0x58, label: 'CLI', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0xb8, label: 'CLV', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0xc9, label: 'CMP', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0xc5, label: 'CMP', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0xd5, label: 'CMP', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0xcd, label: 'CMP', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },
        { opcode: 0xdd, label: 'CMP', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },
        { opcode: 0xd9, label: 'CMP', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_Y },
        { opcode: 0xc1, label: 'CMP', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT },
        { opcode: 0xd1, label: 'CMP', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED },

        { opcode: 0xe0, label: 'CPX', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0xe4, label: 'CPX', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0xec, label: 'CPX', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },

        { opcode: 0xc0, label: 'CPY', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0xc4, label: 'CPY', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0xcc, label: 'CPY', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },

        { opcode: 0xc6, label: 'DEC', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0xd6, label: 'DEC', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0xce, label: 'DEC', length: 3, cycles: 3, mode: AddressingMode.ABSOLUTE },
        { opcode: 0xde, label: 'DEC', length: 3, cycles: 7, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },

        { opcode: 0xca, label: 'DEX', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0x88, label: 'DEY', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0x49, label: 'EOR', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0x45, label: 'EOR', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x55, label: 'EOR', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0x4d, label: 'EOR', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },
        { opcode: 0x5d, label: 'EOR', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },
        { opcode: 0x59, label: 'EOR', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_Y },
        { opcode: 0x41, label: 'EOR', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT },
        { opcode: 0x51, label: 'EOR', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED },

        { opcode: 0xe6, label: 'INC', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0xf6, label: 'INC', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0xee, label: 'INC', length: 3, cycles: 6, mode: AddressingMode.ABSOLUTE },
        { opcode: 0xfe, label: 'INC', length: 3, cycles: 7, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },

        { opcode: 0xe8, label: 'INX', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0xc8, label: 'INY', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0x4c, label: 'JMP', length: 3, cycles: 3, mode: AddressingMode.ABSOLUTE },
        { opcode: 0x6c, label: 'JMP', length: 3, cycles: 5, mode: AddressingMode.ABSOLUTE_INDIRECT },

        { opcode: 0x20, label: 'JSR', length: 3, cycles: 6, mode: AddressingMode.ABSOLUTE },

        { opcode: 0xa9, label: 'LDA', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0xa5, label: 'LDA', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0xb5, label: 'LDA', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0xad, label: 'LDA', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },
        { opcode: 0xbd, label: 'LDA', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },
        { opcode: 0xb9, label: 'LDA', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_Y },
        { opcode: 0xa1, label: 'LDA', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT },
        { opcode: 0xb1, label: 'LDA', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED },

        { opcode: 0xa2, label: 'LDX', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0xa6, label: 'LDX', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0xb6, label: 'LDX', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_Y },
        { opcode: 0xae, label: 'LDX', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },
        { opcode: 0xbe, label: 'LDX', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_Y },

        { opcode: 0xa0, label: 'LDY', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0xa4, label: 'LDY', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0xb4, label: 'LDY', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0xac, label: 'LDY', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },
        { opcode: 0xbc, label: 'LDY', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },

        { opcode: 0x4a, label: 'LSR', length: 1, cycles: 2, mode: AddressingMode.ACCUMULATOR },
        { opcode: 0x46, label: 'LSR', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x56, label: 'LSR', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0x4e, label: 'LSR', length: 3, cycles: 6, mode: AddressingMode.ABSOLUTE },
        { opcode: 0x5e, label: 'LSR', length: 3, cycles: 7, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },

        { opcode: 0xea, label: 'NOP', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0x09, label: 'ORA', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0x05, label: 'ORA', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x15, label: 'ORA', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0x0d, label: 'ORA', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },
        { opcode: 0x1d, label: 'ORA', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },
        { opcode: 0x19, label: 'ORA', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_Y },
        { opcode: 0x01, label: 'ORA', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT },
        { opcode: 0x11, label: 'ORA', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED },

        { opcode: 0x48, label: 'PHA', length: 1, cycles: 3, mode: AddressingMode.IMPLIED },

        { opcode: 0x08, label: 'PHP', length: 1, cycles: 3, mode: AddressingMode.IMPLIED },

        { opcode: 0x68, label: 'PLA', length: 1, cycles: 4, mode: AddressingMode.IMPLIED },

        { opcode: 0x28, label: 'PLP', length: 1, cycles: 4, mode: AddressingMode.IMPLIED },

        { opcode: 0x2a, label: 'ROL', length: 1, cycles: 2, mode: AddressingMode.ACCUMULATOR },
        { opcode: 0x26, label: 'ROL', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x36, label: 'ROL', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0x2e, label: 'ROL', length: 3, cycles: 6, mode: AddressingMode.ABSOLUTE },
        { opcode: 0x3e, label: 'ROL', length: 3, cycles: 7, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },

        { opcode: 0x6a, label: 'ROR', length: 1, cycles: 2, mode: AddressingMode.ACCUMULATOR },
        { opcode: 0x66, label: 'ROR', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x76, label: 'ROR', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0x6e, label: 'ROR', length: 3, cycles: 6, mode: AddressingMode.ABSOLUTE },
        { opcode: 0x7e, label: 'ROR', length: 3, cycles: 7, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },

        { opcode: 0x40, label: 'RTI', length: 1, cycles: 6, mode: AddressingMode.IMPLIED },

        { opcode: 0x60, label: 'RTS', length: 1, cycles: 6, mode: AddressingMode.IMPLIED },

        { opcode: 0xe9, label: 'SBC', length: 2, cycles: 2, mode: AddressingMode.IMMEDIATE },
        { opcode: 0xe5, label: 'SBC', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0xf5, label: 'SBC', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0xed, label: 'SBC', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },
        { opcode: 0xfd, label: 'SBC', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },
        { opcode: 0xf9, label: 'SBC', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE_INDEXED_BY_Y },
        { opcode: 0xe1, label: 'SBC', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT },
        { opcode: 0xf1, label: 'SBC', length: 2, cycles: 5, mode: AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED },

        { opcode: 0x38, label: 'SEC', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0xf8, label: 'SED', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0x78, label: 'SEI', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0x85, label: 'STA', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x95, label: 'STA', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0x8d, label: 'STA', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },
        { opcode: 0x9d, label: 'STA', length: 3, cycles: 5, mode: AddressingMode.ABSOLUTE_INDEXED_BY_X },
        { opcode: 0x99, label: 'STA', length: 3, cycles: 5, mode: AddressingMode.ABSOLUTE_INDEXED_BY_Y },
        { opcode: 0x81, label: 'STA', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT },
        { opcode: 0x91, label: 'STA', length: 2, cycles: 6, mode: AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED },

        { opcode: 0x86, label: 'STX', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x96, label: 'STX', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_Y },
        { opcode: 0x8e, label: 'STX', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },

        { opcode: 0x84, label: 'STY', length: 2, cycles: 3, mode: AddressingMode.ZERO_PAGE },
        { opcode: 0x94, label: 'STY', length: 2, cycles: 4, mode: AddressingMode.ZERO_PAGE_INDEXED_BY_X },
        { opcode: 0x8c, label: 'STY', length: 3, cycles: 4, mode: AddressingMode.ABSOLUTE },

        { opcode: 0xaa, label: 'TAX', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0xa8, label: 'TAY', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0xba, label: 'TSX', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0x8a, label: 'TXA', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0x9a, label: 'TXS', length: 1, cycles: 2, mode: AddressingMode.IMPLIED },

        { opcode: 0x98, label: 'TYA', length: 1, cycles: 2, mode: AddressingMode.IMPLIED }
    ];
}

export default OpcodeTable;