import Memory from '../memory/Memory';

class Video {

    private mem: Memory;
    private context: CanvasRenderingContext2D;
    private pixelData: ImageData;

    constructor(mem: Memory, canvas: HTMLCanvasElement) {
        this.mem = mem;
        this.context = canvas.getContext('2d') as CanvasRenderingContext2D;
        this.context.imageSmoothingEnabled = false;
        this.context.fillStyle = '#0088ff';
        this.context.fillRect(0, 0, canvas.width, canvas.height);
        this.pixelData = this.context.getImageData(64, 41, 320, 200);
    }

    public update() {
        // character memory at 0x400 -> 40*25 characters
        // get character byte from character rom

        // character rom
        // charcode position: CHAR * 8 Bytes - Example: CHAR 01 (A)
        // 08: 18 -> 00011000 ->    ##
        // 09: 3c -> 00111100 ->   ####
        // 0a: 66 -> 01100110 ->  ##  ##
        // 0b: 7e -> 01111110 ->  ######
        // 0c: 66 -> 01100110 ->  ##  ##
        // 0d: 66 -> 01100110 ->  ##  ##
        // 0e: 66 -> 01100110 ->  ##  ##
        // 0f: 00 -> 00000000 ->

        // screen size 40*25 (1000) characters
        let screenPositionX = 0;
        let screenPositionY = 0;

        for (let screenPosition = 0; screenPosition < 1000; screenPosition++) {
            let screenCharacterCode = this.mem.readByte(0x400 + screenPosition);

            if (screenPositionX == 40) {
                screenPositionX = 0;
                screenPositionY++;
            }

            for (let currentRow = 0; currentRow < 8; currentRow++) {
                let currentLine = this.mem.readCharacterRomByte((screenCharacterCode << 3) + currentRow);

                for (let currentCol = 0; currentCol < 8; currentCol++) {
                    let pixelSet = (currentLine & 0x80) == 0x80; // leftmost pixel set?
                    let pixelPosX = (screenPositionX << 3) + currentCol;
                    let pixelPosY = (screenPositionY << 3) + currentRow;

                    // fill linear pixel buffer
                    let posInBuffer = ((pixelPosY * 320) + pixelPosX) << 2;
                    if (pixelSet) {
                        this.pixelData.data[posInBuffer + 0] = 0x00;
                        this.pixelData.data[posInBuffer + 1] = 0x88;
                        this.pixelData.data[posInBuffer + 2] = 0xff;
                        this.pixelData.data[posInBuffer + 3] = 0xff;
                    } else {
                        this.pixelData.data[posInBuffer + 0] = 0x00;
                        this.pixelData.data[posInBuffer + 1] = 0x00;
                        this.pixelData.data[posInBuffer + 2] = 0xaa;
                        this.pixelData.data[posInBuffer + 3] = 0xff;
                    }
                    currentLine = currentLine << 1; // shift left for next pixel
                }
            }
            screenPositionX++;
        }
        this.context.putImageData(this.pixelData, 64, 42);
    }
}

export default Video;