class Keyboard {

    private static keyMap = {
        // BKSP     RETURN      SHIFT       CONTROL     C=-KEY      RUN/STOP    RUN/STOP    SPACE       DEL
        0x08: 0x00, 0x0d: 0x01, 0x10: 0x0f, 0x11: 0x3a, 0x12: 0x3d, 0x13: 0x3f, 0x1b: 0x3f, 0x20: 0x3c, 0x2e: 0x00,
        // 0        1           2           3           4           5           6           7           8           9
        0x30: 0x23, 0x31: 0x38, 0x32: 0x3b, 0x33: 0x08, 0x34: 0x0b, 0x35: 0x10, 0x36: 0x13, 0x37: 0x18, 0x38: 0x1b, 0x39: 0x20,
        // A        B           C           D           E           F           G           H           U           J
        0x41: 0x0a, 0x42: 0x1c, 0x43: 0x14, 0x44: 0x12, 0x45: 0x0e, 0x46: 0x15, 0x47: 0x1a, 0x48: 0x1d, 0x49: 0x21, 0x4a: 0x22,
        // K        L           M           N           O           P           Q           R           S           T
        0x4b: 0x25, 0x4c: 0x2a, 0x4d: 0x24, 0x4e: 0x27, 0x4f: 0x26, 0x50: 0x29, 0x51: 0x3e, 0x52: 0x11, 0x53: 0x0d, 0x54: 0x16,
        // U        V           W           X           Y           Z           ^ = LARROW  +           -
        0x55: 0x1e, 0x56: 0x1f, 0x57: 0x09, 0x58: 0x17, 0x59: 0x19, 0x5a: 0x0c, 0xa0: 0x39, 0xab: 0x28, 0xad: 0x2b,
        // ,        .           CURSOR L/R  CURSOR L/R  CURSOR U/D  CURSOR U/D
        0xbc: 0x2f, 0xbe: 0x2c, 0x25: 0x02, 0x27: 0x02, 0x26: 0x07, 0x28: 0x07,

        // F1=F1/F2 F2=F3/F4    F3=F5/F6    F4=F7/F8
        0x70: 0x04, 0x71: 0x05, 0x72: 0x06, 0x73: 0x03
    }
    private keyArray: number[];

    constructor() {
        this.keyArray = [];
    }

    public onKeyDown(e: KeyboardEvent) {
        // console.log('KeyCode: ' + e.keyCode);
        if (Keyboard.keyMap[e.keyCode] != undefined) {
            if (e.keyCode == 32) {      // prevent scrolling of page
                e.preventDefault();
            }

            if (this.keyArray.indexOf(e.keyCode) == -1) {
                this.keyArray.push(e.keyCode);
            }
        }
    }

    public onKeyUp(e: KeyboardEvent) {
        let indexOfKeyCode = this.keyArray.indexOf(e.keyCode);
        this.keyArray.splice(indexOfKeyCode, 1);
    }

    public getColumnByte(rowByte: number) {
        let rowArray = [0, 0, 0, 0, 0, 0, 0, 0];

        rowByte = ~rowByte;
        rowByte = rowByte & 0xff;

        for (let i = 0; i < this.keyArray.length; i++) {
            let scanCode = this.getScanCode(this.keyArray[i]);
            let rowNum = (scanCode & 0x38) >> 3;
            rowArray[rowNum] = rowArray[rowNum] | (1 << (scanCode & 7));
        }

        let resultByte = 0;
        for (let i = 0; i < 8; i++) {
            let currentRowEnabled = ((1 << i) & rowByte) != 0;
            if (currentRowEnabled) {
                resultByte = resultByte | rowArray[i];
            }
        }
        resultByte = ~resultByte;
        resultByte = resultByte & 0xff;
        return resultByte;
    }

    private getScanCode(keyCode: number): number | undefined {
        return Keyboard.keyMap[keyCode];
    }
}

export default Keyboard;
