class AssetLoader {

    constructor(filenames : string[], callback: (returnValue : Map<string, ArrayBuffer>) => void )  {

        let dataObjects = new Map<string, ArrayBuffer>();
        let promises : Promise<Map<string, ArrayBuffer>>[] = [];

        for ( let f of filenames ) {
            promises.push(this.fetchAsset(f, dataObjects));
        }

        Promise.all(promises)
            .then( () => {
                callback(dataObjects);
            })
            .catch( error => {
                console.log(error);
            });
    }

    fetchAsset(filename : string, dataObjects : Map<string, ArrayBuffer> ) : Promise<Map<string, ArrayBuffer>> {
        return fetch(filename)
            .then((response) => {
                if ( response.status >= 200 && response.status < 300 ) {
                    return Promise.resolve(response.arrayBuffer())
                } else {
                    return Promise.reject(new Error("Couldn't load " + filename + ". Message: " + response.statusText))
                }
            })
            .then( data => {
                return Promise.resolve(dataObjects.set(filename, data))
            })
    }
}

export default AssetLoader;