class Utils {
    /**
     * converts decimal value to hex string with padding
     * 
     * @param d - input number
     * @param padding - pad with zeros to given length
     */
    public static decimalToHex(d : number, padding : number) : string {
      var hex = Number(d).toString(16);
      padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;
  
      while (hex.length < padding) {
          hex = "0" + hex;
      }
      return hex;
    }

    /**
     * mimics an integer division
     * 
     * @param a 
     * @param b 
     */
    public static intDiv(a : number, b : number ) : number {
      let result = a/b;
      if ( result >= 0 )
        return Math.floor(result);
      else
       return Math.ceil(result);
    }
}

export default Utils;