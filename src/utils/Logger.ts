class Logger {

    private log: Array<string>;
    private static INSTANCE: Logger;
    private enabled: boolean;

    private constructor() {
        this.log = new Array();
        this.enabled = false;
    }

    public static getInstance(): Logger {
        if (Logger.INSTANCE == undefined) {
            Logger.INSTANCE = new Logger();
        }
        return Logger.INSTANCE;
    }

    public isEnabled(): boolean {
        return this.enabled;
    }

    public toggleLogging(): void {
        this.enabled = ! this.enabled;
    }

    public debug(msg: string): void {
        if (this.enabled) {
            this.log.push('DEBUG ' + msg + '\n');
        }
    }

    public warn(msg: string): void {
        if (this.enabled) {
            this.log.push('WARN ' + msg + '\n');
        }
    }

    public error(msg: string): void {
        if (this.enabled) {
            this.log.push('ERROR ' + msg + '\n');
        }
    }

    public saveLogFile(filename: string = 'logfile.txt') {
        if (this.log.length > 0) {
            let textToSaveAsBlob = new Blob(this.log, { type: "text/plain" });
            let textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);

            var downloadLink = document.createElement("a");
            downloadLink.download = filename;
            downloadLink.innerHTML = "Download File";
            downloadLink.href = textToSaveAsURL;
            downloadLink.onclick = (e) => this.destroyClickedElement(e);
            downloadLink.style.display = "none";
            document.body.appendChild(downloadLink);

            downloadLink.click();
        }
    }

    public destroyClickedElement(e: Event) {
        document.body.removeChild(e.target as Node);
        this.log = new Array();
    }
}
export default Logger;
