import Utils from '../utils/Utils';
import AddressingMode from '../cpu/AddressingMode';
import OpcodeTable from '../cpu/OpcodeTable';
import { Opcode } from '../cpu/Interfaces';
import Keyboard from '../io/Keyboard';

class Memory {

    public static LITTLE_ENDIAN = 0;
    public static BIG_ENDIAN = 1;

    private memBuffer: ArrayBuffer;
    private memory: Uint8Array;
    private basicROM: Uint8Array;
    private kernalROM: Uint8Array;
    private characterROM: Uint8Array;

    private basicROMloaded: boolean;
    private kernalROMloaded: boolean;

    private keyboard : Keyboard;

    private opcodes: Opcode[];
    private endian: number;

    constructor(size: number, endian: number = Memory.BIG_ENDIAN) {
        this.memBuffer = new ArrayBuffer(size);
        this.memory = new Uint8Array(this.memBuffer);

        this.basicROM = new Uint8Array(8192);
        this.kernalROM = new Uint8Array(8192);
        this.characterROM = new Uint8Array(4096);

        this.basicROMloaded = false;
        this.kernalROMloaded = false;

        this.opcodes = OpcodeTable.getInstance().opcodes;
        this.endian = endian;

        // write zero to memory locations
        for (let i = 0; i < this.memory.length; i++) {
            this.memory[i] = 0;
        }
    }

    public registerKeyboard(keyboard: Keyboard) {
        this.keyboard = keyboard;
    }

    public readCharacterRomByte(address: number): number {
        return this.characterROM[address];
    }

    public readByte(address: number): number {
        if (this.basicROMloaded && address >= 0xa000 && address <= 0xbfff) {
            return this.basicROM[(address & 0x1fff)];
        } else if (this.kernalROMloaded && address >= 0xe000 && address <= 0xffff) {
            return this.kernalROM[(address & 0x1fff)];
        } else if ( address == 0xdc01 ) {
            return this.keyboard ? this.keyboard.getColumnByte(this.memory[0xdc00]) : this.memory[0xdc00];
        } else {
            return this.memory[address];
        }
    }

    public writeByte(address: number, value: number): void {
        this.memory[address] = value;
    }

    public readWord(address: number): number {
        if (this.endian == Memory.BIG_ENDIAN) {  // 0x1122 => 0x11, 0x22
            return this.memory[address] << 8 | this.memory[address + 1];
        } else {
            return this.memory[address + 1] << 8 | this.memory[address];
        }
    }

    public writeWord(address: number, value: number): void {
        if (this.endian == Memory.BIG_ENDIAN) { // 0x1122 => 0x11, 0x22
            this.memory[address] = (value & 0xff00) >> 8;
            this.memory[address + 1] = value & 0x00ff;
        } else {
            this.memory[address + 1] = (value & 0xff00) >> 8;
            this.memory[address] = value & 0x00ff;
        }
    }

    public memcpy(dstOffset: number, src: ArrayBuffer, srcOffset: number, length: number): void {
        var dstU8 = new Uint8Array(this.memBuffer, dstOffset, length);
        var srcU8 = new Uint8Array(src, srcOffset, length);
        dstU8.set(srcU8);
    };

    public loadBASIC(src: ArrayBuffer): void {
        var srcU8 = new Uint8Array(src, 0, 8192);
        this.basicROM.set(srcU8);
        this.basicROMloaded = true;
    };

    public loadKERNAL(src: ArrayBuffer): void {
        var srcU8 = new Uint8Array(src, 0, 8192);
        this.kernalROM.set(srcU8);
        this.kernalROMloaded = true;
    };

    public loadCHARACTER(src: ArrayBuffer): void {
        var srcU8 = new Uint8Array(src, 0, 4096);
        this.characterROM.set(srcU8);
    };

    /**
     * returns a string with the memory contents from the current address.
     * character with values $20-$7E or $80-$ff will be written at the
     * end of the line. other values will be displayed as '.'
     *
     * @param address - starting address
     * @param memory - memory array to dump
     * @param lines - lines to print
     */
    public getMemoryDump(address: number, lines: number): string {
        let length = 16 * lines;
        let msg = '';
        for (var i = address; i < address + length && i < this.memory.length;) {
            msg += Utils.decimalToHex(i, 4) + ": ";
            for (var j = i; j < i + 16; j++) {
                msg += Utils.decimalToHex(this.readByte(j), 2) + " ";
            }
            for (var j = i; j < i + 16; j++) {
                // remove unprintable characters
                if ((this.readByte(j) >= 0x00 && this.readByte(j) <= 0x1f) || this.readByte(j) == 0x7f || // UTF8 - C0 Controls
                    (this.readByte(j) >= 0x80 && this.readByte(j) <= 0x9f) ||
                    this.readByte(j) == 0xad) { // UTF8 - soft hypen
                    msg += '.';
                } else {
                    msg += String.fromCharCode(this.readByte(j));
                }
            }
            i += 16;
            msg += '\n';
        }
        return msg;
    }


    /**
     * returns a string with the disassembled memory contents from the given address.
     *
     * @param address - starting address
     * @param memory - memory array to read from
     * @param lines - lines to print
     */
    public getDisassembly(address: number, lines: number): string {
        let pc: number = address;
        let arg1: number;
        let arg2: number;
        let msg: string = '';

        for (let l = 0; l < lines && pc < this.memory.length; l++) {
            let line = Utils.decimalToHex(pc, 4) + ': ';

            let opcode = this.readByte(pc++);
            line += Utils.decimalToHex(opcode, 2) + ' ';

            if (this.opcodes[opcode].length > 1) {
                arg1 = this.readByte(pc++);
                line += Utils.decimalToHex(arg1, 2) + ' ';
            }

            if (this.opcodes[opcode].length > 2) {
                arg2 = this.readByte(pc++);
                line += Utils.decimalToHex(arg2, 2) + ' ';
            }

            // pad line
            while (line.length < 16) {
                line = line + ' ';
            }

            // disassemble
            line += '- ' + this.opcodes[opcode].label;

            switch (this.opcodes[opcode].mode) {
                case AddressingMode.ACCUMULATOR:
                    break;

                case AddressingMode.IMPLIED:
                    break;

                case AddressingMode.IMMEDIATE:
                    line += ' #$' + Utils.decimalToHex(arg1, 2);
                    break;

                case AddressingMode.RELATIVE:
                    let newpc = pc + ((arg1 > 127) ? (arg1 - 256) : arg1);
                    line += ' $' + Utils.decimalToHex(newpc, 4);
                    break;

                case AddressingMode.ABSOLUTE:
                    line += ' $' + Utils.decimalToHex(arg2, 2) + Utils.decimalToHex(arg1, 2);
                    break;

                case AddressingMode.ABSOLUTE_INDEXED_BY_X:
                    line += ' $' + Utils.decimalToHex(arg2, 2) + Utils.decimalToHex(arg1, 2) + ',X';
                    break;

                case AddressingMode.ABSOLUTE_INDEXED_BY_Y:
                    line += ' $' + Utils.decimalToHex(arg2, 2) + Utils.decimalToHex(arg1, 2) + ',Y';
                    break;

                case AddressingMode.ABSOLUTE_INDIRECT:
                    line += ' ($' + Utils.decimalToHex(arg2, 2) + Utils.decimalToHex(arg1, 2) + ')';
                    break;

                case AddressingMode.ZERO_PAGE:
                    line += ' $' + Utils.decimalToHex(arg1, 2);
                    break;

                case AddressingMode.ZERO_PAGE_INDEXED_BY_X:
                    line += ' $' + Utils.decimalToHex(arg1, 2) + ',X';
                    break;

                case AddressingMode.ZERO_PAGE_INDEXED_BY_Y:
                    line += ' $' + Utils.decimalToHex(arg1, 2) + ',Y';
                    break;

                case AddressingMode.ZERO_PAGE_INDIRECT_Y_INDEXED:
                    line += ' ($' + Utils.decimalToHex(arg1, 2) + '),Y';
                    break;

                case AddressingMode.ZERO_PAGE_X_INDEXED_INDIRECT:
                    line += ' $' + Utils.decimalToHex(arg1, 2);
                    break;
            }

            // append line to final string
            msg += line + '\n';
        }
        return msg;
    }
}

export default Memory;